import numpy as np
from numpy import random
from scipy.stats import multivariate_normal

class AbstractSampler:

    def __init__(self, dimension):
        self.dimension = dimension

    def sample(self, len):
        raise NotImplementedError('Please implement in child classes')

    def update_weights(self):
        """ Each sampler implements its own version."""
        pass


class NormalSampler(AbstractSampler):
    def __init__(self, dimension):
        super(NormalSampler, self).__init__(dimension)

    def sample(self, len):
        return random.normal(size=(len, self.dimension))


class UniformSampler(AbstractSampler):
    def __init__(self, dimension):
        super(UniformSampler, self).__init__(dimension)

    def sample(self, len):
        return random.uniform(-1, 1, size=(len, self.dimension))


class BiSampler(AbstractSampler):
    """
    The distribution is bi-modal, with the two modes differing only in bi_dimension.
    """
    def __init__(self, dimension, bi_dimension, weights=None):
        super(BiSampler, self).__init__(dimension)
        mean_0 = np.zeros(self.dimension)
        mean_0[bi_dimension] = 1.
        mean_1 = - mean_0
        self.means = np.array([mean_0, mean_1])
        if weights is None:
            self.weights = [0.5, 0.5]
        else:
            self.weights = weights
        print('Samplers mean:', self.means)
        self.covar = 0.1 * np.eye(self.dimension)

    def sample(self, len):
        out = np.zeros((len, self.dimension))
        aux = np.random.uniform(size=len)
        mixtures = [0 if elt < self.weights[0] else 1 for elt in aux]
        for i, idx in enumerate(mixtures):
            out[i] = np.random.multivariate_normal(self.means[idx], self.covar)
        return out

    def update_weights(self, realizations):
        """
        Update the weights. Handmade for now, probably justifiable by a max likelihood idea ?
        """
        responsibilities = np.zeros((len(realizations), 2))
        rv0 = multivariate_normal(self.means[0], self.covar)
        rv1 = multivariate_normal(self.means[1], self.covar)
        w0, w1 = self.weights[0], self.weights[1]
        for i, realization in enumerate(realizations):
            g0 = w0 * rv0.pdf(realization)
            g1 = w1 * rv1.pdf(realization)
            responsibilities[i, 0] = g0/(g1 + g0)
            responsibilities[i, 1] = g1/(g1 + g0)
        self.weights = np.sum(responsibilities, axis=0)
        self.weights /= np.sum(self.weights)  # normalization
        print('Mixture weights updated to ', self.weights)

