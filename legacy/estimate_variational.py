from torch.optim import Adam
from torch.utils.data import DataLoader
import torch
import numpy as np
from utils.utils import plot_progression, initialize_autoencoder, initialize_variational
from torch.optim.lr_scheduler import StepLR
import os



from datasets.utils import collate_fn_concat

def get_test_statistics(model, test_dataset, train_noise_variance, randomize_nb_obs=True):
    noise_variance = {}

    test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=False, collate_fn=collate_fn_concat)

    test_loss = 0
    regularity_loss = 0
    for i, data in enumerate(test_dataloader):

        key = "dat"
        values_batch = {key: data[key]["values"]}

        # Get the latent trajectories (here batch is all test subjects)
        if model.variational:
            means, log_variances = model.encode(data, randomize_nb_obs=randomize_nb_obs)  # mean is the latent variable
            latent_trajectories = model.get_latent_trajectories(data, means, log_variances=log_variances, sample=True)
        else:
            means , _= model.encode(data, randomize_nb_obs=randomize_nb_obs)
            latent_trajectories = model.get_latent_trajectories(data, means)


    # Decode the trajectories
    reconstructed_patients, reconstructed_mean = model.decode(latent_trajectories)

    # Computing the loss
    reconstruction = torch.zeros(1)
    if test_dataset.use_cuda:
        reconstruction = reconstruction.cuda()

    for key, val in reconstructed_patients.items():
        assert val.size() == values_batch[key].size()
        # Mse error for the given modality
        mse_error = torch.sum((val - values_batch[key]) ** 2)

        # Weighted mse error for the modality
        reconstruction += mse_error / train_noise_variance[key]

        noise_variance[key] = mse_error.cpu().detach().numpy() / val.numel()

    if model.variational:
        regularity = model.compute_regularity(means, log_variances)/means.shape[0]
    else:
        regularity = model.compute_regularity(means, None) / means.shape[0]

    # Total loss
    loss = regularity + reconstruction
    test_loss += loss.cpu().detach().numpy()
    regularity_loss += regularity.cpu().detach().numpy()


    return noise_variance, regularity_loss, test_loss



def estimate_random_slope_model_variational(model, dataset, n_epochs=10000,
                                learning_rate=5e-4, output_dir='output',
                                batch_size=32, save_every_n_iters=50,
                                print_every_n_iters=1, dico={},
                                initialize_iterations=50,
                                call_back=None, lr_decay=0.95, l=0.1,
                                estimate_noise=True, randomize_nb_obs=False,
                                            test_dataset=None, keys_to_initialize=[]):



    # Dataloader
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat)
    dataloader_train_plot = DataLoader(dataset, batch_size=1, shuffle=True, collate_fn=collate_fn_concat)
    if test_dataset is not None:
        test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=False, collate_fn=collate_fn_concat)

    # Autoencoder initialization if needed (deprecated)
    #if keys_to_initialize is not None:
    for key in keys_to_initialize:
        initialize_autoencoder(model.encoders[key], model.decoders[key],
                               dataloader, model.type, key, n_epochs=50)

    # TODO understand this
    # For now, we initialize the encoder to give the mean of the prior parameters and its variance !
    #if model.variational:
    #    initialize_variational(dataloader, model)

    # if initialize_iterations > 0 and model.is_image:
    #     initialize_autoencoder(model.encoder, model.decoder, dataloader, dataset.type, iterations=initialize_iterations)

    print('l {}, lr {}, decay {}, batch_size {}'.format(l, learning_rate, lr_decay, batch_size))

    if model.model_name != "StanleyModel":
        optimizer = Adam(list(model.parameters()), lr=learning_rate, weight_decay=1e-3)
    else:
        parameters_without_spaceshift, params_spaceshift = model.parameters()
        optimizer = Adam([
            {'params': parameters_without_spaceshift},
            {'params': params_spaceshift, 'weight_decay': 1e-3}
        ], lr=learning_rate, weight_decay=5e-2)

    scheduler = StepLR(optimizer, step_size=max(10, int(n_epochs/50)), gamma=lr_decay)

    # Train
    train_losses = []
    reconstruction_losses = {}
    for key in model.data_info.keys():
        reconstruction_losses[key] = []
    regularity_losses = []

    # test
    test_losses = []
    test_reconstruction_losses = {}
    for key in model.data_info.keys():
        test_reconstruction_losses[key] = []
    test_regularity_losses = []

    time_shift_stds = []
    eta_stds = []
    time_shift_means = []
    eta_means = []

    noise_variance = {}
    for key in model.data_info.keys():
        noise_variance[key] = l

    for epoch in range(n_epochs):
        scheduler.step()

        train_loss = 0.
        regularity_loss = 0.

        no_in_batch = 0
        n_batch = 0
        num_visits_epoch = 0

        values_batch = {}
        latent_traj_batch = {}
        for key in model.decoders.keys():
            values_batch[key] = torch.zeros(0).type(dataset.type)
            latent_traj_batch[key] = torch.zeros(0).type(dataset.type)

        means_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)
        log_variances_batch = None
        if model.variational:
            log_variances_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)

        min_lp_x, max_lp_x = float('inf'), -float('inf')

        for i, data in enumerate(dataloader):

            if model.variational:
                means, logvariances = model.encode(data)
            else:
                means, _ = model.encode(data)

            # TODO : modify values batch
            key = "dat"
            values_batch = {key: data[key]["values"]}

            num_patients_batch = len(data['idx'])
            num_visits_batch = {key: values_batch[key].shape[0]}

            if model.variational:
                latent_trajectories = model.get_latent_trajectories(data, means, logvariances, sample=True)
            else:
                latent_trajectories = model.get_latent_trajectories(data, means)

            # TODO : add variational here

            # Zero-ing the gradients
            optimizer.zero_grad()

            # Decoding
            reconstructed_patients, reconstructed_mean = model.decode(latent_trajectories)

            # Computing the loss
            reconstruction = torch.zeros(1)
            if dataset.use_cuda:
                reconstruction = reconstruction.cuda()

            for key, val in reconstructed_patients.items():
                assert val.size() == values_batch[key].size()
                # Mse error for the given modality
                mse_error = torch.sum((val - values_batch[key]) ** 2)

                if model.model_name == "StanleyModel":
                    lambda_mean = 0.5
                    mse_error = mse_error + lambda_mean * torch.sum((reconstructed_mean[key] - values_batch[key]) ** 2)

                # Weighted mse error for the modality
                if estimate_noise:
                    reconstruction += mse_error / noise_variance[key]
                else:
                    reconstruction += mse_error / l

                # update of the noise variance:
                noise_variance[key] = mse_error.cpu().detach().numpy() / val.numel()



            #if epoch > 50:
            # Computing regularity (simple or variational, the model knows)
            if model.variational:
                regularity = model.compute_regularity(means, logvariances)/means.shape[0]
            else:
                regularity = model.compute_regularity(means, None) / means.shape[0]
            #else:
            #   regularity = torch.zeros(1)

            # Total loss
            #loss = regularity + reconstruction, divised by number of features
            loss = regularity + reconstruction / len(values_batch)

            # Step of gradient descent
            loss.backward()
            optimizer.step()

            # Storing for print and plot
            train_loss += loss.cpu().detach().numpy()
            regularity_loss += regularity.cpu().detach().numpy()

            # Updating the bounds in x positions in the latent space, to know from where and until where to plot.
            for val in latent_trajectories.values():
                min_lp_x = min(np.min(val[:, 0].cpu().detach().numpy()), min_lp_x)
                max_lp_x = max(np.max(val[:, 0].cpu().detach().numpy()), max_lp_x)

            # Zero-ing all storage variables for the batch
            values_batch = {}
            latent_traj_batch = {}
            for key in model.decoders.keys():
                values_batch[key] = torch.zeros(0).type(dataset.type)
                latent_traj_batch[key] = torch.zeros(0).type(dataset.type)

            means_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)
            if model.variational:
                log_variances_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)

            no_in_batch = 0
            n_batch += 1
            key = "dat" # TODO
            num_visits_epoch += num_visits_batch[key]

        train_losses.append(train_loss/len(dataset.datasets[0].ids))
        for key in reconstruction_losses.keys():
            if estimate_noise:
                reconstruction_losses[key].append(np.sqrt(noise_variance[key])) # this is the mse on the latest batch, it's not over all the dataset
        regularity_losses.append(regularity_loss/n_batch)

        time_shift_stds.append(model.get_time_shift_std())
        time_shift_means.append(model.get_time_shift_mean())
        eta_stds.append(model.get_eta_std())
        eta_means.append(model.get_eta_mean())

        # Compute the test regularity/reconstructions/loss
        if test_dataset is not None:
            test_noise_variance, test_regularity, test_loss = get_test_statistics(model, test_dataset, noise_variance, randomize_nb_obs=True)

            test_losses.append(test_loss/len(test_dataset.datasets[0].ids))
            for key in reconstruction_losses.keys():
                test_reconstruction_losses[key].append(np.sqrt(test_noise_variance[key]))
            test_regularity_losses.append(test_regularity)


        if epoch % print_every_n_iters == 0:

            # Learning rate decay
            lr = 0.
            for param_group in optimizer.param_groups:
                lr = [param_group['lr']][0]
                break

            print('{}/{}, train loss {}, regularity loss {}, lr {}, empirical mse {}'.format(epoch, n_epochs, train_loss, regularity_loss, lr, noise_variance))

        # We save a plot of 4 individuals at the end of each epoch
        if epoch % save_every_n_iters == 0 or epoch == n_epochs-1 and False: # TODO add the plots
        #if epoch == -1:


            # Plot of the training loss
            plot_progression([train_losses, regularity_losses] + [reconstruction_losses[key] for key in reconstruction_losses.keys()],
                             ['train', 'regularity'] + ['reconstruction_' + key for key in reconstruction_losses.keys()], output_dir, 'train_loss.pdf')
            if test_dataset is not None:
                plot_progression([train_losses, regularity_losses] + [reconstruction_losses[key] for key in
                                                                      reconstruction_losses.keys()],
                                 ['loss', 'regularity'] + ['reconstruction_' + key for key in
                                                            reconstruction_losses.keys()], output_dir, 'test_loss.pdf',
                                 test_l=[test_losses, test_regularity_losses] + [test_reconstruction_losses[key] for key in test_reconstruction_losses.keys()])



                plot_progression([train_losses[-20:], regularity_losses[-20:]] + [reconstruction_losses[key][-20:] for key in reconstruction_losses.keys()],
                                 ['loss', 'regularity'] + ['reconstruction_' + key for key in reconstruction_losses.keys()], output_dir, 'test_loss_last.pdf',
                                 test_l=[test_losses[-20:], test_regularity_losses[-20:]] + [test_reconstruction_losses[key][-20:] for key in test_reconstruction_losses.keys()])


                # Plot of the prior parameters on eta and tau
            plot_progression([eta_means, eta_stds, time_shift_means, time_shift_stds], ['eta mean', 'eta std', 'time shift mean', 'time shift std'],
                             output_dir, 'prior_parameters.pdf')


            #%% Plot

            # Getting and plotting some reconstructions for training set
            times, real, reconstructed = model.get_reconstructions(dataloader_train_plot)
            model.plot_reconstructions(times, real, reconstructed, output_dir, split="train")

            # Getting and plotting some reconstructions for testing set
            if test_dataset is not None:
                times, real, reconstructed = model.get_reconstructions(test_dataloader)
                model.plot_reconstructions(times, real, reconstructed, output_dir, split="test")

            # Plotting the average trajectory
            model.plot_average_trajectory(output_dir, min_x=min_lp_x, max_x=max_lp_x)

            # Saving the model
            model.save(output_dir)

            # Do extra user-required stuff
            if call_back is not None:
                 call_back(model)

    # Final save of the losses
    np.save(os.path.join(output_dir, 'train_losses.npy'), train_losses)
    np.save(os.path.join(output_dir, 'reconstruction_losses.npy'), reconstruction_losses)
    np.save(os.path.join(output_dir, 'regularity_losses.npy'), regularity_losses)



