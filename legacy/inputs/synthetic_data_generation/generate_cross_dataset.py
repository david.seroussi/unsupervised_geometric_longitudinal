import numpy as np
import os
import math
import pickle as pickle
import shutil
import matplotlib.pyplot as plt

def sigmoid(x):
  return 1 / (1 + math.exp(-x))

def linear(x):
    return x + 1.

def exponential(x):
    return np.exp(x)

def argsh(x):
    return (np.arcsinh(x) + np.arcsinh(20)) / (2 * np.arcsinh(20))

def generate_cross_image(arms_lengths, right_arm_angle=np.pi/2, left_arm_angle=3*np.pi/2.):
    img_height = img_width = 64
    max_arm_length = min(img_height, img_width) / 2.
    kernel_width = 3.
    arms_angles = np.array([0., right_arm_angle, np.pi, left_arm_angle])
    arm_lengths = np.array(arms_lengths)

    for elt in arms_lengths:
        assert elt < max_arm_length, "Too large arm, it would go out of the picture"

    end_arm_points = np.array([(arm_lengths[i] * np.cos(arms_angles[i]), arm_lengths[i] * np.sin(arms_angles[i])) for i in
                               range(len(arms_angles))])
    arm_points = []
    for i in range(4):
        for x, y in zip(np.linspace(0, end_arm_points[i][0], 100.), np.linspace(0, end_arm_points[i][1], 100.)):
            arm_points.append((x, y))

    image = np.zeros((img_height, img_width))
    img_positions = np.zeros((img_height, img_width, 2))
    for (x, y), _ in np.ndenumerate(image):
        img_positions[x, y, :] = np.array([x, y])
    for (x, y) in arm_points:
        x_on_image = x + img_height / 2
        y_on_image = y + img_width / 2
        arr = np.array([x_on_image, y_on_image])
        squared_distances = np.sum((img_positions - arr) ** 2, 2)
        intensities = np.exp(-squared_distances / kernel_width ** 2)
        image += intensities
    image = np.clip(image, 0., 0.3 * np.max(image))
    low_values_flags = image < 0.1 * np.max(image)
    image[low_values_flags] = 0
    image /= np.max(image)
    return image


def get_mean_trajectory_length(t):
    assert t >= -20
    assert t <= 20
    return 25 + (t+15)/40 * (5 - 25)


def generate_simple_dataset(output_dir, n_elem, noise_std=0.):
    if os.path.isdir(output_dir):
        shutil.rmtree(output_dir)
    os.mkdir(output_dir)

    ids = []
    times = []
    images_path = []
    scalar_values = []
    scalar_noises = []
    info = {}

    print(output_dir)

    for i in range(n_elem):
        # generate an acceleration factor
        alpha = np.exp(np.random.normal(scale=0.3))
        time_shift = np.random.normal()
        right_arm_angle = np.random.normal(np.pi / 2., 0.3, size=1)
        left_arm_angle = np.random.normal(3 * np.pi / 2., 0.3, size=1)

        nb_visits = max(2, np.random.poisson(5, size=1))

        observation_window = np.linspace(max(-8, time_shift - 5.), min(8., time_shift + 5.), nb_visits)
        assert observation_window[0] < observation_window[1]

        delta = np.random.normal(loc=0., scale=2.)

        for j, t in enumerate(observation_window):
            time_on_geodesic = alpha * (t - time_shift)
            if time_on_geodesic >=-20 and time_on_geodesic <= 20:
                lower_and_right_lengths = get_mean_trajectory_length(time_on_geodesic)
                upper_and_left_lengths = get_mean_trajectory_length(time_on_geodesic)
                arm_lengths = [lower_and_right_lengths, lower_and_right_lengths, upper_and_left_lengths, upper_and_left_lengths]

                ids.append(i)
                times.append(t)

                image = generate_cross_image(arm_lengths, right_arm_angle, left_arm_angle)
                noise = np.random.normal(scale=noise_std, size=image.shape)
                image = image + noise
                image_name = 'cross_s{}_v{}.npy'.format(i, j)
                denoised_name = 'cross_s{}_v{}_denoised.npy'.format(i, j)
                image_path = os.path.join(output_dir, image_name)
                denoised_path = os.path.join(output_dir, denoised_name)
                np.save(denoised_path, image - noise)
                np.save(image_path, image)

                scalar_value = np.array([sigmoid(t-delta), sigmoid(t+delta)])
                scalar_noise = np.random.normal(loc=0., scale=noise_std, size=scalar_value.shape)
                scalar_value = scalar_value + scalar_noise
                scalar_values.append(scalar_value)
                scalar_noises.append(scalar_noise)
                images_path.append(image_name)
            else:
                print('OOOOOPS')

        # Now storing info for individual
        info[i] = {'alpha': alpha,
                   'time_shift': time_shift,
                   'right_arm_angle': right_arm_angle,
                   'left_arm_angle': left_arm_angle,
                   'delta': delta}

    # Saving the info for this dataset
    np.savetxt(os.path.join(output_dir, 'ids.txt'), ids)
    np.savetxt(os.path.join(output_dir, 'times.txt'), times)
    np.savetxt(os.path.join(output_dir, 'images_path.txt'), images_path, fmt='%s')
    np.savetxt(os.path.join(output_dir, 'scalar_values.txt'), scalar_values)
    np.savetxt(os.path.join(output_dir, 'scalar_noises.txt'), scalar_noises)
    pickle.dump(info, open(os.path.join(output_dir, 'info.p'), 'wb'))


def generate_mixture_dataset(output_dir, n_elem, noise_std=0.):
    """
    here, two types of individuals: ones with arms going clock wise
    ones with arms going counter clock wise.
    """
    if os.path.isdir(output_dir):
        shutil.rmtree(output_dir)
    os.mkdir(output_dir)
    print(output_dir)

    angular_velocity = 0.02

    ids = []
    times = []
    images_path = []
    scalar_values = []
    scalar_noises = []
    info = {}
    labels = {}

    for i in range(n_elem):
        # generate an acceleration factor
        alpha = np.exp(np.random.normal())
        time_shift = np.random.normal()
        right_arm_angle_0 = np.random.normal(np.pi / 2., 0.3, size=1)
        left_arm_angle_0 = np.random.normal(3 * np.pi / 2., 0.3, size=1)

        nb_visits = max(2, np.random.poisson(5, size=1))
        observation_window = np.linspace(max(-15, time_shift - 5.), min(15., time_shift + 5.), nb_visits)

        assert observation_window[0] < observation_window[1]

        mixture = 0
        if np.random.uniform() < 0.5:
            mixture = 1

        labels[i] = mixture

        delta = np.random.normal(loc=2*(mixture-0.5), scale=0.3)

        for j, t in enumerate(observation_window):
            time_on_geodesic = alpha * (t - time_shift)
            if time_on_geodesic >=-15 and time_on_geodesic <= 15:
                lower_and_right_lengths = get_mean_trajectory_length(time_on_geodesic)
                upper_and_left_lengths = get_mean_trajectory_length(time_on_geodesic)
                arm_lengths = [lower_and_right_lengths, lower_and_right_lengths, upper_and_left_lengths, upper_and_left_lengths]

                ids.append(i)
                times.append(t)

                right_arm_angle = right_arm_angle_0 + 2 * (mixture-0.5) * (t-15) * angular_velocity
                left_arm_angle = left_arm_angle_0 + 2 * (mixture-0.5) * (t-15) * angular_velocity

                image = generate_cross_image(arm_lengths, right_arm_angle, left_arm_angle)
                noise = np.random.normal(scale=noise_std, size=image.shape)
                image = image + noise
                image_name = 'cross_s{}_v{}.npy'.format(i, j)
                denoised_name = 'noise_s{}_v{}_denoised.npy'.format(i, j)
                image_path = os.path.join(output_dir, image_name)
                denoised_path = os.path.join(output_dir, denoised_name)
                np.save(denoised_path, image - noise)
                np.save(image_path, image)

                scalar_value = np.array([sigmoid(t-delta), sigmoid(t+delta)])
                scalar_noise = np.random.normal(loc=0., scale=noise_std, size=scalar_value.shape)
                scalar_value = scalar_value + scalar_noise
                scalar_values.append(scalar_value)
                scalar_noises.append(scalar_noise)
                images_path.append(image_name)
            else:
                print('OOOPS')


        # Now storing info for individual
        info[i] = {'alpha': alpha,
                   'time_shift': time_shift,
                   'right_arm_angle': right_arm_angle_0,
                   'left_arm_angle': left_arm_angle_0,
                   'mixture': mixture}

    # Saving the info for this dataset
    np.savetxt(os.path.join(output_dir, 'ids.txt'), ids)
    pickle.dump(labels, open(os.path.join(output_dir, 'labels.p'), 'wb'))
    np.savetxt(os.path.join(output_dir, 'times.txt'), times)
    np.savetxt(os.path.join(output_dir, 'images_path.txt'), images_path, fmt='%s')
    np.savetxt(os.path.join(output_dir, 'scalar_values.txt'), scalar_values)
    np.savetxt(os.path.join(output_dir, 'scalar_noises.txt'), scalar_noises)
    pickle.dump(info, open(os.path.join(output_dir, 'info.p'), 'wb'))


for noise_std in [0.1]:
    dataset_paths = '../synthetic_cross_datasets/noise_{}'.format(noise_std)
    if not os.path.isdir(dataset_paths):
        os.mkdir(dataset_paths)

    for i in range(9, 10):
        generate_simple_dataset(os.path.join(dataset_paths, 'train_{}'.format(i)), 150, noise_std=noise_std)
        generate_simple_dataset(os.path.join(dataset_paths, 'test_{}'.format(i)), 50, noise_std=noise_std)

    # for i in range(10):
    #     generate_mixture_dataset(os.path.join(dataset_paths, 'train_mixture_{}'.format(i)), 150, noise_std=noise_std)
    #     generate_mixture_dataset(os.path.join(dataset_paths, 'test_mixture_{}'.format(i)), 50, noise_std=noise_std)


















