import os
import numpy as np
import pandas as pd
from datetime import datetime
import nibabel as nib
from scipy.ndimage import zoom

def get_id(raw_id):
    # print(raw_id, raw_id[12:16])
    return int(raw_id[12:16])

def convert_viscode(viscode):
    dico = {'ses-M00':'bl','ses-M03': 'm03', 'ses-M06': 'm06','ses-M12': 'm12','ses-M18': 'm18',
            'ses-M24': 'm24','ses-M30': 'm30','ses-M36': 'm36','ses-M48': 'm48',
            'ses-M60': 'm60','ses-M66': 'm66','ses-M72': 'm72','ses-M84': 'm84'
            ,'ses-M96': 'm96','ses-M108': 'm108','ses-M120': 'm120'}
    assert viscode in dico.keys(), 'viscode {} not found'.format(viscode)
    return dico[viscode]

def reshape_64(img_data):
    a, b, c = img_data.shape
    zoom_factors = [64/a, 64/b, 64/c]
    return zoom(img_data, zoom=zoom_factors)

def reshape_128(img_data):
    a, b, c = img_data.shape
    zoom_factors = [128/a, 128/b, 128/c]
    return zoom(img_data, zoom=zoom_factors)


# ADNI MERGE DATA
adni_merge_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ADNIMERGE.csv')
adni_merge = pd.read_csv(adni_merge_path)
ages = {}


data_folder = '/localdrive10TB/data/arnaud.marcoux/ADNI/ADNI_CAPS_reconall/subjects/'

list_of_subjects = os.listdir(data_folder)

images_paths = []
ids = []
ages = []
viscodes = []

for subject in list_of_subjects:
    mri_for_subject = []
    viscodes_for_subjects = []

    list_of_visits = [elt for elt in os.listdir(os.path.join(data_folder, subject)) if elt[:3] == 'ses']
    # print(subject, list_of_visits)

    for visit in list_of_visits:
        # print(subject, visit)
        path_to_visit = os.path.join(data_folder, subject, visit)
        aux = os.listdir(path_to_visit)
        if 't1' in aux:
            path_to_mri = os.path.join(path_to_visit, 't1')
            if 'freesurfer_longitudinal' in os.listdir(path_to_mri):
            # assert 'freesurfer_longitudinal' in os.listdir(path_to_mri)

                path_to_mri = os.path.join(path_to_mri, 'freesurfer_longitudinal')
                sub_folder_name = subject + '_' + visit + '.long.' + subject
                # print(sub_folder_name, os.listdir(path_to_mri))
                if sub_folder_name in os.listdir(path_to_mri):
                # assert sub_folder_name in os.listdir(path_to_mri), 'no subfolder for the image in ' \
                #                                                    'the longitudinal freesurfer direc'


                    path_to_mri = os.path.join(path_to_mri, sub_folder_name)
                    assert 'flirt_registration' in os.listdir(path_to_mri)

                    path_to_mri = os.path.join(path_to_mri, 'flirt_registration')
                    assert 'dof_12' in os.listdir(path_to_mri)

                    path_to_mri = os.path.join(path_to_mri, 'dof_12')
                    assert 'brain_registered.nii.gz' in os.listdir(path_to_mri)

                    path_to_mri = os.path.join(path_to_mri, 'brain_registered.nii.gz')
                    mri_for_subject.append(path_to_mri)
                    viscodes_for_subjects.append(visit)

                else:
                    print("no subfolder", sub_folder_name, 'in', path_to_mri)

            else:
                print("no fl folder in", path_to_mri)

    for i, mri in enumerate(mri_for_subject):
        images_paths.append(mri)
        ids.append(subject)
        viscodes.append(viscodes_for_subjects[i])

assert len(images_paths) == len(ids)

print(len(images_paths), 'images found for', len(set(ids)), 'subjects')

#We now get the ages for the images
for i, elt in enumerate(images_paths):
    id = get_id(ids[i])
    adni_merge_subject = adni_merge[adni_merge['RID'] == id]
    viscode = convert_viscode(viscodes[i])

    if viscode == 0:
        adni_merge_subject_baseline = adni_merge_subject[adni_merge_subject['VISCODE']=='bl']
        age = adni_merge_subject_baseline['AGE'].unique()[0]
        # ages[str(id)+viscode] = '%.2f' % age
        ages.append('%.2f' % age)

    else:
        adni_merge_subject_baseline = adni_merge_subject[adni_merge_subject['VISCODE']=='bl']
        adni_merge_subject_visit = adni_merge_subject[adni_merge_subject['VISCODE']==viscode]
        baseline_age = adni_merge_subject_baseline['AGE'].unique()[0]
        baseline_examdate = datetime.strptime(adni_merge_subject_baseline['EXAMDATE'].unique()[0], "%Y-%m-%d")
        session_examdate = datetime.strptime(adni_merge_subject_visit['EXAMDATE'].unique()[0], "%Y-%m-%d")
        age = baseline_age + (session_examdate - baseline_examdate).total_seconds() / float(3600 * 24 * 365.25)
        # ages[str(id)+viscode] = '%.2f' % age
        ages.append('%.2f' % age)

assert len(ages) == len(images_paths)

info = []

for i in range(len(ages)):
    print(ages[i], os.path.basename(images_paths[i]), ids[i], viscodes[i])
    info.append((ids[i], viscodes[i], ages[i], os.path.basename(images_paths[i])))

np.savetxt('info.txt', info, fmt='%s')

# We copy all the images to a destination folder.
destination_folder = '/home/maxime.louis/MRI_data'


for i, elt in enumerate(images_paths):
    print('{}/{} images done'.format(i, len(images_paths)))
    img_data = nib.load(elt).get_data()
    name = 's' + str(get_id(ids[i])) + '_'  + convert_viscode(viscodes[i]) + '.npy'
    np.save(os.path.join(destination_folder, 'full_image/1_MCIc', name), img_data)


    # image_3d_64 = reshape_64(img_data)
    # image_3d_128 = reshape_128(img_data)
    # print(img_data.shape, image_3d_64.shape)
    # np.save(os.path.join(destination_folder, 'image_3d_64/1_MCIc', name), image_3d_64)
    # np.save(os.path.join(destination_folder, 'image_3d_128/1_MCIc', name), image_3d_128)


# We also save the ages, ids and viscodes for everyone.




