
import os
import numpy as np
from scipy.ndimage import zoom


def reshape_64(img_data):
    a, b, c = img_data.shape
    zoom_factors = [64/a, 64/b, 64/c]
    return zoom(img_data, zoom=zoom_factors)


def reshape_128(img_data):
    a, b, c = img_data.shape
    zoom_factors = [128/a, 128/b, 128/c]
    return zoom(img_data, zoom=zoom_factors)


input_path = '../../MRI_data_aux/full_image/1_MCIc'
images = os.listdir(input_path)
images = [os.path.join(input_path, elt) for elt in images if elt.find('.npy')>0]

destination_folder = '../../MRI_data'

for i, image in enumerate(images):
    print('{}/{}'.format(i, len(images)))
    img_data = np.load(image)
    image_3d_64 = reshape_64(img_data)
    image_3d_128 = reshape_128(img_data)
    name = os.path.basename(image)
    np.save(os.path.join(destination_folder, 'image_3d_64/1_MCIc', name), image_3d_64)
    np.save(os.path.join(destination_folder, 'image_3d_128/1_MCIc', name), image_3d_128)