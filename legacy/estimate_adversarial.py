from torch.optim import Adam
from torch.utils.data import DataLoader
import torch
import numpy as np
from models.networks import Discriminator
from utils.utils import plot_training_losses, initialize_autoencoder
from torch.optim.lr_scheduler import StepLR
import os


def estimate_random_slope_model_adversarial(model, dataset, n_epochs=10000,
                                learning_rate=5e-4, output_dir='output',
                                batch_size=16, save_every_n_iters=50,
                                print_every_n_iters=1, dico={},
                                initialize_iterations=50, use_cuda=False,
                                sampler=None, update_sampler=False,
                                call_back=None, randomize_nb_observations=False,
                                lr_decay=0.95, l=10.):

    if sampler is None:
        raise ValueError('Please provide a sampler for the prior')

    dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

    if initialize_iterations > 0 and model.is_image:
        initialize_autoencoder(model.encoder, model.decoder, dataloader, dataset.type, iterations=initialize_iterations)

    discriminator = Discriminator(in_dim=model.encoder_dim, hidden_dim=64)

    print('Discriminator has {} parameters'
          .format(sum([len(elt.view(-1)) for elt in discriminator.parameters()])))

    print('l: {}, randomized_nb_observations {}, lr {}, decay {}, batch_size {}'.format(l, randomize_nb_observations, learning_rate, lr_decay, batch_size))

    if use_cuda:
        discriminator.cuda()
    else:
        discriminator.float()

    optimizer_D = Adam(discriminator.parameters(), lr=0.5 * learning_rate)
    optimizer_model = Adam(model.parameters(), lr=learning_rate)

    scheduler_D = StepLR(optimizer_D, step_size=max(10, int(n_epochs/50)), gamma=lr_decay)
    scheduler_model = StepLR(optimizer_model, step_size=max(10, int(n_epochs/50)), gamma=lr_decay)

    train_losses = []
    discriminator_losses = []
    reconstruction_losses = []

    for epoch in range(n_epochs):
        scheduler_D.step()
        scheduler_model.step()

        train_loss = 0.
        discriminator_loss = 0.
        reconstruction_loss = 0.
        n = 0

        if update_sampler:
            z_realizations = []

        no_in_batch = 0
        n_batch = len(dataloader) / batch_size

        fake_samples = torch.from_numpy(np.zeros((batch_size, model.encoder_dim))).type(dataset.type)
        samples = torch.from_numpy(sampler.sample(batch_size)).type(dataset.type)

        latent_traj_batch = torch.zeros(0).type(dataset.type)
        values_batch = torch.zeros(0).type(dataset.type)

        for i, data in enumerate(dataloader):

            times = data['times'].squeeze(0)
            values = data['values'].squeeze(0)

            if randomize_nb_observations:
                if len(times) > 2:
                    to_keep = np.random.randint(2, len(times))
                else:
                    to_keep = 2
                times = times[:to_keep]
                values = values[:to_keep]

            means = model.encoder.get_sequence_output(times, values)

            if update_sampler:
                z_realizations.append(means.cpu().detach().numpy())

            fake_samples[no_in_batch] = means

            latent_traj = model.get_latent_positions_sample_from_means_and_log_variances(means, times)

            latent_traj_batch = torch.cat([latent_traj_batch, latent_traj], 0)
            values_batch = torch.cat([values_batch, values], 0)

            no_in_batch += 1

            # If we have gathered all the data for one batch, we perform a step of gradient descent:
            if no_in_batch % batch_size == 0 or i == len(dataloader)-1:

                # Zero-ing the gradients
                optimizer_D.zero_grad()

                # Training the discriminator
                loss_D = - torch.mean(torch.log(discriminator(samples.detach())) + torch.log(1 - discriminator(fake_samples.detach())))
                loss_D.backward(retain_graph=True)
                optimizer_D.step()

                reconstructed = model.decode(latent_traj_batch)

                reconstruct_loss = torch.sum((reconstructed - values_batch)**2)

                # Training for reconstruction
                optimizer_model.zero_grad()
                loss = reconstruct_loss / len(latent_traj_batch) \
                       - l * torch.sum(torch.log(discriminator(fake_samples))) / no_in_batch
                loss.backward()
                optimizer_model.step()

                train_loss += loss.cpu().detach().numpy() / no_in_batch
                discriminator_loss += loss_D.cpu().detach().numpy() / no_in_batch
                reconstruction_loss += reconstruct_loss.cpu().detach().numpy() / \
                                       len(latent_traj_batch)

                if dataset.type == torch.cuda.FloatTensor and dataset.load_on_the_fly:
                    torch.cuda.empty_cache()

                fake_samples = torch.from_numpy(np.zeros((batch_size, model.encoder_dim))).type(dataset.type)
                samples = torch.from_numpy(sampler.sample(batch_size)).type(dataset.type)

                latent_traj_batch = torch.zeros(0).type(dataset.type)
                values_batch = torch.zeros(0).type(dataset.type)

                no_in_batch = 0

        if update_sampler and epoch > 500:
            sampler.update_weights(np.array(z_realizations))

        train_losses.append(train_loss/n_batch)
        discriminator_losses.append(discriminator_loss/n_batch)
        reconstruction_losses.append(reconstruction_loss)

        if epoch % print_every_n_iters == 0:

            lr = 0.
            for param_group in optimizer_model.param_groups:
                lr = [param_group['lr']][0]
                break

            print('{}/{}, train loss {}, reconstruction loss {}, discriminator loss {}, lr {}'.format(epoch, n_epochs, train_loss, reconstruction_loss, discriminator_loss, lr))

        # We save a plot of 4 individuals at the end of each epoch
        if epoch % save_every_n_iters == 0 or epoch == n_epochs-1:
            plot_training_losses([train_losses, discriminator_losses, reconstruction_losses], ['train', 'discriminator', 'reconstruction'], output_dir)

            times_list, real_list, reconstructed_list = model.get_reconstructions(dataloader)

            model.plot_reconstructions(times_list, real_list, reconstructed_list, output_dir)
            model.plot_average_trajectory(output_dir, dataset)
            model.extra_writing(output_dir, dataloader, dico)
            model.save(output_dir)

            if call_back is not None:
                call_back(model)

    np.save(os.path.join(output_dir, 'train_losses.npy'), train_losses)
    np.save(os.path.join(output_dir, 'discriminator_losses.npy'), discriminator_losses)
    np.save(os.path.join(output_dir, 'reconstruction_losses.npy'), reconstruction_losses)



