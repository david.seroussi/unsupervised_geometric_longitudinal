from torch import nn
import numpy as np


class autoencoder(nn.Module):
    def __init__(self):
        super(autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(1, 16, 3, stride=3, padding=1),  # b, 16, 10, 10
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=2),  # b, 16, 5, 5
            nn.Conv2d(16, 8, 3, stride=2, padding=1),  # b, 8, 3, 3
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=1)  # b, 8, 2, 2
        )
        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(8, 16, 3, stride=2),  # b, 16, 5, 5
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 8, 5, stride=3, padding=1),  # b, 8, 15, 15
            nn.ReLU(True),
            nn.ConvTranspose2d(8, 1, 2, stride=2, padding=1),  # b, 1, 28, 28
            nn.Tanh()
        )

        self.latent_space_shape = 12

        self.linear_encode = nn.Linear(32, self.latent_space_shape)
        self.linear_decode = nn.Linear(self.latent_space_shape, 32)

    def encode(self, x):
        x = self.encoder(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        return x

    def forward(self, x):
        x = self.encoder(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 8, 2, 2)
        x = self.decoder(x)
        return x



class variational_autoencoder(nn.Module):
    def __init__(self):
        super(variational_autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(1, 16, 3, stride=3, padding=1),  # b, 16, 10, 10
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=2),  # b, 16, 5, 5
            nn.Conv2d(16, 8, 3, stride=2, padding=1),  # b, 8, 3, 3
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=1)  # b, 8, 2, 2
        )
        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(8, 16, 3, stride=2),  # b, 16, 5, 5
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 8, 5, stride=3, padding=1),  # b, 8, 15, 15
            nn.ReLU(True),
            nn.ConvTranspose2d(8, 1, 2, stride=2, padding=1),  # b, 1, 28, 28
            nn.Tanh()
        )

        self.linear_mu = nn.Linear(32,16)
        self.linear_sigma = nn.Linear(32, 16)

        self.linear_decoder = nn.Linear(16, 32)

    def encode(self, x):
        batch_size = x.shape[0]
        x = self.encoder(x)
        x = x.reshape(batch_size, -1)
        mu, sigma = self.linear_mu(x), self.linear_sigma(x)
        return mu, sigma

    def forward(self, x):
        batch_size = x.shape[0]
        x = self.encoder(x)
        x = x.reshape(batch_size, -1)
        mu, sigma = self.linear_mu(x), self.linear_sigma(x)
        z = np.random.normal()
        x = mu + sigma * z
        x = self.linear_decoder(x)
        x = x.reshape(batch_size, 8, 2, 2)
        x = self.decoder(x)
        return x


#%%


### 32x32x32


class Autoencoder3D64(nn.Module):
    def __init__(self, in_dim):
        super(Autoencoder3D64, self).__init__()
        self.conv3D = Conv3D64()
        self.deconv3D = Deconv3D64(in_dim)

        self.latent_space_shape = 16

        self.linear_encode = nn.Linear(128, self.latent_space_shape)
        self.linear_decode = nn.Linear(self.latent_space_shape, 128)

    def encode(self, x):
        x = self.conv3D(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        return x

    def forward(self, x):
        x = self.conv3D(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 16, 2, 2, 2)
        x = self.deconv3D(x)
        return x



class Conv3D64(nn.Module):
    def __init__(self):
        super(Conv3D64, self).__init__()

        self.convolutions = nn.ModuleList([
            # Layer 1
            nn.Conv3d(1, 8, 3, 2),  # 8 x 16 x 16 x 16
            #nn.BatchNorm3d(8),
            nn.ReLU(),
            nn.MaxPool3d(kernel_size=2, stride=2),

            # Layer 2
            nn.Conv3d(8, 16, 3, 2),  # 8 x 16 x 16 x 16
            #nn.BatchNorm3d(16),
            nn.ReLU(),
            nn.MaxPool3d(kernel_size=2, stride=2),

            # Layer 3
            nn.Conv3d(16, 16, 3, 2, padding=1),  # 8 x 16 x 16 x 16
            nn.ReLU()
        ])

        print('Conv 3D64 has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.convolutions:
            x = layer(x)
        return(x)


class Deconv3D64(nn.Module):

    def __init__(self, in_dim=2):
        self.in_dim = in_dim
        super(Deconv3D64, self).__init__()

        ngf = 3
        last_function = nn.Sigmoid()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(in_dim, 32 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32 * ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 1, 2, stride=2),
            # nn.LeakyReLU(),
            # nn.ConvTranspose3d(4 * ngf, 1, 1, stride=1),
            nn.ReLU()
        ])
        print('Deconv 3D64 has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):

        for layer in self.layers:
            x = layer(x)
        return x


#%%

### 32x32x32


class autoencoder3D32(nn.Module):
    def __init__(self, in_dim):
        super(autoencoder3D32, self).__init__()
        self.conv3D = Conv3D32()
        self.deconv3D = Deconv3D32(in_dim)

        self.latent_space_shape = 12

        self.linear_encode = nn.Linear(128, self.latent_space_shape)
        self.linear_decode = nn.Linear(self.latent_space_shape, 128)

    def encode(self, x):
        x = self.conv3D(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        return x

    def forward(self, x):
        x = self.conv3D(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 16, 2, 2, 2)
        x = self.deconv3D(x)
        return x



class Conv3D32(nn.Module):
    def __init__(self):
        super(Conv3D32, self).__init__()

        self.convolutions = nn.ModuleList([nn.Conv3d(1, 8, 4, 4),  # 8 x 16 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv3d(8, 16, 2, 2),  # 16 x 8 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv3d(16, 16, 2, 2),  # 16 x 4 x 4 x 4
                                 nn.ReLU()
                                 ])

        print('Conv 3D32 has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.convolutions:
            x = layer(x)
        return(x)


class Deconv3D32(nn.Module):

    def __init__(self, in_dim=2):
        self.in_dim = in_dim
        super(Deconv3D32, self).__init__()

        ngf = 3
        last_function = nn.Sigmoid()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(in_dim, 32 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32*ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(8 * ngf, 1, 2, stride=2),
            #nn.LeakyReLU(),
            #nn.ConvTranspose3d(4 * ngf, 4 * ngf, 2, stride=2),
            #nn.LeakyReLU(),
            #nn.ConvTranspose3d(4 * ngf, 1, 1, stride=1),
            last_function
        ])
        print('Deconv 3D32 has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):

        for layer in self.layers:
            x = layer(x)
        return x


#%% 121x145x121

class Autoencoder3DMRI(nn.Module):
    def __init__(self, in_dim):
        super(Autoencoder3DMRI, self).__init__()
        self.conv3D = Conv3DMRI()
        self.deconv3D = Deconv3DMRI(in_dim)

        self.latent_space_shape = 16

        self.linear_encode = nn.Linear(128, self.latent_space_shape)
        self.linear_decode = nn.Linear(self.latent_space_shape, 128)

    def encode(self, x):
        x = self.conv3D(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        return x

    def forward(self, x):
        x = self.conv3D(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 16, 2, 2, 2)
        x = self.deconv3D(x)
        return x

class Conv3DMRI(nn.Module):
    def __init__(self):
        super(Conv3DMRI, self).__init__()

        self.convolutions = nn.ModuleList([
            nn.Conv3d(1, 8, kernel_size=5, stride=3, padding=(4,0,4)),
            nn.MaxPool3d(kernel_size=4, stride=2, padding=(1,0,1)),
            nn.Conv3d(8, 16, 3, 2, padding=(1,0,1)),
            nn.MaxPool3d(kernel_size=3, stride=2),
            nn.Conv3d(16, 16, 2, 2),
        ])

        print('Conv 3DMRI has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.convolutions:
            x = layer(x)
        return(x)


class Deconv3DMRI(nn.Module):

    def __init__(self, in_dim=2):
        self.in_dim = in_dim
        super(Deconv3DMRI, self).__init__()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(16, 32, kernel_size=5, stride=2, padding=(1, 1, 1)),
            nn.ConvTranspose3d(32, 16, kernel_size=3, stride=2, padding=(1, 1, 1)),
            nn.ConvTranspose3d(16, 8, kernel_size=3, stride=2, padding=(1, 0, 1)),
            nn.ConvTranspose3d(8, 4, kernel_size=2, stride=2, padding=(1, 0, 1)),
            nn.ConvTranspose3d(4, 4, kernel_size=2, stride=2, padding=(1, 1, 1)),
            nn.ConvTranspose3d(4, 1, kernel_size=2, stride=2, padding=1),
            nn.ReLU()
        ])
        print('Deconv 3DMRI has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):

        for layer in self.layers:
            x = layer(x)
        return x[:,:,:-1,:-1,:-1]


class Classifier3DMRI(nn.Module):
    def __init__(self, out_dim=2):
        super(Classifier3DMRI, self).__init__()
        self.conv3D = Conv3DMRI()


        self.out_dim = out_dim

        self.linear_1 = nn.Linear(128, 64)
        self.linear_2 = nn.Linear(64, 16)
        self.linear_3 = nn.Linear(16, self.out_dim)



    def forward(self, x):
        x = self.conv3D(x)
        x = self.linear_1(x.reshape(x.shape[0], -1))
        x = nn.functional.leaky_relu(x)
        x = self.linear_2(x)
        x = nn.functional.leaky_relu(x)
        x = self.linear_3(x)
        x = nn.functional.softmax(x)
        return x



#%%

class Autoencoder2D128(nn.Module):
    def __init__(self):
        super(Autoencoder2D128, self).__init__()

        self.encoder = nn.ModuleList([
            # 1
            nn.Conv2d(1, 4, kernel_size=3, stride=2, padding=1),  # 8 x 32 x 32
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 2
            nn.Conv2d(4, 8,  kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 3
            nn.Conv2d(8, 16, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 4
            nn.Conv2d(16, 32, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.ReLU()
        ])

        ngf = 2
        self.decoder = nn.ModuleList([
            nn.ConvTranspose2d(32, 32 * ngf, 4, stride=4),
            nn.BatchNorm2d(32*ngf),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(32 * ngf, 16 * ngf, 2, stride=2),
            nn.BatchNorm2d(16 * ngf),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.BatchNorm2d(8 * ngf),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.BatchNorm2d(4 * ngf),
            #nn.LeakyReLU(),
            nn.ConvTranspose2d(4 * ngf, 1, 2, stride=2),
            #nn.LeakyReLU(),
            #nn.ConvTranspose2d(2 * ngf, 1, 2, stride=2),
            nn.Sigmoid()
        ])

        self.latent_space_shape = 16

        self.linear_encode = nn.Linear(128, self.latent_space_shape)
        self.linear_decode = nn.Linear(self.latent_space_shape, 128)

    def encode(self, x):
        for layer in self.encoder:
            x = layer(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        return x

    def forward(self, x):
        for layer in self.encoder:
            x = layer(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 32, 2, 2)
        for layer in self.decoder:
            x = layer(x)
        return x



class Autoencoder2D64(nn.Module):
    def __init__(self):
        super(Autoencoder2D64, self).__init__()

        self.encoder = nn.ModuleList([
            # 1
            nn.Conv2d(1, 4, kernel_size=3, stride=1, padding=1),  # 8 x 32 x 32
            nn.BatchNorm2d(4),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 2
            nn.Conv2d(4, 8,  kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.BatchNorm2d(8),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 3
            nn.Conv2d(8, 16, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.BatchNorm2d(16),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 4
            nn.Conv2d(16, 32, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
        ])

        ngf = 2

        self.decoder = nn.ModuleList([
            # 1
            nn.ConvTranspose2d(32, 16 * ngf, 3, stride=3),
            nn.BatchNorm2d(16 * ngf),
            nn.LeakyReLU(),

            # 2
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 3, stride=3, padding=1),
            nn.BatchNorm2d(8 * ngf),
            nn.LeakyReLU(),

            # 3
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 3, stride=2, padding=1),
            nn.BatchNorm2d(4 * ngf),
            nn.LeakyReLU(),

            # 3
            nn.ConvTranspose2d(4 * ngf, 2 * ngf, 3, stride=2, padding=0),
            nn.BatchNorm2d(2 * ngf),
            nn.LeakyReLU(),

            # 4
            nn.ConvTranspose2d(2 * ngf, 1, 2, stride=1),
            nn.LeakyReLU()
        ])



        self.latent_space_shape = 16

        self.linear_encode = nn.Linear(128, self.latent_space_shape)
        self.linear_decode = nn.Linear(self.latent_space_shape, 128)

    def encode(self, x):
        for layer in self.encoder:
            x = layer(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        return x

    def forward(self, x):
        for layer in self.encoder:
            x = layer(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 32, 2, 2)
        for layer in self.decoder:
            x = layer(x)
        return x




from collections import OrderedDict

import torch
import torch.nn as nn



class UNet(nn.Module):

    def __init__(self, in_channels=1, out_channels=1, init_features=2, latent_dim=8):
        super(UNet, self).__init__()

        features = init_features
        self.encoder1 = UNet._block(in_channels, features, name="enc1")
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.encoder2 = UNet._block(features, features * 2, name="enc2")
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.encoder3 = UNet._block(features * 2, features * 4, name="enc3")
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.encoder4 = UNet._block(features * 4, features * 8, name="enc4")
        self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.bottleneck = UNet._block(features * 8, features * 16, name="bottleneck")

        self.upconv4 = nn.ConvTranspose2d(
            features * 16, features * 8, kernel_size=2, stride=2
        )
        self.decoder4 = UNet._block((features * 8) * 2, features * 8, name="dec4")
        self.upconv3 = nn.ConvTranspose2d(
            features * 8, features * 4, kernel_size=2, stride=2
        )
        self.decoder3 = UNet._block((features * 4) * 2, features * 4, name="dec3")
        self.upconv2 = nn.ConvTranspose2d(
            features * 4, features * 2, kernel_size=2, stride=2
        )
        self.decoder2 = UNet._block((features * 2) * 2, features * 2, name="dec2")
        self.upconv1 = nn.ConvTranspose2d(
            features * 2, features, kernel_size=2, stride=2
        )
        self.decoder1 = UNet._block(features * 2, features, name="dec1")

        self.conv = nn.Conv2d(
            in_channels=features, out_channels=out_channels, kernel_size=1
        )


        self.fc_reduce_1 = nn.Linear(32*4*4, 32)
        self.fc_reduce_2 = nn.Linear(32, latent_dim)

        self.fc_augment_1 = nn.Linear(latent_dim, 32)
        self.fc_augment_2 = nn.Linear(32, 32*4*4)

    def forward(self, x):
        enc1 = self.encoder1(x)
        enc2 = self.encoder2(self.pool1(enc1))
        enc3 = self.encoder3(self.pool2(enc2))
        enc4 = self.encoder4(self.pool3(enc3))

        bottleneck = self.bottleneck(self.pool4(enc4))

        print(bottleneck.shape)

        bottleneck = bottleneck.reshape(bottleneck.shape[0], -1)

        fc_reduce_1 = self.fc_reduce_1(torch.tanh(bottleneck.reshape(bottleneck.shape[0], -1)))
        fc_reduce_2 = self.fc_reduce_2(torch.tanh(fc_reduce_1))

        #print(fc_reduce_2.shape)

        fc_augment_1 = self.fc_augment_1(torch.tanh(fc_reduce_2))
        fc_augment_2 = self.fc_augment_2(torch.tanh(fc_augment_1))

        bottleneck = fc_augment_2.reshape(bottleneck.shape[0], 32, 4, 4)

        dec4 = self.upconv4(bottleneck)
        dec4 = torch.cat((dec4, enc4), dim=1)
        dec4 = self.decoder4(dec4)
        dec3 = self.upconv3(dec4)
        dec3 = torch.cat((dec3, enc3), dim=1)
        dec3 = self.decoder3(dec3)
        dec2 = self.upconv2(dec3)
        dec2 = torch.cat((dec2, enc2), dim=1)
        dec2 = self.decoder2(dec2)
        dec1 = self.upconv1(dec2)
        dec1 = torch.cat((dec1, enc1), dim=1)
        dec1 = self.decoder1(dec1)
        return torch.sigmoid(self.conv(dec1))

    @staticmethod
    def _block(in_channels, features, name):
        return nn.Sequential(
            OrderedDict(
                [
                    (
                        name + "conv1",
                        nn.Conv2d(
                            in_channels=in_channels,
                            out_channels=features,
                            kernel_size=3,
                            padding=1,
                            bias=False,
                        ),
                    ),
                    (name + "norm1", nn.BatchNorm2d(num_features=features)),
                    (name + "relu1", nn.LeakyReLU(inplace=True))]))

""",
                    (
                        name + "conv2",
                        nn.Conv2d(
                            in_channels=features,
                            out_channels=features,
                            kernel_size=3,
                            padding=1,
                            bias=False,
                        ),
                    ),
                    (name + "norm2", nn.BatchNorm2d(num_features=features)),
                    (name + "relu2", nn.LeakyReLU(inplace=True)),
                ]
            )
        )"""