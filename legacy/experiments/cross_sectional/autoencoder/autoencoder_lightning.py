import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader
from pytorch_lightning.core.lightning import LightningModule

batch_size = 16
num_epochs = 100
learning_rate = 1e-3


def make_grid(dim, img, output):
    # Plot 3D
    n_patients = 4

    img_numpy = img.cpu().detach().numpy()[:, :, :121, 12:133, :121]
    output_numpy = output.cpu().detach().numpy()[:, :, :121, 12:133, :121]

    x_patient = []

    if dim == 3:

        slice1, slice2, slice3 = output.shape[2:5]
        slice1, slice2, slice3 = int(slice1 / 2), int(slice2 / 2), int(slice3 / 2)

        for patient in range(n_patients):
            img_numpy_plot = np.concatenate([img_numpy[patient, 0, slice1, :, :],
                                             img_numpy[patient, 0, :, slice2, :],
                                             img_numpy[patient, 0, :, :, slice3]], axis=1)

            output_numpy_plot = np.concatenate([output_numpy[patient, 0, slice1, :, :],
                                                output_numpy[patient, 0, :, slice2, :],
                                                output_numpy[patient, 0, :, :, slice3]], axis=1)

            x_patient.append(np.concatenate([img_numpy_plot, output_numpy_plot], axis=0))

        x_patient = np.concatenate(x_patient, axis=0)

    elif dim == 2:
        for patient in range(n_patients):
            x_patient.append(np.concatenate([img_numpy[patient, 0, :, :],
                                             output_numpy[patient, 0, :, :]], axis=1))

        x_patient = np.concatenate(x_patient, axis=0)

    return x_patient.T



class LitModel(LightningModule):

    def __init__(self, longitudinal_model):
        super().__init__()
        self.model = longitudinal_model

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=learning_rate)

    def forward(self, x):
        return longitudinal_model(x)

    def training_step(self, batch, batch_idx):
        x = batch["image"]
        x_hat = self(x)
        loss = F.mse_loss(x_hat, x)
        tensorboard_logs = {'train_loss': loss}

        # Add in training step
        # TODO torchvision grid ?
        grid = make_grid(3, x, x_hat)
        self.logger.experiment.add_image('train reconsutruction', grid, 0, dataformats="HW")
        #img = x_hat[0,0,60,:,:]
        #self.logger.experiment.add_image('images', img, 1,dataformats="HW")

        return {'loss': loss, 'log': tensorboard_logs}

    def test_step(self, batch, batch_idx):
        x = batch["image"]
        x_hat = self(x)

        return {'test_loss': F.mse_loss(x_hat, x)}

    def validation_step(self, batch, batch_idx):
        x = batch["image"]
        x_hat = self(x)

        # Logs
        grid = make_grid(3, x, x_hat)
        self.logger.experiment.add_image('val reconsutruction', grid, 0, dataformats="HW")

        return {'val_loss': F.mse_loss(x_hat, x)}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        tensorboard_logs = {'val_loss': avg_loss, 'val_loss_/2': avg_loss/2}
        return {'val_loss': avg_loss, 'log': tensorboard_logs}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['test_loss'] for x in outputs]).mean()
        tensorboard_logs = {'test_loss': avg_loss}
        return {'avg_test_loss': avg_loss, 'log': tensorboard_logs}

"""
    def train_dataloader(self):
        dataset = MNIST(os.getcwd(), train=True, download=True, transform=transforms.ToTensor())
        loader = DataLoader(dataset, batch_size=32, num_workers=4, shuffle=True)
        return loader

    def test_dataloader(self):
        # TODO: do a real train/val split
        dataset = MNIST(os.getcwd(), train=False, download=True, transform=transforms.ToTensor())
        loader = DataLoader(dataset, batch_size=32, num_workers=4)
        return loader
    
    def val_dataloader(self):
        # TODO: do a real train/val split
        dataset = MNIST(os.getcwd(), train=False, download=True, transform=transforms.ToTensor())
        loader = DataLoader(dataset, batch_size=32, num_workers=4)
        return loader
"""


#%% Data
import numpy as np
in_dim = 16

from inputs.access_datasets import access_PPMI_3DMRI
from legacy.experiments.cross_sectional.autoencoder.medical_image_dataset import MedicalImageDataset

def transform(x):
    x = torch.Tensor(x).unsqueeze(0)
    return x

ids, times, paths, dim, shape, df = access_PPMI_3DMRI(size=10)
medical_dataset = MedicalImageDataset(dim, ids, times, paths, labels= list(df["groups"]),transform=transform)

from sklearn.model_selection import train_test_split
ids_train, ids_test = train_test_split(np.unique(ids), test_size=0.2, random_state=42)

idx_train = np.arange(len(ids))[np.isin(ids, ids_train)]
idx_test = np.arange(len(ids))[np.isin(ids, ids_test)]


train_dataset = torch.utils.data.Subset(medical_dataset, idx_train)
test_dataset = torch.utils.data.Subset(medical_dataset, idx_test)


train = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=1)
test = DataLoader(test_dataset, batch_size=batch_size, shuffle=True, num_workers=1)


#%% Instanciate model

from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

# default used by the Trainer
checkpoint_callback = ModelCheckpoint(
    save_top_k=True,
    verbose=True,
    monitor='val_loss',
    mode='min',
    prefix=''
)

from legacy.experiments.cross_sectional.networks import Autoencoder3DMRI

longitudinal_model = Autoencoder3DMRI(in_dim=1)
litmodel = LitModel(longitudinal_model)



# most basic trainer, uses good defaults
trainer = Trainer(gpus=1, num_nodes=1, min_epochs=1, max_epochs=3, default_root_dir='../../../Results/Unsupervised/Autoencoder_PPMI_3DMRI',
                  checkpoint_callback=checkpoint_callback)
# , default_root_path='../../../Results/lightning'

trainer.fit(litmodel, train, test)
#trainer.test(test_dataloader=test)


#%%
#checkpoint = torch.load("../../../Results/Unsupervised/Autoencoder_PPMI_3DMRI/lightning_logs/version_9/checkpoints/epoch=3.ckpt")
#litmodel = LitModel(longitudinal_model)
#litmodel.load_state_dict(checkpoint["state_dict"])

# TODO save image (OK) / custom loss with reg / save and load model (OK)


"""

#%% Callbacks
from pytorch_lightning import Callback

class PlotterCallback(Callback):
    def on_epoch_end(self, trainer, pl_module):
        print('Starting to init trainer!')

        image_path = os.path.join(trainer.default_root_dir, "images")
        if not os.path.exists(image_path):
            os.makedirs(image_path)

        # Compute model on observations
        #for split, dataloader in zip (["train", "val"], [trainer.train_dataloader, trainer.test_dataloaders]):

        split, dataloader = "train", trainer.train_dataloader

        batch = next(iter(dataloader))
        img = data['image']
        output = litmodel(img).cpu()

        # Save picture train / test spluts
        save_path = os.path.join(image_path, "slices_{}_{}.pdf".format(trainer.current_epoch, split))
        medical_dataset.plot(img, output, save_path)
"""

#%%
