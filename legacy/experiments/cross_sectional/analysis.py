


"""
Load model

Load Data

Perform t-sne on latent variables
"""
import torch
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt

#%% Subsampled 64

#%%

in_dim = 16

from inputs.access_datasets import *

from legacy.experiments.cross_sectional.autoencoder.medical_image_dataset import MedicalImageDataset


def transform(x):
    x = torch.Tensor(x).unsqueeze(0)
    return x

ids, times, paths, dim, shape, df = access_syntheticDatscanCluster_2D128()
medical_dataset = MedicalImageDataset(dim, ids, times, paths, transform=transform)
loader = DataLoader(medical_dataset, batch_size=4, shuffle=False, num_workers=1)



#%%
experiment_folder = "Real_PD_2D64/exp_2020-3-2___5_archived"
experiment_folder = "test_128/exp_2020-3-2___5_archived"

output_autoencoder_folder = '../output/output_autoencoder/'
output_experiment_folder = os.path.join(output_autoencoder_folder, experiment_folder)

from legacy.experiments.cross_sectional.networks import autoencoder3D32, Autoencoder2D128, Autoencoder3DMRI, Autoencoder3D64, Autoencoder2D64

if shape == (64, 64):
    model = Autoencoder2D64()
elif shape == (64, 64, 64):
    model = Autoencoder3D64(in_dim)
elif shape == (32, 32, 32):
    model = autoencoder3D32(in_dim)
elif shape == (128, 128):
    model = Autoencoder2D128()
elif shape == (121, 145, 121):
    model = Autoencoder3DMRI(in_dim=16)
else:
    raise ValueError("Input shape does not have network")

model.load_state_dict(torch.load(os.path.join(output_experiment_folder, "conv_autoencoder.pth")))


#%% Compute latent

latent_z = []

for i, batch in enumerate(loader):
    latent_z.append(model.encode(batch['image']))

latent_z = torch.cat(latent_z)

#%% Do t-sne

import numpy as np
from sklearn.manifold import TSNE
X_embedded = TSNE(n_components=2, init="pca", random_state=0).fit_transform(latent_z.detach().numpy())




#%% Is 1 direction of tsne related to age ?

mean_diffs = []

for id_patient in np.unique(ids):
    ids_patient = np.arange(len(ids))[np.array(ids)==id_patient]

    if len(ids_patient)>1:
        mean_diff = np.diff(X_embedded[ids_patient, :], axis=0).mean(axis=0)
        mean_diffs.append(mean_diff)
mean_diffs = np.array(mean_diffs)

fig, ax = plt.subplots()
ax.hist(mean_diffs, bins=40, alpha=0.5)
ax.vlines(mean_diffs.mean(axis=0)[0], 0, 100)
ax.vlines(mean_diffs.mean(axis=0)[1], 0, 100)
plt.savefig(os.path.join(output_experiment_folder, "tsne_mean_diff.pdf"))
plt.show()

print("Mean : ", mean_diffs.mean(axis=0))


#%% Spaghetti plot of progression in disease

# for 1 patient
import matplotlib.pyplot as plt
import matplotlib.cm as cm


fig, ax = plt.subplots()

for id_patient in np.unique(ids)[:100]:
    ids_patient = np.arange(len(ids))[np.array(ids)==id_patient]
    ax.plot(X_embedded[ids_patient, 0],
            X_embedded[ids_patient, 1], c="black")
    for j, i in enumerate(ids_patient):

        color_palette = cm.get_cmap('jet', len(ids_patient)+1)
        colors = color_palette(range(len(ids_patient)+1))

        ax.scatter(X_embedded[i,0],
                   X_embedded[i,1], c=colors[j])

ax.arrow(0,0, mean_diffs.mean(axis=0)[0], mean_diffs.mean(axis=0)[1], width = 1, color="red")
#ax.plot([0,0], mean_diffs.mean(axis=0), c="black", linewidth = 4, alpha=0.4)

plt.savefig(os.path.join(output_experiment_folder, "tsne_spaghetti.pdf"))
plt.show()


#%% Check PCA and PCA spaghetti
from sklearn.decomposition import PCA
pca = PCA(n_components=2)
X_embedded = pca.fit_transform(latent_z.detach().numpy())

print(pca.explained_variance_ratio_)
print(pca.components_)

mean_diffs = []
for id_patient in np.unique(ids):
    ids_patient = np.arange(len(ids))[np.array(ids)==id_patient]

    if len(ids_patient)>1:
        mean_diff = np.diff(X_embedded[ids_patient, :], axis=0).mean(axis=0)
        mean_diffs.append(mean_diff)
mean_diffs = np.array(mean_diffs)


fig, ax = plt.subplots()

for id_patient in np.unique(ids)[:100]:
    ids_patient = np.arange(len(ids))[np.array(ids)==id_patient]
    ax.plot(X_embedded[ids_patient, 0],
            X_embedded[ids_patient, 1], c="black")
    for j, i in enumerate(ids_patient):

        color_palette = cm.get_cmap('jet', len(ids_patient)+1)
        colors = color_palette(range(len(ids_patient)+1))

        ax.scatter(X_embedded[i,0],
                   X_embedded[i,1], c=colors[j])

ax.arrow(0,0, mean_diffs.mean(axis=0)[0], mean_diffs.mean(axis=0)[1], width = 0.4, color="red")
#ax.plot([0,0], mean_diffs.mean(axis=0), c="black", linewidth = 4, alpha=0.4)

plt.savefig(os.path.join(output_experiment_folder, "pca_spaghetti.pdf"))
plt.show()


#%% Plot cohort




groups = df["subject_cohort"]=="PD"
groups = df["subject_cohort"]
label = "cohort"

fig, ax = plt.subplots()

for cohort, df_cohort in df.reset_index().groupby("subject_cohort"):

    ax.scatter(X_embedded[df_cohort.index,0],
               X_embedded[df_cohort.index,1], label=cohort, alpha=0.5)
plt.legend()
plt.savefig(os.path.join(output_experiment_folder, "tsne_{}.pdf".format(label)))
plt.show()


#%% PLS latent sur time or delta time ????