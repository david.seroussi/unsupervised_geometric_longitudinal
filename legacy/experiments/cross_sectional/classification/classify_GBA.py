import torch
torch.multiprocessing.set_sharing_strategy('file_system')
import os
import matplotlib.pyplot as plt
from torch import nn
from torch.utils.data import DataLoader
import numpy as np

#%% Parameters


experiment_name = "classify_GBA"

if torch.cuda.is_available():
    cuda = 1
    print("Torch CUDA is available")
else:
    cuda = 0
    print("Torch CUDA is not available")
batch_size = 8
num_epochs = 10
learning_rate = 1e-3
in_dim = 16

from datetime import date
today = date.today()
output_folder = '../output/output_classifier/{}/exp'.format(experiment_name)
output_folder += "_{}-{}-{}".format(today.year, today.month, today.day)
output_folder += "___0"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)
else:
    i=0
    while os.path.exists(output_folder):
        output_folder = output_folder.split("___")[0]+"___{}".format(i)
        i += 1
    os.makedirs(output_folder)

if not os.path.exists(os.path.join(output_folder, "examples")):
    os.makedirs(os.path.join(output_folder, "examples"))

#%% Dataset PPMI - Datscan 2D Cross

from inputs.access_datasets import access_PPMI_3DMRI
from legacy.experiments.cross_sectional.autoencoder.medical_image_dataset import MedicalImageDataset



#%% Access GBA Data PPMI

def transform(x):
    x = torch.Tensor(x).unsqueeze(0)
    return x


# Create MedicalImageDataset
ids, times, paths, dim, shape, df = access_PPMI_3DMRI(size=20)
medical_dataset = MedicalImageDataset(dim, ids, times, paths, labels= list(df["groups"]),transform=transform)


print("Number of GBA+ : ", len(df[df["groups"]=="GBA+"]))
print("Number of non GBA+ : ", len(df[df["groups"]!="GBA+"]))

#%%


from sklearn.model_selection import train_test_split
ids_train, ids_test = train_test_split(np.unique(ids), test_size=0.2, random_state=42)

idx_train = np.arange(len(ids))[np.isin(ids, ids_train)]
idx_test = np.arange(len(ids))[np.isin(ids, ids_test)]


train_dataset = torch.utils.data.Subset(medical_dataset, idx_train)
test_dataset = torch.utils.data.Subset(medical_dataset, idx_test)


train = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=1)
test = DataLoader(test_dataset, batch_size=batch_size, shuffle=True, num_workers=1)


#%% Instanciate model
from legacy.experiments.cross_sectional.networks import Classifier3DMRI

model = Classifier3DMRI(2)


if cuda:
    model = model.cuda()



#%%

print("Begin optimization")

from torch.autograd import Variable

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate,
                             weight_decay=1e-5)

log_train_loss = []
log_test_loss = []

for epoch in range(num_epochs):
    acc_train_loss = 0

    for batch_num, data in enumerate(train):
        img = data['image']
        labels = torch.LongTensor(1 * (np.isin(data['label'],["GBA+", "LRRK2+"]))).reshape(-1)
        #labels = torch.LongTensor(1*(np.array(data['label'])=="GBA+")).reshape(-1)
        if not cuda:
            img = Variable(img)#.cuda()
        else:
            img = Variable(img).cuda()
            labels = labels.cuda()
        # ===================forward=====================
        output = model(img)
        if not cuda:
            loss = criterion(output, torch.tensor(labels, dtype=torch.float32))
        else:
            loss = criterion(output, labels)
        # ===================backward====================
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    # ===================log========================
        acc_train_loss += float(loss.detach().cpu().numpy())

    print('epoch [{}/{}], loss:{:.4f}'
          .format(epoch+1, num_epochs, float(loss.cpu().data)))

    log_train_loss.append(acc_train_loss/batch_num)

    # Compute test loss
    acc_test_loss = 0
    for batch_num, data in enumerate(test):
        img = data['image']
        labels = torch.LongTensor(1 * (np.isin(data['label'],["GBA+", "LRRK2+"]))).reshape(-1)
        #labels = torch.LongTensor(1 * (np.array(data['label']) == "GBA+")).reshape(-1)
        if cuda:
            img = img.cuda()
            labels = labels.cuda()
        # ===================forward=====================
        output = model(img)
        if not cuda:
            loss = criterion(output, torch.LongTensor(labels))
        else:
            loss = criterion(output, labels)
        acc_test_loss += float(loss.detach().cpu().numpy())
    log_test_loss.append(acc_test_loss / batch_num)



    # Save picture
    save_path = os.path.join(output_folder, "examples","slices_{}_test.pdf".format(epoch))
    #medical_dataset.plot_input(img, save_path)

    for batch_num, data in enumerate(train):
        img = data['image']
        if not cuda:
            img = Variable(img)
        else:
            img = Variable(img).cuda()
        output = model(img)
        if batch_num>1:
            break
    save_path = os.path.join(output_folder, "examples","slices_{}_train.pdf".format(epoch))
    #medical_dataset.plot_input(img, save_path)

    # Plot Loss
    fig, ax = plt.subplots(1, 1)

    ax.plot(log_train_loss, label='train')
    ax.plot(log_test_loss, label='test')

    plt.legend()
    plt.savefig(os.path.join(output_folder, 'train_test_loss.pdf'))
    plt.show()
    plt.close

    # Save model
    torch.save(model.state_dict(), os.path.join(output_folder, 'conv_classifier.pth'))



