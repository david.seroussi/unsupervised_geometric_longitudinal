import numpy as np
import os
import pandas as pd
from multimodal_dataset import MultimodalDataset, DatasetTypes
from sklearn.model_selection import RepeatedStratifiedKFold
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from legacy.estimate_variational import estimate_random_slope_model_variational
from torch.utils.data import DataLoader
from models.longitudinal_models.abstract_model import RandomSlopeModel
import pickle
import torch



class Benchmark():
    """
    Compute and compare the residual errors for both multimodal and pet-only datasets.
    Validation via Repeated Stratified Cross Validation
    Performance assessed in both extrapolation and interpolation : do we do better than basic strategies ?
    """
    def __init__(self, paths, nn_parameters, seed=0, validation=(5, 10), name=None, patients='intersection',
                 learning_rate=1e-3, batch_size=32, test_validation=False):

        # Hyperparameters
        self.seed = seed

        # Paths
        self.name = 'Unnamed yet'
        if name is not None:
            self.name = name
        self.paths = paths

        # Data
        self.patients = patients
        self.df = None
        self.distinct_rids = None
        self.groups = None
        self.initialize_data()

        # Validation procedure parameters
        self.validation = validation
        self.test_validation = test_validation

        # Neural net parameters
        self.nn_parameters = nn_parameters
        self.learning_rate = learning_rate
        self.batch_size = batch_size

        # Folds
        self.generate_folds()
        self.save_infos()

    def save_infos(self):
        if not os.path.exists(self.paths['output_dir']):
            os.makedirs(self.paths['output_dir'])

        with open(os.path.join(self.paths['output_dir'],'{0}.p'.format(self.name)), 'wb') as output:  # Overwrites any existing file.
            pickle.dump(self, output)


    def initialize_data(self):
        # Load pet .csv
        df_pet = pd.read_csv(self.paths['pet_scores'], header=None)
        df_pet[0] = df_pet[0].apply(lambda x: int(x.split('_S_')[1]))
        df_pet.set_index([0, 1], inplace=True)
        df_pet.index.rename(['id', 'age'], inplace=True)
        distinct_rids_pet = df_pet.index.unique('id')

        # Load adas .csv
        df_adas = pd.read_csv(self.paths['adas_scores'], header=None)
        df_adas[0] = df_adas[0].apply(lambda x: int(x.split('S')[1]))
        df_adas.set_index([0, 1], inplace=True)
        df_adas.index.rename(['id', 'age'], inplace=True)
        df_adas = df_adas.iloc[:,:4]
        distinct_rids_adas = df_adas.index.unique('id')

        # Union of ids
        union_rids = np.union1d(distinct_rids_adas, distinct_rids_pet)

        # Intersect of ids, adas_only, pet_only ids
        intersect_rids = np.intersect1d(distinct_rids_adas, distinct_rids_pet)
        adas_only_rids = np.setdiff1d(distinct_rids_adas, intersect_rids)
        pet_only_rids = np.setdiff1d(distinct_rids_pet, intersect_rids)

        # Union / Intersection
        if self.patients=='union':
            distinct_rids = union_rids
        elif self.patients=='intersection':
            distinct_rids = intersect_rids

            # Update Pet/Adas Dataframes
            df_adas = df_adas.loc[distinct_rids]
            df_pet = df_pet.loc[distinct_rids]
        else:
            print('Patients selection not done')
            distinct_rids = np.nan

        # 0 for id in both, 1 for pet only, 2 for adas only
        groups = np.zeros_like(distinct_rids)
        groups[np.isin(distinct_rids, pet_only_rids)] = 1
        groups[np.isin(distinct_rids, adas_only_rids)] = 2

        self.df = {'adas_scores': df_adas, 'pet_scores': df_pet}
        self.distinct_rids = distinct_rids
        self.groups = groups
        self.data_dim = {'adas_scores' : df_adas.shape[1], 'pet_scores' : df_pet.shape[1]}

    def generate_folds(self):
        n_splits, n_repeats = self.validation
        folds = {}
        rkf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats, random_state=self.seed)
        for fold, (train_index, test_index) in enumerate(rkf.split(self.distinct_rids, self.groups)):
            folds[fold] = {'train': self.distinct_rids[train_index], 'test': self.distinct_rids[test_index]}
        self.folds = folds

    def generate_datasets_from_fold(self, fold, key='train'):

        # Get mu, sigma from train
        # Adas
        train_adas_dataset_ = LongitudinalScalarDataset(list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                                                        list(self.df['adas_scores'].loc[self.folds[fold][key]].values),
                                                        list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='age')))

        # Adas
        adas_dataset_ = LongitudinalScalarDataset(list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                                                        list(self.df['adas_scores'].loc[self.folds[fold][key]].values),
                                                        list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='age')),
                                                       ages_std=train_adas_dataset_.ages_std,
                                                       ages_mean=train_adas_dataset_.ages_mean)


        pet_dataset_ = LongitudinalScalarDataset(list(self.df['pet_scores'].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                                                        list(self.df['pet_scores'].loc[self.folds[fold][key]].values),
                                                        list(self.df['pet_scores'].loc[self.folds[fold][key]].index.get_level_values(level='age')),
                                                       ages_std=train_adas_dataset_.ages_std,
                                                       ages_mean=train_adas_dataset_.ages_mean)

        # Multimodal dataset
        dataset_multimodal = MultimodalDataset([adas_dataset_, pet_dataset_], ['adas_scores', 'pet_scores'], [DatasetTypes.SCALAR, DatasetTypes.PET])

        # Only PET Dataset
        dataset_pet = MultimodalDataset([pet_dataset_], ['pet_scores'],[DatasetTypes.PET])

        # Only ADAS Dataset
        dataset_adas = MultimodalDataset([adas_dataset_], ['adas_scores'], [DatasetTypes.SCALAR])

        return dataset_multimodal, dataset_pet, dataset_adas

    def compute_all(self, data_type='multimodal'):

        torch.manual_seed(self.seed)
        np.random.seed(self.seed)


        train_adas_dataset_ = LongitudinalScalarDataset(list(self.df['adas_scores'].index.get_level_values(level='id')),
                                                        list(self.df['adas_scores'].values),
                                                        list(self.df['adas_scores'].index.get_level_values(level='age')))

        # Adas
        adas_dataset_ = LongitudinalScalarDataset(list(self.df['adas_scores'].index.get_level_values(level='id')),
                                                        list(self.df['adas_scores'].values),
                                                        list(self.df['adas_scores'].index.get_level_values(level='age')),
                                                       ages_std=train_adas_dataset_.ages_std,
                                                       ages_mean=train_adas_dataset_.ages_mean)


        pet_dataset_ = LongitudinalScalarDataset(list(self.df['pet_scores'].index.get_level_values(level='id')),
                                                        list(self.df['pet_scores'].values),
                                                        list(self.df['pet_scores'].index.get_level_values(level='age')),
                                                       ages_std=train_adas_dataset_.ages_std,
                                                       ages_mean=train_adas_dataset_.ages_mean)

        # Multimodal dataset
        train_dataset_multimodal = MultimodalDataset([adas_dataset_, pet_dataset_], ['adas_scores', 'pet_scores'], [DatasetTypes.SCALAR, DatasetTypes.PET])

        # Only PET Dataset
        train_dataset_pet = MultimodalDataset([pet_dataset_], ['pet_scores'],[DatasetTypes.PET])

        # Only ADAS Dataset
        train_dataset_adas = MultimodalDataset([adas_dataset_], ['adas_scores'], [DatasetTypes.SCALAR])

        # Get the parameters
        latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs = self.nn_parameters

        # Get Data info
        pet_info = (DatasetTypes.PET, 24, 24, self.data_dim['pet_scores'],
                    ['Y_{0}'.format(i) for i in range(self.data_dim['pet_scores'])],
                    ['r' for i in range(self.data_dim['pet_scores'])])
        adas_info = (DatasetTypes.SCALAR, 8, 8, self.data_dim['adas_scores'],
                     ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b'])

        data_info_multimodal = {'adas_scores': adas_info, 'pet_scores': pet_info}
        data_info_pet = {'pet_scores': pet_info}
        data_info_adas = {'adas_scores': adas_info}

        # Directories
        output_dir_multimodal = os.path.join(self.paths['output_dir'], 'fold_all2', 'multimodal/')
        output_dir_pet = os.path.join(self.paths['output_dir'], 'fold_all2', 'pet/')
        output_dir_adas = os.path.join(self.paths['output_dir'], 'fold_all2', 'adas/')

        if not os.path.exists(output_dir_multimodal):
            os.makedirs(output_dir_multimodal)
        if not os.path.exists(output_dir_pet):
            os.makedirs(output_dir_pet)
        if not os.path.exists(output_dir_adas):
            os.makedirs(output_dir_adas)

        # Save the fold datasets
        train_dataset_multimodal.save(os.path.join(output_dir_multimodal, 'train_dataset.p'))
        train_dataset_pet.save(os.path.join(output_dir_pet, 'train_dataset.p'))
        train_dataset_adas.save(os.path.join(output_dir_adas, 'train_dataset.p'))

        if data_type == 'multimodal':
            # Model building
            random_slope_model_multimodal = RandomSlopeModel(
                data_info=data_info_multimodal,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                pre_decoder_dim=pre_decoder_dim,
                random_slope=random_slope,
                variational=False,
                use_cuda=False,
                atlas_path=self.paths['atlas']
            )

            # Estimation
            estimate_random_slope_model_variational(random_slope_model_multimodal, train_dataset_multimodal,
                                                    n_epochs=n_epochs,
                                                    learning_rate=self.learning_rate, output_dir=output_dir_multimodal,
                                                    batch_size=self.batch_size, save_every_n_iters=100,
                                                    call_back=lambda x: call_back(x, train_dataset_multimodal,
                                                                                  output_dir_multimodal),
                                                    lr_decay=0.98,
                                                    l=1., estimate_noise=True,
                                                    randomize_nb_obs=True, test_dataset=None)

        elif data_type == 'pet':

            random_slope_model_pet = RandomSlopeModel(
                data_info=data_info_pet,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                pre_decoder_dim=pre_decoder_dim,
                random_slope=random_slope,
                variational=False,
                use_cuda=False,
                atlas_path=self.paths['atlas']
            )

            estimate_random_slope_model_variational(random_slope_model_pet, train_dataset_pet, n_epochs=n_epochs,
                                                    learning_rate=self.learning_rate, output_dir=output_dir_pet,
                                                    batch_size=self.batch_size, save_every_n_iters=100,
                                                    call_back=lambda x: call_back(x, train_dataset_pet, output_dir_pet),
                                                    lr_decay=0.98,
                                                    l=1., estimate_noise=True,
                                                    randomize_nb_obs=True, test_dataset=None)

        elif data_type == 'adas':

            random_slope_model_adas = RandomSlopeModel(
                data_info=data_info_adas,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                pre_decoder_dim=pre_decoder_dim,
                random_slope=random_slope,
                variational=False,
                use_cuda=False,
                atlas_path=None
            )

            estimate_random_slope_model_variational(random_slope_model_adas, train_dataset_adas, n_epochs=n_epochs,
                                                    learning_rate=self.learning_rate, output_dir=output_dir_adas,
                                                    batch_size=self.batch_size, save_every_n_iters=100,
                                                    call_back=lambda x: call_back(x, train_dataset_adas,
                                                                                  output_dir_adas),
                                                    lr_decay=0.98,
                                                    l=1., estimate_noise=True,
                                                    randomize_nb_obs=True, test_dataset=None)
        else:
            print('Data type not recognized')



    def compute_fold(self, fold, data_type='multimodal'):

        # Set the seed
        torch.manual_seed(self.seed)
        np.random.seed(self.seed)

        # get the fold datasets
        train_dataset_multimodal, train_dataset_pet, train_dataset_adas = self.generate_datasets_from_fold(fold, key='train')
        test_dataset_multimodal, test_dataset_pet, test_dataset_adas = None, None, None

        if self.test_validation:
            test_dataset_multimodal, test_dataset_pet, test_dataset_adas = self.generate_datasets_from_fold(fold, key='test')

        # Get the parameters
        latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs = self.nn_parameters

        # Get Data info
        pet_info = (DatasetTypes.PET, 24, 24, self.data_dim['pet_scores'],
                    ['Y_{0}'.format(i) for i in range(self.data_dim['pet_scores'])],
                    ['r' for i in range(self.data_dim['pet_scores'])])
        adas_info = (DatasetTypes.SCALAR, 8, 8, self.data_dim['adas_scores'],
                     ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b'])

        data_info_multimodal = {'adas_scores': adas_info, 'pet_scores': pet_info}
        data_info_pet = {'pet_scores': pet_info}
        data_info_adas = {'adas_scores': adas_info}



        # Directories
        output_dir_multimodal = os.path.join(self.paths['output_dir'],'fold_{0}'.format(fold),'multimodal/')
        output_dir_pet = os.path.join(self.paths['output_dir'],'fold_{0}'.format(fold),'pet/')
        output_dir_adas = os.path.join(self.paths['output_dir'], 'fold_{0}'.format(fold), 'adas/')

        if not os.path.exists(output_dir_multimodal):
            os.makedirs(output_dir_multimodal)
        if not os.path.exists(output_dir_pet):
            os.makedirs(output_dir_pet)
        if not os.path.exists(output_dir_adas):
            os.makedirs(output_dir_adas)


        # Save the fold datasets
        train_dataset_multimodal.save(os.path.join(output_dir_multimodal, 'train_dataset.p'))
        train_dataset_pet.save(os.path.join(output_dir_pet, 'train_dataset.p'))
        train_dataset_adas.save(os.path.join(output_dir_adas, 'train_dataset.p'))

        if data_type == 'multimodal':
            # Model building
            random_slope_model_multimodal = RandomSlopeModel(
                data_info=data_info_multimodal,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                pre_decoder_dim=pre_decoder_dim,
                random_slope=random_slope,
                variational=False,
                use_cuda=False,
                atlas_path=self.paths['atlas']
            )

            # Estimation
            estimate_random_slope_model_variational(random_slope_model_multimodal, train_dataset_multimodal,
                                                    n_epochs=n_epochs,
                                                    learning_rate=self.learning_rate, output_dir=output_dir_multimodal,
                                                    batch_size=self.batch_size, save_every_n_iters=100,
                                                    call_back=lambda x: call_back(x, train_dataset_multimodal,
                                                                                  output_dir_multimodal),
                                                    lr_decay=0.98,
                                                    l=1., estimate_noise=True,
                                                    randomize_nb_obs=True, test_dataset=test_dataset_multimodal)

        elif data_type == 'pet':

            random_slope_model_pet = RandomSlopeModel(
                data_info=data_info_pet,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                pre_decoder_dim=pre_decoder_dim,
                random_slope=random_slope,
                variational=False,
                use_cuda=False,
                atlas_path=self.paths['atlas']
            )

            estimate_random_slope_model_variational(random_slope_model_pet, train_dataset_pet, n_epochs=n_epochs,
                                                    learning_rate=self.learning_rate, output_dir=output_dir_pet,
                                                    batch_size=self.batch_size, save_every_n_iters=100,
                                                    call_back=lambda x: call_back(x, train_dataset_pet, output_dir_pet),
                                                    lr_decay=0.98,
                                                    l=1., estimate_noise=True,
                                                    randomize_nb_obs=True, test_dataset=test_dataset_pet)

        elif data_type == 'adas':

            random_slope_model_adas = RandomSlopeModel(
                data_info=data_info_adas,
                latent_space_dim=latent_space_dim,
                pre_encoder_dim=pre_encoder_dim,
                pre_decoder_dim=pre_decoder_dim,
                random_slope=random_slope,
                variational=False,
                use_cuda=False,
                atlas_path=None
            )

            estimate_random_slope_model_variational(random_slope_model_adas, train_dataset_adas, n_epochs=n_epochs,
                                                    learning_rate=self.learning_rate, output_dir = output_dir_adas,
                                                    batch_size=self.batch_size, save_every_n_iters=100,
                                                    call_back=lambda x: call_back(x, train_dataset_adas, output_dir_adas),
                                                    lr_decay=0.98,
                                                    l=1., estimate_noise=True,
                                                    randomize_nb_obs=True, test_dataset=test_dataset_adas)
        else:
            print('Data type not recognized')




def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model, dataset, output_dir):
    save_dataset_info(dataset, model, 'train', output_dir)



