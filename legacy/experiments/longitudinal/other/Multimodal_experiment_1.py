import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import pandas as pd
import torch

torch.manual_seed(0)
np.random.seed(0)


"""
Experiment 1 : Can we better predict future PET with the help of cognitive scores ? (only train set)

We remove the last PET on 20% of the training subjects (only subjects that have at least 3 PETS).
We train on this dataset.
We test the performance on the last PET that we removed.


Thus :
-Separate train/test set
-Launch PET-only model / PET-Adas model on train set
-Assess results
"""



#%% Parameters

# Random slope model parameters
pre_encoder_dim = 12
pre_decoder_dim = 12
latent_space_dim = 6
random_slope = False
n_epochs = 2000
n_folds = 60


#%%

# Paths
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
results_dir = '../output/benchmark_multimodal_experiment1/'
adas_path = '../data/data_adas/df.csv'


def keep_at_least_2_visits(df):
    res = pd.DataFrame()
    if (df.shape[1]>1):
        res = df
    return res




## PET

# Load pet .csv
df_pet = pd.read_csv(pet_path, header = None)
df_pet = df_pet.set_index([0,1]).groupby(0).apply(keep_at_least_2_visits).reset_index()
ids_pet = df_pet.iloc[:, 0].values
ids_pet = pd.Series(ids_pet).apply(lambda x: int(x.split('_S_')[1]))
times_pet = df_pet.iloc[:, 1].values
values_pet = df_pet.iloc[:, 2:].values
data_dim_pet = values_pet.shape[1]

# Extract unique rids / labels
distinct_rids_pet = np.unique(ids_pet)


## ADAS

# load adas .csv
df_adas = pd.read_csv(adas_path, header = None)
df_adas = df_adas.set_index([0,1]).groupby(0).apply(keep_at_least_2_visits).reset_index()
ids_adas = pd.Series(df_adas.values[:, 0]).apply(lambda x: int(x.split('S')[1]))
values_adas = df_adas.values[:, 2:6]
times_adas = df_adas.values[:, 1]
data_dim_adas = values_adas.shape[1]

# Extract unique rids/labels
distinct_rids_adas = np.unique(ids_adas)

# Intersection of ids
distinct_rids = np.union1d(distinct_rids_adas, distinct_rids_pet)
distinct_rids_intersection = np.intersect1d(distinct_rids_adas, distinct_rids_pet)

# Pet Dataset with intersection of id + rename columns
df_pet = pd.DataFrame({'id': ids_pet, 'time': times_pet})
df_pet = pd.concat([df_pet, pd.DataFrame(values_pet)], axis=1)
df_pet = df_pet[np.isin(df_pet['id'],distinct_rids )]
df_pet.set_index(['id','time'], inplace=True)

# Adas Dataset with intersection of id + rename columns
df_adas = pd.DataFrame({'id': ids_adas, 'time': times_adas})
df_adas = pd.concat([df_adas, pd.DataFrame(values_adas)], axis=1)
df_adas = df_adas[np.isin(df_adas['id'],distinct_rids )]
df_adas.set_index(['id','time'], inplace=True)


#%%

#### Multiple folds

fold = 0
name_experiment = 'output_multimodal_pet-adas_experiment_1_test{0}'.format(fold)

if not os.path.exists(os.path.join(results_dir, name_experiment)):
    os.makedirs(os.path.join(results_dir, name_experiment))


#%% Train / Test

print("Beginning train/test split")


n_test = int(0.2*df_pet.loc[distinct_rids_intersection].index.unique('id').shape[0])
print("We seek 20%={0} patients out of {1} that have at least 3 visits, "
      "and have both scalar and pet, and we will remove the last".format(
    n_test,
    df_pet.loc[distinct_rids_intersection].index.unique('id').shape[0]))

df_npets = df_pet.loc[distinct_rids_intersection].groupby('id').apply(lambda x: x.shape[0])
potential_test_patients = df_npets[df_npets>2].index.values

print("{0} patients have at least 3 visits, let's select randomly from them".format(
    potential_test_patients.shape[0]))

test_patients = np.random.choice(potential_test_patients, n_test)




#%% Label patient/visit with train/test status

# Set Adas
df_adas['patient_split'] = 'train'
df_adas['visit_split'] = 'train'


# Set label to patient
df_pet['patient_split'] = 'train'
df_pet.loc[(test_patients,),'patient_split'] = 'test'

# Set label to visits
def set_label_last_visit(df):
    df['visit_split'][-1] = 'test'
    return df
df_pet['visit_split'] = 'train'
df_pet[df_pet['patient_split'] == 'test'] = df_pet[df_pet['patient_split'] == 'test'].groupby('id').apply(set_label_last_visit)

#
ids_pet = df_pet[df_pet['visit_split'] == 'train'].index.get_level_values('id')
times_pet = df_pet[df_pet['visit_split'] == 'train'].index.get_level_values('time')
values_pet = df_pet[df_pet['visit_split'] == 'train'].values[:,:-2]

ids_adas = df_adas.index.get_level_values('id')
times_adas = df_adas.index.get_level_values('time')
values_adas = df_adas.values[:,:-2]


df_pet.to_csv(os.path.join(results_dir, name_experiment,'df_pet.csv'))
df_adas.to_csv(os.path.join(results_dir, name_experiment,'df_adas.csv'))


#%%

# Choose fold

# Adas
adas_train_dataset_ = LongitudinalScalarDataset(list(ids_adas),
                                          list(values_adas),
                                          list(times_adas))


# Pet
pet_train_dataset_ = LongitudinalScalarDataset(list(ids_pet),
                                          list(values_pet),
                                          list(times_pet),
                                         ages_std=adas_train_dataset_.ages_std,
                                         ages_mean=adas_train_dataset_.ages_mean)



#%%


# Multimodal dataset
train_dataset_multimodal = MultimodalDataset([adas_train_dataset_, pet_train_dataset_], ['adas_scores', 'pet_scores'],
                                  [DatasetTypes.SCALAR, DatasetTypes.PET])


# Only PET Dataset
train_dataset_pet = MultimodalDataset([pet_train_dataset_], ['pet_scores'],
                                  [DatasetTypes.PET])




#%%


# Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors


pet_info = (DatasetTypes.PET, 32, 32, data_dim_pet,
                   ['Y_{0}'.format(i) for i in range(data_dim_pet)],
                   ['r' for i in range(data_dim_pet)])
adas_info = (DatasetTypes.SCALAR, 16, 16, data_dim_adas,
                     ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b'])

data_info_multimodal = {'adas_scores': adas_info, 'pet_scores': pet_info}
data_info_pet = {'pet_scores': pet_info}



output_dir = os.path.join(results_dir,name_experiment)
print('Output directory', output_dir)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

output_dir_pet = os.path.join(output_dir, 'pet')
output_dir_multimodal = os.path.join(output_dir, 'multimodal')

if not os.path.isdir(output_dir_pet):
    os.mkdir(output_dir_pet)

if not os.path.isdir(output_dir_multimodal):
    os.mkdir(output_dir_multimodal)

train_dataset_pet.save(os.path.join(output_dir_pet, 'train_dataset.p'))
train_dataset_multimodal.save(os.path.join(output_dir_multimodal, 'train_dataset.p'))



#%% Launch

random_slope_model_multimodal = RandomSlopeModel(
    data_info=data_info_multimodal,
    latent_space_dim=latent_space_dim,
    pre_encoder_dim=pre_encoder_dim,
    random_slope=random_slope,
    variational=False,
    use_cuda=False,
    atlas_path=atlas_path
)


random_slope_model_pet = RandomSlopeModel(
    data_info=data_info_pet,
    latent_space_dim=latent_space_dim,
    pre_encoder_dim=pre_encoder_dim,
    random_slope=random_slope,
    variational=False,
    use_cuda=False,
    atlas_path=atlas_path
)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model, dataset, output_dir):
    save_dataset_info(dataset, model, 'train', output_dir)



#%% Launch longitudinal_models





estimate_random_slope_model_variational(random_slope_model_multimodal, train_dataset_multimodal, n_epochs=n_epochs,
                                        learning_rate=1e-3, output_dir=output_dir_multimodal,
                                        batch_size=32, save_every_n_iters=100,
                                        call_back=lambda x: call_back(x, train_dataset_multimodal, output_dir_multimodal),
                                        lr_decay=0.98,
                                        l=1., estimate_noise=True,
                                        randomize_nb_obs=True)


estimate_random_slope_model_variational(random_slope_model_pet, train_dataset_pet, n_epochs=n_epochs,
                                        learning_rate=1e-3, output_dir=output_dir_pet,
                                        batch_size=32, save_every_n_iters=100,
                                        call_back=lambda x: call_back(x, train_dataset_pet, output_dir_pet),
                                        lr_decay=0.98,
                                        l=1., estimate_noise=True,
                                        randomize_nb_obs=True)


