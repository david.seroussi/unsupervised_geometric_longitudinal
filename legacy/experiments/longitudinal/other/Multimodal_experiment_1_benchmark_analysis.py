from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from torch.utils.data import DataLoader
import pandas as pd
import torch

torch.manual_seed(0)
np.random.seed(0)


"""
Experiment 1 : Can we better predict future PET with the help of cognitive scores ? (only train set)

We remove the last PET on 20% of the training subjects (only subjects that have at least 3 PETS).
We train on this dataset.
We test the performance on the last PET that we removed.


Thus :
-Separate train/test set
-Launch PET-only model / PET-Adas model on train set
-Assess results
"""




#%% utils

def load_text_output(output_dir, output_type, keys, folds=['train']):
    res = {'train': {}, 'test': {}}

    for datasubset in folds:
        for key in keys:
            key_name = key
            if key == '':
                key_name = 'latent'
            res[datasubset][key_name] = np.loadtxt(
                os.path.join(output_dir, '{0}_{1}{2}.txt'.format(datasubset, key, output_type)))
    return res


def load_output(output_dir, keys, splits=['train']):
    # Load .txt
    residuals = load_text_output(output_dir, '_residuals', keys)
    latent_positions = load_text_output(output_dir, 'latent_positions', [''])
    latent_trajectories = load_text_output(output_dir, 'latent_trajectories', [''])

    # Load model
    model = load_model(os.path.join(output_dir, 'model'))

    return residuals, latent_positions, latent_trajectories, model



def pop_visit(data, visit_idx, key_target='pet_scores'):
    # Pop visit
    data_popped = dict.fromkeys(data.keys())
    visit = dict.fromkeys(data.keys())

    for key in data.keys():
        if (key != 'idx'):
            visit[key] = dict.fromkeys(data[key].keys())
            data_popped[key] = dict.fromkeys(data[key].keys())

    for key in data:
        if (key=='idx'):
            pass
        elif (key==key_target):
            # Create the visit
            visit[key]['times'] = data[key]['times'][visit_idx,:].unsqueeze(0).clone().unsqueeze(0)
            visit[key]['values'] = data[key]['values'][visit_idx,:].unsqueeze(0).clone().unsqueeze(0)
            visit[key]['ages'] = data[key]['ages'][visit_idx].unsqueeze(0).clone()
            visit[key]['idx'] = data[key]['idx']

            # Pop the visit
            data_popped[key]['times'] = torch.cat([data[key]['times'][:visit_idx,:],
                                                     data[key]['times'][1+visit_idx:, :]], dim = 0).clone().unsqueeze(0)
            data_popped[key]['values'] = torch.cat([data[key]['values'][:visit_idx,:],
                                                     data[key]['values'][1+visit_idx:, :]], dim = 0).clone().unsqueeze(0)
            data_popped[key]['ages'] = torch.cat([data[key]['ages'][:visit_idx],
                                                     data[key]['ages'][1+visit_idx:]], dim = 0).clone()

            data_popped[key]['idx'] = data[key]['idx']

            # Set the overall idx
            visit['idx'] = visit[key]['idx']
            data_popped['idx'] = data_popped[key]['idx']
        else:
            # The popped dataset has all the adas
            data_popped[key]['times'] = data[key]['times'].clone().unsqueeze(0)
            data_popped[key]['values'] = data[key]['values'].clone().unsqueeze(0)
            data_popped[key]['ages'] = data[key]['ages'].clone()
            data_popped[key]['idx'] = data[key]['idx']

            visit[key]['times'] = data[key]['times'].clone().unsqueeze(0)
            visit[key]['values'] = data[key]['values'].clone().unsqueeze(0)
            visit[key]['ages'] = data[key]['ages'].clone()
            visit[key]['idx'] = data[key]['idx']

            # Popped visit has all the adas (useless)

    return data_popped, visit


def compute_losses(data, data_popped, visit, visit_idx, predicted_values, type='extrapolate', key_target='pet_scores'):

    loss_model = np.sum(np.square(predicted_values[key_target].data.numpy()-visit[key_target]['values'].data.numpy().reshape(1,-1)))/120


    # Loss forward
    loss_forward = np.sum(np.square(data[key_target]['values'][visit_idx-1].data.numpy().reshape(1,-1) -
                                 visit[key_target]['values'].data.numpy().reshape(1, -1))) / 120

    loss_linearinterpolation = np.nan
    if (type == 'interpolate'):
        # Loss linear interpolation
        diff_y = data[key_target]['values'][visit_idx+1]-data[key_target]['values'][visit_idx-1]
        diff_x = data[key_target]['times'][visit_idx+1]-data[key_target]['times'][visit_idx-1]
        slope = diff_y/diff_x
        intercept = data[key_target]['values'][visit_idx-1]-slope*data[key_target]['times'][visit_idx-1]
        linear_interpolation = intercept + slope * data[key_target]['times'][visit_idx]
        loss_linearinterpolation = np.sum(np.square(linear_interpolation.data.numpy().reshape(1, -1) -
                                                    visit[key_target]['values'].data.numpy().reshape(1, -1))) / 120


    elif (type=='extrapolate'):
        loss_linearinterpolation = np.nan
        """
        # Extrapolate linearly from the 2 last visits
        diff_y = data[key_target]['values'][visit_idx-1]-data[key_target]['values'][visit_idx-2]
        diff_x = data[key_target]['times'][visit_idx-1]-data[key_target]['times'][visit_idx-2]
        slope = diff_y/diff_x
        intercept = data[key_target]['values'][visit_idx-1]-slope*data[key_target]['times'][visit_idx-1]
        linear_interpolation = intercept + slope * data[key_target]['times'][visit_idx]
        loss_linearinterpolation = np.sum(np.square(linear_interpolation.data.numpy().reshape(1, -1) -
                                                    visit[key_target]['values'].data.numpy().reshape(1, -1))) / 120
        """

        """
        # Linear Regression on all components of the pet
        from sklearn.linear_model import LinearRegression
        time_to_predict = data[key_target]['times'][visit_idx].data.numpy().reshape(-1,1)

        n_dim = data[key_target]['values'].shape[1]
        X = data_popped[key_target]['times'].squeeze(0).data.numpy()
        pred = []
        for dim in range(n_dim):
            y = data_popped[key_target]['values'].data.numpy()[:,:,dim]
            reg = LinearRegression().fit(X, y.T)
            pred.append(reg.predict(time_to_predict))
        linear_interpolation = torch.FloatTensor(pred)
        loss_linearinterpolation = np.sum(np.square(linear_interpolation.data.numpy().reshape(1, -1) -
                                                    visit[key_target]['values'].data.numpy().reshape(1, -1))) / 120
        """
    elif (type == 'extrapolate_lastfromfirst'):
        loss_linearinterpolation = np.nan

        loss_forward = np.sum(np.square(data[key_target]['values'][0].data.numpy().reshape(1, -1) -
                                        visit[key_target]['values'].data.numpy().reshape(1, -1))) / 120

    else:
        print('Type not known')



    return loss_model, loss_forward, loss_linearinterpolation





def compute_loss_from_modeldataset(model, dataset, pet_test_patients, type = 'extrapolate', key_target='pet_scores'):
    np.random.seed(0)
    total_loss = {'model':0,'forward':0,'interpolate':0}

    counter = 0

    for data in dataset:
        if (data['idx'] in pet_test_patients):

            if data[key_target]['values'].size()[0]>2:
                counter += 1

                if type =='extrapolate':
                    visit_idx = data[key_target]['times'].size()[0]-1
                elif type == 'interpolate':
                    visit_idx = np.random.randint(1, data[key_target]['times'].size()[0]-1)
                elif type == 'extrapolate_lastfromfirst':
                    visit_idx = data[key_target]['times'].size()[0] - 1

                else:
                    print('Case Not Handled')
                    visit_idx = np.nan

                # Compute model
                if (type == 'extrapolate_lastfromfirst'):
                    _, visit_0 = pop_visit(data, 0, key_target=key_target)
                    _, visit = pop_visit(data, visit_idx, key_target=key_target)
                    z = model.encode(visit_0)
                    data_popped = None
                else:
                    data_popped, visit = pop_visit(data, visit_idx, key_target=key_target)
                    z = model.encode(data_popped)
                latent_trajectories = model.get_latent_trajectories(visit, z)
                predicted_values = model.decode(latent_trajectories)

                # Compute losses
                loss_model, loss_forward, loss_linearinterpolation = compute_losses(data, data_popped,
                                                                                    visit, visit_idx, predicted_values,
                                                                                    type=type, key_target=key_target)
                total_loss['model'] += loss_model
                total_loss['forward'] += loss_forward
                total_loss['interpolate'] += loss_linearinterpolation

    total_loss['model'] /= counter
    total_loss['forward'] /= counter
    total_loss['interpolate'] /= counter


    return(total_loss)



def compute_loss_from_fold(fold, results_dir, type='extrapolate', split = 'test', key_target='pet_scores'):

    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'multimodal')
    output_dir_pet= os.path.join(directory, 'pet')
    output_dir_adas = os.path.join(directory, 'adas')

    keys = ['adas_scores', 'pet_scores']

    # Get test patients that have key_target
    test_patients = experiment.folds[fold][split]
    if key_target == 'pet_scores':
        modality_patients = experiment.distinct_rids[np.isin(experiment.groups, [0, 1])]
    elif key_target == 'adas_scores':
        modality_patients = experiment.distinct_rids[np.isin(experiment.groups, [0, 2])]
    else:
        print('Keytarget not known')
    modality_test_patients = np.intersect1d(modality_patients, test_patients)


    # Load the datasets
    dataset_multimodal_test, dataset_pet_test, dataset_adas_test = experiment.generate_datasets_from_fold(fold, split)

    # Load multimodal model and compute loss for given target scores
    _, _, _, model_multimodal = load_output(output_dir_multimodal,  keys,['train'])
    fold_loss_multimodal = compute_loss_from_modeldataset(model_multimodal, dataset_multimodal_test,
                                                          modality_test_patients, type=type, key_target=key_target)

    # Load the unimodal model

    if key_target=='pet_scores':
        output_dir_unimodal = output_dir_pet
        dataset_unimodal = dataset_pet_test
    elif key_target=='adas_scores':
        output_dir_unimodal = output_dir_adas
        dataset_unimodal = dataset_adas_test
    else:
        print('Keytarget not known')

    _, _, _, model_unimodal = load_output(output_dir_unimodal, [key_target], ['train'])
    fold_loss_unimodal = compute_loss_from_modeldataset(model_unimodal, dataset_unimodal,
                                                        modality_test_patients, type=type, key_target=key_target)

    fold_loss = {}
    fold_loss[key_target] = fold_loss_unimodal
    fold_loss['multimodal'] = fold_loss_multimodal
    return fold_loss


def get_mse_from_fold(directory, fold):

    output_dir_multimodal = os.path.join(directory, 'multimodal')
    output_dir_pet = os.path.join(directory, 'pet')
    output_dir_adas = os.path.join(directory, 'adas')

    dataset_multimodal_test, dataset_pet_test, dataset_adas_test = experiment.generate_datasets_from_fold(fold, 'test')
    dataset_multimodal_train, dataset_pet_train, dataset_adas_train = experiment.generate_datasets_from_fold(fold, 'train')

    _, _, _, model_multimodal = load_output(output_dir_multimodal, ['pet_scores', 'adas_scores'], ['train'])
    _, _, _, model_pet = load_output(output_dir_pet, ['pet_scores'], ['train'])
    _, _, _, model_adas = load_output(output_dir_adas, ['adas_scores'], ['train'])

    models = {'multimodal' : model_multimodal, 'pet' : model_pet, 'adas': model_adas}
    datasets = {'multimodal' : {'train' : dataset_multimodal_train, 'test': dataset_multimodal_test},
                'pet' : {'train' : dataset_pet_train, 'test': dataset_pet_test},
                'adas' : {'train' : dataset_adas_train, 'test': dataset_adas_test}}

    splits = ['train', 'test']
    mse = {}
    for model_name in models:
        model = models[model_name]

        mse[model_name] = {}

        for key in model.data_info.keys():
            mse[model_name][key] = {}
            for split in splits:
                dataloader = DataLoader(datasets[model_name][split])
                mse[model_name][key][split] = np.mean(model.compute_residuals(dataloader)[1][key])

    return mse





#%%
import pickle
import os
n_runs = 40

name = 'experiment_16-nodropout-weightdecay-first300adas'


# Paths
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
adas_path = '../data/data_adas/df.csv'
results_dir = '../output/benchmark_multimodal_{0}/'.format(name)

# Load experiment
experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))

# Types
types = ['interpolate','extrapolate','extrapolate_lastfromfirst']


#%% Reconstruction error on train/test
# Do we generalize well ?




# Load datasets

mse_multimodals = pd.DataFrame()
mse_pets = pd.DataFrame()
mse_adass = pd.DataFrame()

for fold in range(n_runs):
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    mse = get_mse_from_fold(directory, fold)
    mse_multimodal = pd.DataFrame.from_dict(mse['multimodal'], orient = 'index').stack()
    mse_pet = pd.DataFrame.from_dict(mse['pet'], orient = 'index').stack()
    mse_adas = pd.DataFrame.from_dict(mse['adas'], orient='index').stack()

    mse_multimodals = pd.concat([mse_multimodals, mse_multimodal], axis = 1)
    mse_pets = pd.concat([mse_pets, mse_pet], axis = 1)
    mse_adass = pd.concat([mse_adass, mse_adas], axis=1)



#  Plot the mse
import matplotlib.pyplot as plt

fig, axes = plt.subplots(1,3,figsize=(10,6), sharey=True)
axes[0].set_title('Overall mse multimodal model')
axes[1].set_title('Overall mse pet model')
axes[2].set_title('Overall mse adas model')
mse_multimodals.T.boxplot(ax = axes[0])
mse_pets.T.boxplot(ax = axes[1])
mse_adass.T.boxplot(ax = axes[2])
for ax in axes:
    ax.set_xticklabels(ax.get_xticklabels(), rotation=20, size = 8)
plt.savefig(os.path.join(results_dir, 'figures','Overall_mse.pdf'))
plt.show()
plt.close()



#%% ## Evaluate interpolation / Extrapolation tasks


key_targets = [ 'adas_scores', 'pet_scores']

for key_target in key_targets:
    for type in types:
        # In test
        df_results_test = pd.DataFrame()
        for fold in range(n_runs):
            print(fold)
            res = compute_loss_from_fold(fold, results_dir, type, split='test', key_target=key_target)
            df_res = pd.DataFrame(res).unstack()
            df_results_test = pd.concat([df_results_test, df_res], axis=1)
        df_results_test.columns = list(range(n_runs))

        # In Train
        df_results_train = pd.DataFrame()
        for fold in range(n_runs):
            print(fold)
            res = compute_loss_from_fold(fold, results_dir, type, split='train', key_target=key_target)
            df_res = pd.DataFrame(res).unstack()
            df_results_train = pd.concat([df_results_train, df_res], axis=1)
        df_results_train.columns = list(range(n_runs))

        # Merge 2 datasets

        import matplotlib.pyplot as plt

        df_train = df_results_train.T.dropna(axis=1)
        df_train.columns = df_train.columns.map('|'.join)

        df_test = df_results_test.T.dropna(axis=1)
        df_test.columns = df_test.columns.map('|'.join)

        positions = np.array(list(range(df_train.shape[1])))

        a = plt.boxplot(df_train.values, positions = positions-0.25)
        b = plt.boxplot(df_test.values, positions = positions+0.25)

        plt.xticks(range(df_train.shape[1]), df_train.columns.values, rotation=20)
        plt.xlim(-0.6,df_train.shape[1]-0.5)
        plt.title('Train/Test Performance for task : {0}_target_{1}'.format(type, key_target))
        plt.savefig(os.path.join(results_dir, 'figures','performance_boxplot_train-test_task_{0}_target_{1}.pdf'.format(type,key_target)))
        plt.show()
        plt.close()



#%% Visualization

df_results = df_results_test

import matplotlib.pyplot as plt

# Boxplots of raw results

fig, ax = plt.subplots(1,1, figsize=(10,6))

df_results.T.boxplot(ax=ax)
plt.xticks(rotation=45)
plt.title('{0} task'.format(type))
plt.tight_layout()
plt.savefig(os.path.join(results_dir, 'figures','raw_boxplots_{0}.pdf'.format(type)))
plt.show()
plt.close()

# Boxplots of difference in results

diff_petmultimodal = df_results.loc['multimodal'].loc['model']-df_results.loc['pet_scores'].loc['model']
diff_forwardmultimodal = df_results.loc['multimodal'].loc['model']-df_results.loc['pet_scores'].loc['forward']
diff_forwardpet = df_results.loc['pet_scores'].loc['model']-df_results.loc['pet_scores'].loc['forward']

fig, ax = plt.subplots(1,1,figsize=(10,6))
ax.boxplot([diff_petmultimodal,
            diff_forwardmultimodal,
            diff_forwardpet])
ax.set_title('{0} task'.format(type))
plt.legend(['1 : Pet vs Multimodal', '2 : Multimodal - Forward', '3 : Pet - Forward'])
ax.plot([-10,10],[0,0], c = 'black')
plt.tight_layout()
plt.savefig(os.path.join(results_dir, 'figures','difference_boxplots_{0}.pdf'.format(type)))
plt.show()
plt.close()


#%% Mann Whitney Test

import scipy

test = scipy.stats.mannwhitneyu(df_results.loc[('multimodal','model')],
                         df_results.loc[('pet_scores', 'model')])

ttest = scipy.stats.ttest_rel(df_results.loc[('multimodal','model')],
                         df_results.loc[('pet_scores', 'model')])

print("Mann Whitney U test p-value for task {0} : {1}".format(type,test[1]))
print("Paired ttest p-value for task {0} : {1}".format(type,ttest[1]))



#%% Average trajectory


import numpy as np
import pickle
import os
from torch.utils.data import DataLoader
import pandas as pd
import torch
from models.longitudinal_models.abstract_model import load_model
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from multimodal_dataset import MultimodalDataset, DatasetTypes

torch.manual_seed(0)
np.random.seed(0)


n_runs = 40

name = 'experiment_16-nodropout-weightdecay-first300adas'

# Paths
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
adas_path = '../data/data_adas/df.csv'
results_dir = '../output/benchmark_multimodal_{0}/'.format(name)

# Load experiment
experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))

#%%

# Overall mean/std age
df_adas = pd.read_csv(adas_path, header = None).set_index([0,1])
df_pet = pd.read_csv(pet_path, header = None).set_index([0,1])
mean_age = np.mean(df_adas.index.get_level_values(1))
std_age = np.std(df_adas.index.get_level_values(1))

# PCA
pca = PCA(n_components=1)
pca.fit(df_pet.values)


# Plot
fig, axes = plt.subplots(2,1,figsize=(8,10))

for fold in np.random.choice(list(range(n_runs)),30):
    pass

for fold in range(n_runs):

    colors = ['blue', 'cyan', 'darkgreen', 'darkorange','red','brown']

    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'multimodal')
    output_dir_pet = os.path.join(directory, 'pet')

    keys = ['adas_scores', 'pet_scores']
    dataset_multimodal, _, _ = experiment.generate_datasets_from_fold(fold, 'train')
    _, _, _, model = load_output(output_dir_multimodal,  keys,['train'])

    min_x, max_x = np.quantile(dataset_multimodal.datasets[0].times,[0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)
    age_min_x, age_max_x = min_x*std_age+mean_age, max_x*std_age+mean_age


    adas_trajectory = mean_traj['adas_scores'].data.numpy()
    pet_trajectory = pca.components_.dot(mean_traj['pet_scores'].data.numpy().T)

    for dim in range(adas_trajectory.shape[1]):
        axes[0].plot(np.linspace(age_min_x, age_max_x, adas_trajectory.shape[0]).reshape(-1,1),
                     adas_trajectory[:,dim], c=colors[dim])
    for dim in range(pet_trajectory.shape[0]):
        axes[1].plot(np.linspace(age_min_x, age_max_x, pet_trajectory.shape[1]).reshape(-1,1),
                     pet_trajectory.T[:,dim], c = colors[dim])

    axes[0].set_title('Adas mean trajectory')
    axes[1].set_title('Pet (PCA) mean trajectory')

plt.savefig(os.path.join(results_dir, 'figures','mean_trajectories.pdf'))
plt.show()


#%% Interact with the source ???

fold = 3
dimension = 2

for dimension in range(1, experiment.nn_parameters[0]):

    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'multimodal')
    output_dir_pet = os.path.join(directory, 'pet')

    keys = ['adas_scores', 'pet_scores']
    dataset_multimodal, _, _ = experiment.generate_datasets_from_fold(fold, 'train')
    _, _, _, model = load_output(output_dir_multimodal, keys, ['train'])

    min_x, max_x = np.quantile(dataset_multimodal.datasets[0].times, [0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)


    def get_mean_trajectory_source(model, min_x, max_x, source_value, dimension=1):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        x_out = {}
        mean_traj = {}
        for key, decoder in model.decoders.items():
            dataset_type, _, _, _, _, _ = model.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            x_np = np.linspace(min_x, max_x, nb_points)
            x_torch = torch.from_numpy(x_np).type(model.type)
            source = np.zeros(model.latent_space_dim)
            source[dimension]=source_value
            latent_traj_np = np.array(
                [source + model.mean_slope.cpu().detach().numpy() * t for t in x_np])
            latent_traj = torch.from_numpy(latent_traj_np).type(model.type)
            x_out[key] = x_torch
            mean_traj[key] = decoder(model.pre_decoder(latent_traj))
        return x_out, mean_traj


    fig, axes = plt.subplots(2,1,figsize=(8,10))

    for source_value in np.linspace(0,1,10):

        x_out, mean_traj = get_mean_trajectory_source(model,min_x=min_x, max_x=max_x,source_value=source_value,dimension=dimension)

        adas_trajectory = mean_traj['adas_scores'].data.numpy()
        pet_trajectory = pca.components_.dot(mean_traj['pet_scores'].data.numpy().T)

        for dim in range(adas_trajectory.shape[1]):
            axes[0].plot(np.linspace(age_min_x, age_max_x, adas_trajectory.shape[0]).reshape(-1, 1),
                         adas_trajectory[:, dim], c=colors[dim], linewidth = 1-source_value)
        for dim in range(pet_trajectory.shape[0]):
            axes[1].plot(np.linspace(age_min_x, age_max_x, pet_trajectory.shape[1]).reshape(-1, 1),
                         pet_trajectory.T[:, dim], c=colors[dim], linewidth = 1-source_value)

    axes[0].set_title('Adas mean trajectory with varying source in dimension {0}'.format(dimension))
    axes[1].set_title('Pet (PCA) mean trajectory with varying source in dimension {0}'.format(dimension))
    plt.savefig(os.path.join(results_dir, 'figures','mean_trajectories_vith_varying_source{0}_fold{1}.pdf'.format(dimension, fold)))
    plt.show()




#%% Plot mean and sources with the most impacted PET

# Get the most impacted PET (on PET Model)

petvars = []

for fold in range(n_runs):
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)

    petvar = np.load(os.path.join(directory, 'pet','pet_scores_var_rate.npy'))
    petvars.append(petvar)

petvars = np.array(petvars)

#%%

pos = np.argsort(np.mean(petvars, axis = 0))[0:5]



#%%

# Overall mean/std age
df_adas = pd.read_csv(adas_path, header = None).set_index([0,1])
df_pet = pd.read_csv(pet_path, header = None).set_index([0,1])
mean_age = np.mean(df_adas.index.get_level_values(1))
std_age = np.std(df_adas.index.get_level_values(1))


# Plot
fig, axes = plt.subplots(2,1,figsize=(8,10))


for fold in range(n_runs):

    colors = ['blue', 'cyan', 'darkgreen', 'darkorange','red','brown']

    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'multimodal')
    output_dir_pet = os.path.join(directory, 'pet')

    keys = ['adas_scores', 'pet_scores']
    dataset_multimodal, _, _ = experiment.generate_datasets_from_fold(fold, 'train')
    _, _, _, model = load_output(output_dir_multimodal,  keys,['train'])

    min_x, max_x = np.quantile(dataset_multimodal.datasets[0].times,[0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)
    age_min_x, age_max_x = min_x*std_age+mean_age, max_x*std_age+mean_age


    adas_trajectory = mean_traj['adas_scores'].data.numpy()
    pet_trajectory = mean_traj['pet_scores'].data.numpy()[:,pos].T

    for dim in range(adas_trajectory.shape[1]):
        axes[0].plot(np.linspace(age_min_x, age_max_x, adas_trajectory.shape[0]).reshape(-1,1),
                     adas_trajectory[:,dim], c=colors[dim])
    for dim in range(pet_trajectory.shape[0]):
        axes[1].plot(np.linspace(age_min_x, age_max_x, pet_trajectory.shape[1]).reshape(-1,1),
                     pet_trajectory.T[:,dim], c = colors[dim])

    axes[0].set_title('Adas mean trajectory')
    axes[1].set_title('Pet (PCA) mean trajectory')

plt.savefig(os.path.join(results_dir, 'figures','mean_trajectories_mostImpactedPET.pdf'))
plt.show()


#%% Interact with the source ???

fold = 0
dimension = 2

for dimension in range(1, experiment.nn_parameters[0]):

    name_dir = 'fold_{0}'.format(fold)
    name_dir = 'fold_all2'
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'multimodal')
    output_dir_pet = os.path.join(directory, 'pet')

    keys = ['adas_scores', 'pet_scores']
    dataset_multimodal, _, _ = experiment.generate_datasets_from_fold(fold, 'train')
    _, _, _, model = load_output(output_dir_multimodal, keys, ['train'])

    min_x, max_x = np.quantile(dataset_multimodal.datasets[0].times, [0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)


    def get_mean_trajectory_source(model, min_x, max_x, source_value, dimension=1):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        x_out = {}
        mean_traj = {}
        for key, decoder in model.decoders.items():
            dataset_type, _, _, _, _, _ = model.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            x_np = np.linspace(min_x, max_x, nb_points)
            x_torch = torch.from_numpy(x_np).type(model.type)
            source = np.zeros(model.latent_space_dim)
            source[dimension]=source_value
            latent_traj_np = np.array(
                [source + model.mean_slope.cpu().detach().numpy() * t for t in x_np])
            latent_traj = torch.from_numpy(latent_traj_np).type(model.type)
            x_out[key] = x_torch
            mean_traj[key] = decoder(model.pre_decoder(latent_traj))
        return x_out, mean_traj


    fig, axes = plt.subplots(2,1,figsize=(8,10))

    for source_value in np.linspace(0,1,10):

        x_out, mean_traj = get_mean_trajectory_source(model,min_x=min_x, max_x=max_x,source_value=source_value,dimension=dimension)

        adas_trajectory = mean_traj['adas_scores'].data.numpy()
        pet_trajectory = mean_traj['pet_scores'].data.numpy()[:,pos].T

        for dim in range(adas_trajectory.shape[1]):
            axes[0].plot(np.linspace(age_min_x, age_max_x, adas_trajectory.shape[0]).reshape(-1, 1),
                         adas_trajectory[:, dim], c=colors[dim], linewidth = 1-source_value)
        for dim in range(pet_trajectory.shape[0]):
            axes[1].plot(np.linspace(age_min_x, age_max_x, pet_trajectory.shape[1]).reshape(-1, 1),
                         pet_trajectory.T[:, dim], c=colors[dim], linewidth = 1-source_value)

    axes[0].set_title('Adas mean trajectory with varying source in dimension {0}'.format(dimension))
    axes[1].set_title('Pet (PCA) mean trajectory with varying source in dimension {0}'.format(dimension))
    plt.savefig(os.path.join(results_dir, 'figures','mean_trajectories_mostPET_vith_varying_source{0}_fold{1}.pdf'.format(dimension, fold)))
    plt.show()


#%% See the Tau and Ksi

fig, axes = plt.subplots(2,1,figsize=(20,10))

import numpy as np

df_ksis = pd.DataFrame()
df_taus = pd.DataFrame()

for fold in range(n_runs):

    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'pet')

    latent_positions = np.loadtxt(os.path.join(output_dir_multimodal, 'train_latent_positions.txt'))
    ids = experiment.folds[fold]['train']

    df_tau = pd.DataFrame({'ID' : ids, 'Tau_{0}'.format(fold) : latent_positions[:,0]}).set_index('ID')
    df_ksi = pd.DataFrame({'ID': ids, 'Ksi_{0}'.format(fold): latent_positions[:, 3]}).set_index('ID')

    df_taus = pd.merge(df_taus, df_tau, how = 'outer', right_index=True, left_index=True)
    df_ksis = pd.merge(df_ksis, df_ksi, how='outer', right_index=True, left_index=True)


pd.plotting.scatter_matrix(df_taus.iloc[:, 0:4])
plt.show()
pd.plotting.scatter_matrix(df_ksis.iloc[:, 0:4])
plt.show()

#%%

df_taus_normalized = (df_taus-df_taus.mean())/df_taus.std()

pd.plotting.scatter_matrix(df_taus_normalized.iloc[:,0:4])
plt.show()