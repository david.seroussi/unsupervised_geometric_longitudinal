import os
import numpy as np
from sklearn.model_selection import GroupShuffleSplit
import pandas as pd
import torch
from multimodal_dataset import load_multimodal_dataset
from models.longitudinal_models.abstract_model import RandomSlopeModel, load_model

torch.manual_seed(0)
np.random.seed(0)


"""
Experiment 1 : Can we better predict future PET with the help of cognitive scores ? (only train set)

We remove the last PET on 20% of the training subjects (only subjects that have at least 3 PETS).
We train on this dataset.
We test the performance on the last PET that we removed.


Thus :
-Separate train/test set
-Launch PET-only model / PET-Adas model on train set
-Assess results
"""




#%% utils

def load_text_output(output_dir, output_type, keys, folds=['train']):
    res = {'train': {}, 'test': {}}

    for datasubset in folds:
        for key in keys:
            key_name = key
            if key == '':
                key_name = 'latent'
            res[datasubset][key_name] = np.loadtxt(
                os.path.join(output_dir, '{0}_{1}{2}.txt'.format(datasubset, key, output_type)))
    return res


def load_output(output_dir, keys, folds=['train']):
    # Load .txt
    residuals = load_text_output(output_dir, '_residuals', keys)
    latent_positions = load_text_output(output_dir, 'latent_positions', [''])
    latent_trajectories = load_text_output(output_dir, 'latent_trajectories', [''])

    # Load model
    model = load_model(os.path.join(output_dir, 'model'))

    # Datasets
    datasets = {}
    for datasubset in folds:
        datasets[datasubset] = load_multimodal_dataset(os.path.join(output_dir, '{0}_dataset.p'.format(datasubset)))
    return residuals, latent_positions, latent_trajectories, datasets, model




def compute_model_test_loss(directory, output_dir, keys):
    folds = ['train']
    _, _, _, datasets, model = load_output(output_dir, keys, folds)
    mean_age, std_age = datasets['train'].ages_mean, datasets['train'].ages_std

    df_pet = pd.read_csv(os.path.join(directory, 'df_pet.csv')).set_index(['id', 'time'])
    df_adas = pd.read_csv(os.path.join(directory, 'df_adas.csv')).set_index(['id', 'time'])

    patients_test = df_pet[df_pet['patient_split'] == 'test'].index.unique('id')

    total_loss_last = 0
    total_loss_model = 0
    for patient in patients_test:
        loss_model, loss_last = compute_patient_loss(model, patient, df_adas, df_pet, mean_age, std_age, keys)
        total_loss_model += loss_model
        total_loss_last += loss_last
    return total_loss_model/patients_test.shape[0], total_loss_last/patients_test.shape[0]



def compute_patient_loss(model, idx, df_adas, df_pet, mean_age, std_age, keys):

    idx_visit = -1

    df_patient = {'adas_scores': df_adas.loc[idx], 'pet_scores': df_pet.loc[idx].iloc[:idx_visit, :]}
    age_to_predict_patient = torch.FloatTensor([[df_pet.loc[idx].index[idx_visit]]])
    time_to_predict_patient = ((age_to_predict_patient-mean_age)/std_age).unsqueeze(0)

    #train_dataset, test_dataset = multimodal1, multimodal2
    #dataloader = Dataloader(train_dataset, test_dataset)

    data = {}
    for key in keys:
        data[key] = {'ages' : torch.from_numpy(np.array(df_patient[key].index.get_level_values('time'))).unsqueeze(0).type(torch.FloatTensor),
                     'idx' : torch.Tensor([idx]),
        'times' : torch.from_numpy(np.array((df_patient[key].index.get_level_values('time')-mean_age)/std_age)).unsqueeze(1).unsqueeze(0).type(torch.FloatTensor),
        'values' : torch.from_numpy(np.array(df_patient[key].values[:,:-2], dtype=np.float64)).unsqueeze(0).type(torch.FloatTensor)}
    data['idx']=torch.Tensor([idx])

    z = model.encode(data)
    data_containing_target_times = data
    data_containing_target_times['pet_scores']["times"] = time_to_predict_patient
    latent_trajectories = model.get_latent_trajectories(data_containing_target_times, z)
    predicted_values = model.decode(latent_trajectories)

    loss_model = np.sum(np.square(predicted_values['pet_scores'].data.numpy()-df_pet.loc[idx].values[idx_visit,:-2].reshape(1,-1)))/120
    loss_last = np.sum(np.square(df_pet.loc[idx].values[idx_visit, :-2].reshape(1, -1) -
                                 df_pet.loc[idx].values[idx_visit-1, :-2].reshape(1, -1))) / 120
    return loss_model, loss_last


#%%

# Paths
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
adas_path = '../data/data_adas/df.csv'

results_dir = '../output/output_test_multimodal/'
name_dir = 'output_multimodal_pet-adas_experiment_1_0'
directory = os.path.join(results_dir, name_dir)


#%% Load a model

multimodal_output_dir = os.path.join(directory, 'multimodal')
pet_output_dir = os.path.join(directory, 'pet')
keys_multimodal = ['adas_scores', 'pet_scores']
key_pet = ['pet_scores']
folds = ['train']

loss_multimodal = compute_model_test_loss(directory, multimodal_output_dir, keys_multimodal)
loss_pet = compute_model_test_loss(directory, pet_output_dir, ['pet_scores'])

print('model')
print(' loss multimodal : {0} \n loss pet :{1}'.format(loss_multimodal[0], loss_pet[0]))
print('last')
print(' loss multimodal : {0} \n loss pet :{1}'.format(loss_multimodal[1], loss_pet[1]))


