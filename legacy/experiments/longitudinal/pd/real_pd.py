import os
from datasets.longitudinal_image_dataset import LongitudinalImageDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_variational import estimate_random_slope_model_variational
import numpy as np
from datasets.multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import torch

#%% Parameters
torch.manual_seed(0)
np.random.seed(0)
use_cuda = True

#%% Load Data

#/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/MICCAI_2020/PD/Images/data_ppmi

data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/MICCAI_2020"
data_realdat_path = os.path.join(data_path, "PD", "Images", "data_ppmi")
results_dir = '../output/output_real_pd_debug_clean/'

if not os.path.exists(results_dir):
    os.makedirs(results_dir)

#%%
import pandas as pd
df_ppmi_imaging = pd.read_csv(os.path.join(data_realdat_path, "df_images.csv"))

distinct_rids = np.array(df_ppmi_imaging['Subject'])
ids = np.array(df_ppmi_imaging['Subject'])
times = np.array(df_ppmi_imaging['Age at Visit'])

images_path = []
for _, row in df_ppmi_imaging.iterrows():
    image_name = "PID{}_V{}_Img{}.npy".format(row['Subject'],
                                             row["Visit"],
                                             row["Image Data ID"])
    images_path.append(os.path.join(data_realdat_path, image_name))
images_path = np.array(images_path)

#%% Normalize Data
listx = []
images_path_normalized = []
for i, row in df_ppmi_imaging.iterrows():
    image_name = "PID{}_V{}_Img{}.npy".format(row['Subject'],
                                             row["Visit"],
                                             row["Image Data ID"])

    #x = np.load(os.path.join(data_realdat_path, image_name))

    # Transform x
    #x = np.abs(x)
    #x = x/32800

    image_name_normalized = "PID{}_V{}_Img{}_normalized.npy".format(row['Subject'],
                                             row["Visit"],
                                             row["Image Data ID"])

    images_path_normalized.append(os.path.join(data_realdat_path, image_name_normalized))
    #np.save(os.path.join(data_realdat_path, image_name_normalized), x)
images_path_normalized = np.array(images_path_normalized)


    #listx.append(np.load(os.path.join(data_realdat_path, image_name)))
    #if i>50:
    #    break

#%% Remove broken images for now

broken = [ 19,  36,  39,  74,  78,  85,  99, 102, 104, 109, 113, 117, 128,
       132, 159, 164, 167, 168, 185, 189, 193, 221, 222, 225, 235, 239,
       243, 250, 254, 258, 262, 266, 270, 274, 278, 282, 301]

okay = [   0,    1,    2,    3,    4,    5,    6,    7,    8,    9,   10,
         11,   12,   13,   14,   15,   16,   17,   18,   20,   21,   22,
         23,   24,   25,   26,   27,   28,   29,   30,   31,   32,   33,
         34,   35,   37,   38,   40,   41,   42,   43,   44,   45,   46,
         47,   48,   49,   50,   51,   52,   53,   54,   55,   56,   57,
         58,   59,   60,   61,   62,   63,   64,   65,   66,   67,   68,
         69,   70,   71,   72,   73,   75,   76,   77,   79,   80,   81,
         82,   83,   84,   86,   87,   88,   89,   90,   91,   92,   93,
         94,   95,   96,   97,   98,  100,  101,  103,  105,  106,  107,
        108,  110,  111,  112,  114,  115,  116,  118,  119,  120,  121,
        122,  123,  124,  125,  126,  127,  129,  130,  131,  133,  134,
        135,  136,  137,  138,  139,  140,  141,  142,  143,  144,  145,
        146,  147,  148,  149,  150,  151,  152,  153,  154,  155,  156,
        157,  158,  160,  161,  162,  163,  165,  166,  169,  170,  171,
        172,  173,  174,  175,  176,  177,  178,  179,  180,  181,  182,
        183,  184,  186,  187,  188,  190,  191,  192,  194,  195,  196,
        197,  198,  199,  200,  201,  202,  203,  204,  205,  206,  207,
        208,  209,  210,  211,  212,  213,  214,  215,  216,  217,  218,
        219,  220,  223,  224,  226,  227,  228,  229,  230,  231,  232,
        233,  234,  236,  237,  238,  240,  241,  242,  244,  245,  246,
        247,  248,  249,  251,  252,  253,  255,  256,  257,  259,  260,
        261,  263,  264,  265,  267,  268,  269,  271,  272,  273,  275,
        276,  277,  279,  280,  281,  283,  284,  285,  286,  287,  288,
        289,  290,  291,  292,  293,  294,  295,  296,  297,  298,  299,
        300,  302,  303,  304,  305,  306,  307,  308,  309,  310,  311,
        312,  313,  314,  315,  316,  317,  318,  319,  320,  321,  322,
        323,  324,  325,  326,  327,  328,  329,  331,  332,  333,  335,
        336,  337,  339,  340,  341,  342,  344,  345,  346,  347,  348,
        349,  350,  351,  352,  353,  354,  355,  356,  357,  358,  359,
        360,  361,  362,  363,  364,  365,  366,  368,  369,  370,  371,
        372,  373,  375,  376,  377,  379,  380,  381,  383,  384,  385,
        387,  388,  389,  391,  392,  393,  395,  396,  397,  399,  400,
        401,  402,  403,  404,  405,  406,  407,  408,  409,  410,  411,
        412,  413,  414,  415,  416,  418,  419,  420,  421,  422,  423,
        425,  426,  427,  428,  429,  430,  431,  432,  434,  435,  436,
        437,  438,  439,  440,  441,  442,  443,  444,  445,  446,  447,
        448,  449,  450,  451,  452,  453,  454,  455,  456,  457,  458,
        459,  460,  461,  462,  464,  465,  466,  468,  469,  470,  472,
        473,  474,  476,  477,  478,  480,  481,  482,  484,  485,  486,
        488,  489,  490,  492,  493,  494,  495,  496,  497,  498,  499,
        500,  502,  503,  504,  505,  506,  507,  508,  509,  510,  511,
        512,  513,  515,  516,  517,  519,  520,  521,  523,  524,  525,
        527,  528,  530,  531,  532,  533,  534,  535,  536,  537,  538,
        539,  540,  541,  542,  543,  545,  546,  547,  549,  550,  551,
        553,  554,  555,  556,  557,  558,  559,  560,  561,  563,  564,
        565,  566,  567,  568,  569,  570,  571,  572,  573,  574,  575,
        576,  578,  579,  580,  581,  582,  583,  584,  585,  586,  588,
        589,  590,  591,  592,  593,  594,  595,  596,  597,  599,  600,
        601,  603,  604,  605,  607,  608,  610,  611,  612,  613,  614,
        615,  616,  617,  618,  619,  620,  621,  622,  623,  624,  625,
        626,  627,  628,  629,  630,  631,  632,  633,  634,  635,  636,
        637,  639,  640,  642,  643,  644,  645,  646,  647,  648,  649,
        650,  651,  652,  653,  654,  655,  656,  657,  658,  659,  660,
        661,  663,  664,  665,  666,  667,  668,  670,  671,  674,  675,
        676,  678,  679,  680,  681,  682,  683,  684,  685,  686,  687,
        689,  690,  691,  692,  693,  694,  695,  696,  697,  698,  699,
        700,  701,  702,  703,  704,  706,  707,  709,  710,  711,  712,
        713,  714,  715,  716,  717,  718,  719,  720,  721,  722,  723,
        724,  725,  726,  727,  728,  729,  730,  731,  732,  733,  734,
        736,  737,  738,  739,  740,  741,  743,  744,  745,  747,  748,
        749,  751,  752,  753,  754,  755,  756,  757,  758,  759,  760,
        761,  762,  763,  764,  765,  766,  767,  768,  769,  770,  771,
        773,  774,  775,  777,  778,  779,  780,  781,  782,  784,  785,
        786,  788,  789,  790,  792,  793,  794,  796,  797,  798,  800,
        801,  802,  804,  805,  806,  808,  809,  810,  812,  813,  814,
        815,  816,  817,  818,  819,  820,  821,  822,  823,  824,  825,
        826,  827,  829,  830,  831,  832,  833,  834,  835,  836,  838,
        839,  840,  841,  842,  843,  844,  845,  846,  847,  849,  850,
        851,  853,  854,  855,  857,  858,  859,  860,  861,  862,  863,
        864,  865,  866,  867,  868,  869,  870,  871,  872,  873,  874,
        875,  876,  877,  878,  879,  881,  882,  883,  885,  886,  887,
        889,  890,  891,  893,  894,  895,  897,  898,  899,  901,  902,
        903,  904,  905,  906,  907,  908,  909,  910,  911,  913,  914,
        915,  916,  917,  918,  919,  921,  922,  923,  925,  926,  927,
        928,  929,  930,  931,  932,  933,  934,  935,  936,  937,  938,
        939,  940,  941,  942,  943,  944,  945,  946,  947,  948,  949,
        951,  952,  954,  955,  956,  957,  958,  960,  961,  962,  964,
        965,  966,  967,  968,  969,  970,  971,  972,  973,  974,  976,
        977,  978,  979,  980,  982,  983,  984,  985,  986,  987,  989,
        990,  991,  993,  994,  995,  997,  998,  999, 1001, 1002, 1003,
       1005, 1006, 1007, 1009, 1010, 1012, 1013, 1014, 1015, 1016, 1017,
       1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028,
       1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1038, 1039, 1040,
       1042, 1043, 1044, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053,
       1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064,
       1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075,
       1076, 1077, 1079, 1080, 1081, 1083, 1084, 1085, 1086, 1087, 1088,
       1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099,
       1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110,
       1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121,
       1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132,
       1133, 1134, 1135, 1136, 1137, 1138, 1139, 1141, 1142, 1143, 1144,
       1145, 1146, 1147, 1148, 1149, 1151, 1152, 1153, 1154, 1155, 1156,
       1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168,
       1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1180,
       1181, 1182, 1183, 1185, 1186, 1187, 1189, 1190, 1191, 1192]

#%%

ids = ids[okay]
images_path_normalized = images_path_normalized[okay]
times = times[okay]

#%% Split Data
from sklearn.model_selection import KFold
kf = KFold(n_splits=10, random_state=0, shuffle=True)

folds = {}
for fold, (train_patients_index, test_patients_index) in enumerate(kf.split(distinct_rids)):
    train_patients, test_patients = distinct_rids[train_patients_index], distinct_rids[test_patients_index]
    folds[fold] = {'train' : train_patients, 'test' : test_patients}


#%%

import matplotlib.pyplot as plt

x = np.load(images_path[0])
plt.imshow(x[6,:,:])
plt.savefig(os.path.join(results_dir, "plot.pdf"))
plt.show()
plt.close()


#%% Create datasets
image_shape = (32, 32, 32)
image_dimension = 3


images_train_dataset_ = LongitudinalImageDataset(
    ids=list(ids[np.isin(ids, folds[fold]['train'])]),
    images_path=list(images_path_normalized[np.isin(ids, folds[fold]['train'])]),
    times=list(times.reshape(-1)[np.isin(ids, folds[fold]['train'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda
)


images_test_dataset_ = LongitudinalImageDataset(
    ids=list(ids[np.isin(ids, folds[fold]['test'])]),
    images_path=list(images_path_normalized[np.isin(ids, folds[fold]['test'])]),
    times=list(times.reshape(-1)[np.isin(ids, folds[fold]['test'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    ages_std=images_train_dataset_.ages_std,
    ages_mean=images_train_dataset_.ages_mean
)

#%%


# Multimodal dataset
train_dataset = MultimodalDataset([images_train_dataset_], ['dat'],
                                  [DatasetTypes.IMAGE])

train_dataset.print_dataset_statistics()

test_dataset = MultimodalDataset([images_test_dataset_], ['dat'],
                                  [DatasetTypes.IMAGE])


#%%


# Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors


data_info = {
    'dat': (DatasetTypes.IMAGE, 16, 16, image_shape,
                   None, None),
}


output_dir = os.path.join(results_dir,'output_multimodal_synthetic_pd_{}'.format(fold))
print('Output directory', output_dir)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))



#%% Launch

random_slope_model = RandomSlopeModel(
    data_info=data_info,
    latent_space_dim=8,
    pre_encoder_dim=16,
    #pre_decoder_dim=16,
    random_slope=False,
    variational=False,
    use_cuda=use_cuda,
    atlas_path=None
)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)

def call_back(model):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)


estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=10000,
                                        learning_rate=1e-3, output_dir=output_dir,
                                        batch_size=64, save_every_n_iters=5,
                                        call_back=call_back, lr_decay=0.98,
                                        l=1e-3, estimate_noise=True,
                                        test_dataset=test_dataset,
                                        randomize_nb_obs=True, keys_to_initialize=['dat'],
                                        )



