"""

Author : Raphael Couronne

What can we do with a model ???

-Study Tau / Xi / Sources

-Visualize Tau / Xi / Sources effects. As well as their gradient ???


Ideas :
- Predict also the error ?
-Or get a bootstrap approximation of the model via sampling in latent space in variational ?

-Only autoencoder and deltaT and predict what's next


"""

#%%

import os
import numpy as np
from torch.utils.data import DataLoader
import torch

torch.manual_seed(0)
np.random.seed(0)
use_cuda = False


#%% Load

#folder_path = "/Volumes/dtlake01.aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_real_pd/output_multimodal_synthetic_pd_9"
#data_path = "/Volumes/dtlake01.aramis/users/raphael.couronne/Data/MICCAI_2020"
#data_realdat_path = os.path.join(data_path, "PD", "Images", "data_ppmi")
#folder_path = "/Volumes/dtlake01.aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_synthetic_pd_cluster/output_multimodal_synthetic_pd_9"

#data_path = "/Volumes/dtlake01.aramis/users/raphael.couronne/Data/MICCAI_2020"
#data_path = "/Volumes/dtlake01.aramis/users/raphael.couronne/Data/MICCAI_2020"
#data_realdat_path = os.path.join(data_path, "Synthetic", "PD_DAT_2D_cluster", "Images")


folder_longitudinal =  "/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_longitudinal/"
experient_folder = "output_real_pd_2D64_no-init/exp_2020-3-1___0_archived/output_multimodal_synthetic_pd_9"

experient_folder = "output_real_pd_2D64_noinit_tuned/exp_2020-3-9___12/output_multimodal_synthetic_pd_9"



folder_path = os.path.join(folder_longitudinal, experient_folder)


shape = (64,64)

analysis_path = os.path.join(folder_path, "analysis")

if not os.path.exists(analysis_path):
    os.makedirs(analysis_path)

# Load model
model_path = os.path.join(folder_path, "model")
from models.longitudinal_models.abstract_model import load_model
model = load_model(model_path)

# Load Data
train_data_path = os.path.join(folder_path, "train_dataset.p")
test_data_path = os.path.join(folder_path, "test_dataset.p")
from datasets.multimodal_dataset import load_multimodal_dataset
train_dataset = load_multimodal_dataset(train_data_path, None)
test_dataset = load_multimodal_dataset(test_data_path, None)

#train_dataset.set_teacher_forcing(True)
#test_dataset.set_teacher_forcing(True)

from datasets.utils import collate_fn_concat
dataloader = DataLoader(train_dataset, batch_size=1, shuffle=False, collate_fn=collate_fn_concat)

#%% Visualize over
batch = next(iter(dataloader))
mean = model.encode(batch)
latent_trajectories = model.get_latent_trajectories(batch, mean)
output = model.decode(latent_trajectories)

#%% Get std of the dimensions

latent_z = []
idx_z = []
times_baseline = []

for i, batch in enumerate(dataloader):
    latent_z.append(model.encode(batch).reshape(1,-1))
    idx_z.append(batch['idx'])
    times_baseline.append(float(batch['dat']['times'][0][0]))
latent_z = torch.cat(latent_z)

std_1 = latent_z.std(axis=0)

#%%

output_baseline = output['dat'][0,0,:,:]
slice = output_baseline[:,:].detach().numpy()

import matplotlib.pyplot as plt
plt.switch_backend('agg')

import matplotlib
print( matplotlib.get_backend())

fig, ax = plt.subplots(1,1)
ax.imshow(slice)
plt.savefig(os.path.join(analysis_path, "imshow.pdf"))
plt.show()
#plt.close()


#%% Do it without grads, with grad approximation

delta = 0.5

num_features = latent_trajectories['dat'].shape[1]

fig, ax = plt.subplots(num_features, 1, figsize=(6,30))

for dim in range(num_features):

    first_visit = latent_trajectories['dat'][0].clone()
    second_visit = latent_trajectories['dat'][0].clone()
    before_visit = latent_trajectories['dat'][0].clone()
    second_visit[dim] = first_visit[dim] + delta*std_1[dim+1]
    before_visit[dim] = first_visit[dim] - delta * std_1[dim + 1]

    trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

    output = model.decode(trajs)

    output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

    slice = output_diff[:,:].detach().numpy()

    img = ax[dim].imshow(slice, cmap='jet')
    ax[dim].set_title(dim)
    cbar = fig.colorbar(img, ax=ax[dim], extend='both')
plt.savefig(os.path.join(analysis_path, "finite_difference_all.pdf".format(dim)))
plt.show()
plt.close()



#%% Show tsne of patients source

from sklearn.manifold import TSNE
X_embedded = TSNE(n_components=2, init="pca", random_state=0).fit_transform(latent_z.detach().numpy()[:,1:-1])

#%%


fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(X_embedded[:, 0],
           X_embedded[:, 1], alpha=0.5)

for j, patient in enumerate(idx_z):
    ax.text( X_embedded[j,0], X_embedded[j,1], patient[0], fontsize=6)


plt.savefig(os.path.join(analysis_path, "tsne.pdf"))
plt.show()



#%% Plot the t-sne with age


fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(X_embedded[:, 0],
           X_embedded[:, 1],
           c = times_baseline,
           alpha=0.5)

for j, patient in enumerate(idx_z):
    ax.text( X_embedded[j,0], X_embedded[j,1], patient[0], fontsize=6)


plt.savefig(os.path.join(analysis_path, "tsne_age.pdf"))
plt.show()





#%%
fig, ax = plt.subplots(figsize=(10,10))


size = 3

min_x, max_x = min(X_embedded[:,0]), max(X_embedded[:,0])
min_y, max_y = min(X_embedded[:,1]), max(X_embedded[:,1])

for j, patient in enumerate(idx_z[:30]):

    x, y = X_embedded[j, 0], X_embedded[j, 1]

    img = train_dataset[j]['dat']['values'][0, 0]
    plot = ax.imshow(img, extent=[x-size/2, x+size/2,
                           y-size/2, y+size/2])

ax.set_xlim(min_x, max_x)
ax.set_ylim(min_y, max_y)

plt.savefig(os.path.join(analysis_path, "tsne_img.pdf"))
plt.show()

#%% Pred the time PCA - and performance of time prediction

from sklearn.decomposition import PCA
pca = PCA(n_components=2)
X_embedded_pca = pca.fit_transform(latent_z.detach().numpy()[:,1:-1])

print(pca.explained_variance_ratio_)
print(pca.components_)


import numpy as np
from sklearn.linear_model import LinearRegression
X = X_embedded_pca
y = times_baseline
reg = LinearRegression().fit(X, y)




print("R2 of linear regr PCA / time : {}".format((reg.score(X,y))))


#%% Linear Regr

fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(reg.predict(X),
           y,
           alpha=0.5)
plt.savefig(os.path.join(analysis_path, "pca_age_linearregr.pdf"))
plt.show()



#%% Plot PCA

scale_arrow = 8

fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(X_embedded_pca[:, 0],
           X_embedded_pca[:, 1],
           c = times_baseline,
           alpha=0.5)

for j, patient in enumerate(idx_z):
    ax.text( X_embedded_pca[j,0], X_embedded_pca[j,1], patient[0], fontsize=6)


ax.arrow(0,0, reg.coef_[0]/scale_arrow, reg.coef_[1]/scale_arrow, width = 0.06, color="red")


plt.savefig(os.path.join(analysis_path, "pca_age.pdf"))
plt.show()

#%% PCA Age with varying age for subjects

import matplotlib.cm as cm
n_patients = 40
n_sampling = 11

color_palette = cm.get_cmap('jet', n_sampling)
colors = color_palette(range(n_sampling))

fig, ax = plt.subplots(figsize=(10,10))
source_dim = 7

for i, batch in enumerate(dataloader):
    batch_times = batch['dat']['times'].clone()
    patient_info_source = []

    mean = model.encode(batch)
    x_real, y_real = pca.components_.dot(mean[:,1:-1].detach().cpu().numpy().reshape(-1,1))
    ax.text(x_real, y_real, batch['idx'])



    for j, delta_time in enumerate(np.linspace(-0.5, 0.5, n_sampling)):
        batch['dat']['times'] = (batch_times+delta_time).clone()
        mean = model.encode(batch)
        patient_info_source.append(mean[0,1:-1].detach().cpu().numpy().reshape(-1,1))

        sources_pca_delta = pca.components_.dot(mean[0,1:-1].detach().cpu().numpy().reshape(-1,1))

        ax.plot(sources_pca_delta[0, :], sources_pca_delta[1, :], linestyle="--", alpha=0.5, c=colors[j])
        ax.scatter(sources_pca_delta[0, :], sources_pca_delta[1, :], alpha=0.5, c=colors[j])


    sources = np.concatenate(patient_info_source, axis=1)
    sources_pca = pca.components_.dot(sources)

    #ax.plot(sources_pca[0, :], sources_pca[1, :], linestyle="--", alpha=0.5, c=colors[j])
    #ax.scatter(sources_pca[0, :], sources_pca[1, :], alpha=0.5, c=colors[j])



    if i>n_patients:
        break


ax.arrow(0,0, reg.coef_[0]/scale_arrow, reg.coef_[1]/scale_arrow, width = 0.06, color="red")

plt.savefig(os.path.join(analysis_path, "pca_age_varying.pdf"))
plt.show()


#%%
res_1 = np.corrcoef(X_embedded_pca[:,0], times_baseline)
res_2 = np.corrcoef(X_embedded_pca[:,1], times_baseline)
res = [res_1, res_2]


min_x, max_x = np.quantile(X_embedded_pca[:,0], 0.1), np.quantile(X_embedded_pca[:,0], 0.9)
min_y, max_y = np.quantile(X_embedded_pca[:,1], 0.1), np.quantile(X_embedded_pca[:,1], 0.9)

sampling_images = 6
size_x = (max_x-min_x)/sampling_images
size_y = (max_y-min_y)/sampling_images

fig, ax = plt.subplots(figsize=(12,12))

for x in np.linspace(min_x+size_x/2, max_x-size_x/2, sampling_images):
    for y in np.linspace(min_y+size_y/2, max_y-size_y/2, sampling_images):

        sources = np.array([x,y]).dot(pca.components_)
        latent_position = np.concatenate([[0], sources])

        output = model.decode({"dat" : torch.FloatTensor(latent_position).unsqueeze(0)})

        img = output['dat'][0,0].detach().numpy()
        plot_img = ax.imshow(img, extent=[x - size_x / 2, x + size_x / 2,
                                      y - size_y / 2, y + size_y / 2])


ax.set_xlabel("1")
ax.set_ylabel("2")

ax.set_xlim(min_x, max_x)
ax.set_ylim(min_y, max_y)

plt.title({"Corr 1 with time : {0:.2f}, 2 with time : {1:.2f}".format(res_1[0,1], res_2[0,1])})
plt.savefig(os.path.join(analysis_path, "pca_img_interpolate.pdf"))
plt.show()


#%%
fig, ax = plt.subplots(figsize=(10,10))


size = 0.12

min_x, max_x = min(X_embedded_pca[:,0]), max(X_embedded_pca[:,0])
min_y, max_y = min(X_embedded_pca[:,1]), max(X_embedded_pca[:,1])

for j, patient in enumerate(idx_z[:30]):

    x, y = X_embedded_pca[j, 0], X_embedded_pca[j, 1]

    img = train_dataset[j]['dat']['values'][0, 0]
    plot = ax.imshow(img, extent=[x-size/2, x+size/2,
                           y-size/2, y+size/2])

ax.set_xlim(min_x, max_x)
ax.set_ylim(min_y, max_y)

plt.savefig(os.path.join(analysis_path, "pca_img.pdf"))
plt.show()


#%% First 2 effects of PCA to plot :


delta = 0.5
fig, ax = plt.subplots(1, 2, figsize=(10,6))

dim = 0
for dim in range(2):

    first_visit = latent_trajectories['dat'][0].clone()
    second_visit = latent_trajectories['dat'][0].clone()
    before_visit = latent_trajectories['dat'][0].clone()
    second_visit[1:] = first_visit[1:] + delta*torch.FloatTensor(pca.components_[dim])
    before_visit[1:] = first_visit[1:] - delta*torch.FloatTensor(pca.components_[dim])

    trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

    output = model.decode(trajs)

    output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

    slice = output_diff[:,:].detach().numpy()

    img = ax[dim].imshow(slice, cmap='jet')
    ax[dim].set_title("PCA dim {0} -- Corr time : {1:.2f}".format(dim+1, res[dim][0,1] ))
    cbar = fig.colorbar(img, ax=ax[dim], extend='both')
plt.savefig(os.path.join(analysis_path, "finite_difference_pca.pdf".format(dim)))
plt.show()
plt.close()






#%% Do Correlations

data_folder = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data"

import pandas as pd
df_covariables = pd.read_csv(os.path.join(data_folder, "Parkinson", "Processed","PPMI", "df_covariables.csv"))
df_covariables = df_covariables.set_index('Unnamed: 0')
df_covariables = df_covariables.rename(index={'Unnamed: 0': 'id'})
df_metadata = pd.read_csv(os.path.join(data_folder, "Parkinson", "Processed","PPMI", "df_ppmi_metadata.csv"))


df_z = pd.DataFrame(latent_z.detach().numpy(), index = np.array(idx_z).reshape(-1))
df_covariables.rename(index={None: 'id'})

df_analysis = df_z.join(df_covariables)


df_analysis_corr = df_analysis.corr().iloc[:(num_features+1), (num_features+1):]

import seaborn as sns
fig, ax = plt.subplots(figsize=(14,14))
ax = sns.heatmap(np.abs(df_analysis_corr), annot=True, fmt=".2f")
plt.savefig(os.path.join(analysis_path, "corr_abs.pdf"))
plt.show()

fig, ax = plt.subplots(figsize=(14,14))
ax = sns.heatmap(df_analysis_corr, annot=True, fmt=".2f")
plt.savefig(os.path.join(analysis_path, "corr.pdf"))
plt.show()


fig, ax = plt.subplots(figsize=(20,20))
ax = sns.heatmap(df_analysis.corr(), annot=True, fmt=".2f")
plt.savefig(os.path.join(analysis_path, "corr_all.pdf"))
plt.show()

#%% Now test if sources are robust

for num_visit_removed in range(3):

    patients_sources_std = []
    patients_sources_mean = []
    patients_sources_full = []


    latent_this_subject_list = []

    for i, batch in enumerate(dataloader):

        # Sub batch
        num_visits = len(batch['dat']['times'])

        if num_visits>3:

            visits = list(range(num_visits))


            latent_this_subject = []

            for ii in range(20):
                #visits_kept = visits.copy()
                #visits_kept.remove(visit_removed)

                visits_kept = np.sort(np.random.choice(visits, num_visits - num_visit_removed, replace=False))

                print(i, visits, visits_kept, num_visit_removed)

                batch_kept = {
                    "dat":
                        {
                            "idx" : batch["idx"].copy(),
                            "values" : batch["dat"]["values"].clone(),
                            #"ages" : batch["dat"]["ages"].clone(),
                            "times": batch["dat"]["times"].clone(),
                            "times_list": [batch["dat"]["times_list"][0].clone()],
                            "lengths": batch["dat"]["lengths"].copy()
                        },
                    "idx":batch["idx"].copy()
                }

                for key in batch['dat'].keys():
                    #print(key)
                    if key == "lengths":
                        batch_kept['dat'][key] = [len(visits_kept)]
                    elif key == "times_list":
                        batch_kept['dat'][key] = [batch["dat"][key][0][visits_kept].clone()]
                    elif key != "idx":
                        batch_kept['dat'][key] = batch_kept['dat'][key][visits_kept]



                mean = model.encode(batch_kept)
                latent_trajectories = model.get_latent_trajectories(batch_kept, mean)
                #print(latent_trajectories)

                latent_this_subject.append(latent_trajectories['dat'][0,1:].reshape(-1,1))

            latent_this_subject_list.append(latent_this_subject)

            mean = model.encode(batch)
            latent_trajectories = model.get_latent_trajectories(batch, mean)
            subjects_full = latent_trajectories['dat'][0,1:].reshape(-1,1)

            subject_mean = torch.std(torch.cat(latent_this_subject, dim=1), dim=1)
            subject_std = torch.mean(torch.cat(latent_this_subject, dim=1), dim=1)

            patients_sources_std.append(subject_std.reshape(-1,1))
            patients_sources_mean.append(subject_mean.reshape(-1,1))
            patients_sources_full.append(subjects_full.reshape(-1,1))

        if i>40:
            break


    #%%

    patients_sources_mean = torch.cat(patients_sources_mean, dim=1)
    patients_sources_std = torch.cat(patients_sources_std, dim=1)
    patients_sources_full = torch.cat(patients_sources_full, dim=1)

    #%%

    print("std between patients full", patients_sources_full.std(dim=1))
    print("std between patients mean", patients_sources_mean.std(dim=1))
    print("mean of patients std", patients_sources_std.mean(dim=1))


    #%% Plot some patients

    fig, ax = plt.subplots(1,1)

    n_patients = 20

    import matplotlib.cm as cm

    color_palette = cm.get_cmap('jet', n_patients)
    colors = color_palette(range(n_patients))

    for i in range(n_patients):
        X_embedded_patient_array = []
        for randomized_source in latent_this_subject_list[i]:
            X_embedded_patient_array.append(pca.components_.dot(randomized_source.detach().numpy()))

        for j, embedded in enumerate(X_embedded_patient_array):
            ax.text(embedded[0, 0], embedded[1, 0],i, fontsize=6, color=colors[i])

        X_embedded_patient_array = np.array(X_embedded_patient_array)

        ax.scatter(X_embedded_patient_array[:,0,0],
                   X_embedded_patient_array[:,1,0],
                   c = colors[i], alpha=0)



    plt.savefig(os.path.join(analysis_path, "intra-inter_representation_minus{}visits.pdf".format(num_visit_removed)))
    plt.show()
    plt.close()


#%% Plot the robustness of patient on 10-20 patients at random

print("coucou")
import pandas as pd

patient = 0
source_dim = 7

batch = next(iter(dataloader))
batch_times = batch['dat']['times'].clone()

patient_info = dict.fromkeys(['tau', 'xi']+["source_{}".format(i) for i in range(source_dim)])
for key in patient_info.keys():
    patient_info[key] = []

for delta_time in np.linspace(-0.5, 0.5, 10):
    batch['dat']['times'] = (batch_times+delta_time).clone()
    mean = model.encode(batch)

    patient_info["tau"].append(mean[0,0].detach().cpu().numpy())
    patient_info["xi"].append(mean[0,-1].detach().cpu().numpy())
    for i in range(source_dim):
        patient_info["source_{}".format(i)].append(mean[0,i+1].detach().cpu().numpy())

df_patient = pd.DataFrame(patient_info)
df_patient["idx"] = batch['idx'][0]
df_patient["delta_time"] = np.linspace(-0.5, 0.5, 10)


fig, ax = plt.subplots(1,1)

ax.plot(np.linspace(-0.5, 0.5, 10), df_patient["tau"], c="black")

for i in range(source_dim):
    ax.plot(np.linspace(-0.5, 0.5, 10), df_patient["source_{}".format(i)], alpha=0.2)

plt.savefig(os.path.join(analysis_path, "disentanglement.pdf"))


#%%

