import numpy as np
import os
import pandas as pd
from multimodal_dataset import MultimodalDataset, DatasetTypes
from sklearn.model_selection import RepeatedKFold
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from legacy.estimate_variational import estimate_random_slope_model_variational
from torch.utils.data import DataLoader
from models.longitudinal_models.abstract_model import RandomSlopeModel
import pickle
import torch


class Benchmark_adas():
    """
    Compute and compare the residual errors for both multimodal and pet-only datasets.
    Validation via Repeated Stratified Cross Validation
    Performance assessed in both extrapolation and interpolation : do we do better than basic strategies ?
    """

    def __init__(self, paths, nn_parameters, seed=0, validation=(5, 10), name=None, patients='intersection',
                 learning_rate=1e-3, batch_size=32, test_validation=False):

        # Hyperparameters
        self.seed = seed

        # Paths
        self.name = 'Unnamed yet'
        if name is not None:
            self.name = name
        self.paths = paths

        # Data
        self.patients = patients
        self.df = None
        self.distinct_rids = None
        self.groups = None
        self.initialize_data()

        # Validation procedure parameters
        self.validation = validation
        self.test_validation = test_validation

        # Neural net parameters
        self.nn_parameters = nn_parameters
        self.learning_rate = learning_rate
        self.batch_size = batch_size

        # Folds
        self.generate_folds()
        self.save_infos()

    def save_infos(self):
        if not os.path.exists(self.paths['output_dir']):
            os.makedirs(self.paths['output_dir'])

        with open(os.path.join(self.paths['output_dir'], '{0}.p'.format(self.name)),
                  'wb') as output:  # Overwrites any existing file.
            pickle.dump(self, output)

    def initialize_data(self):

        # Load adas .csv
        df_adas = pd.read_csv(self.paths['adas_scores'], header=None)
        df_adas[0] = df_adas[0].apply(lambda x: int(x.split('S')[1]))
        df_adas.set_index([0, 1], inplace=True)
        df_adas.index.rename(['id', 'age'], inplace=True)
        df_adas = df_adas.iloc[:, :4]
        distinct_rids = df_adas.index.unique('id')

        self.df = {'adas_scores': df_adas}
        self.distinct_rids = distinct_rids
        self.data_dim = {'adas_scores': df_adas.shape[1]}

    def generate_folds(self):
        n_splits, n_repeats = self.validation
        folds = {}
        rkf = RepeatedKFold(n_splits=n_splits, n_repeats=n_repeats, random_state=self.seed)
        for fold, (train_index, test_index) in enumerate(rkf.split(self.distinct_rids)):
            folds[fold] = {'train': self.distinct_rids[train_index], 'test': self.distinct_rids[test_index]}
        self.folds = folds

    def generate_datasets_from_fold(self, fold, key='train', data_type='multimodal', pruning_frequency=0):

        # Get mu, sigma from train
        # Adas

        if data_type == 'multimodal_2-2':

            if key == 'train':

                adas_dataset_1_ = LongitudinalScalarDataset(list(
                    self.df['adas_scores'].iloc[:, [0, 1]].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                                                            list(self.df['adas_scores'].iloc[:, [0, 1]].loc[
                                                                     self.folds[fold][key]].values),
                                                            list(self.df['adas_scores'].iloc[:, [0, 1]].loc[
                                                                     self.folds[fold][key]].index.get_level_values(
                                                                level='age')))

                self.folds[fold]['age mean std'] = (adas_dataset_1_.ages_mean, adas_dataset_1_.ages_std)

                ages_mean, ages_std = self.folds[fold]['age mean std']

            elif key == 'test':
                ages_mean, ages_std = self.folds[fold]['age mean std']

                adas_dataset_1_ = LongitudinalScalarDataset(list(
                    self.df['adas_scores'].iloc[:, [0, 1]].loc[self.folds[fold][key]].index.get_level_values(
                        level='id')),
                    list(self.df['adas_scores'].iloc[:, [0, 1]].loc[
                             self.folds[fold][key]].values),
                    list(self.df['adas_scores'].iloc[:, [0, 1]].loc[
                        self.folds[fold][key]].index.get_level_values(
                        level='age')), ages_mean=ages_mean, ages_std=ages_std)


            # Pruning Frequency in train/test
            if pruning_frequency == 0:

                adas_dataset_2_ = LongitudinalScalarDataset(list(
                    self.df['adas_scores'].iloc[:, [2, 3]].loc[self.folds[fold][key]].index.get_level_values(
                        level='id')),
                    list(self.df['adas_scores'].iloc[:, [2, 3]].loc[
                             self.folds[fold][key]].values),
                    list(self.df['adas_scores'].iloc[:, [2, 3]].loc[
                        self.folds[fold][key]].index.get_level_values(
                        level='age')), ages_mean=ages_mean, ages_std=ages_std)


            else:

                def prune(patient, pruning_frequency):
                    patient = patient.iloc[
                              np.random.choice(patient.shape[0], int((1 - pruning_frequency) * patient.shape[0]),
                                               replace=False), :]
                    return patient

                df_pruned = pd.DataFrame()

                for id, patient in self.df['adas_scores'].groupby('id'):
                    patient_pruned = prune(patient, pruning_frequency)
                    df_pruned = pd.concat([df_pruned, patient_pruned])

                adas_dataset_2_ = LongitudinalScalarDataset(list(
                    df_pruned.iloc[:, [2, 3]].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                    list(df_pruned.iloc[:, [2, 3]].loc[
                             self.folds[fold][key]].values),
                    list(df_pruned.iloc[:, [2, 3]].loc[
                        self.folds[fold][key]].index.get_level_values(
                        level='age')), ages_mean=ages_mean, ages_std=ages_std)



            dataset_adas = MultimodalDataset([adas_dataset_1_, adas_dataset_2_],
                                             ['memory+language', 'praxis+concentration'],
                                             [DatasetTypes.SCALAR, DatasetTypes.SCALAR])



        elif data_type == 'multimodal_4-1':


            if key == 'train':

                adas_dataset_1_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 0].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                    list(self.df['adas_scores'].iloc[:, 0].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 0].loc[self.folds[fold][key]].index.get_level_values(level='age')))

                adas_dataset_2_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 1].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                    list(self.df['adas_scores'].iloc[:, 1].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 1].loc[self.folds[fold][key]].index.get_level_values(level='age')))

                adas_dataset_3_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 2].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                    list(self.df['adas_scores'].iloc[:, 2].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 2].loc[self.folds[fold][key]].index.get_level_values(level='age')))

                adas_dataset_4_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 3].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                    list(self.df['adas_scores'].iloc[:, 3].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 3].loc[self.folds[fold][key]].index.get_level_values(level='age')))

                self.folds[fold]['age mean std'] = (adas_dataset_1_.ages_mean, adas_dataset_1_.ages_std)


            elif key == 'test':

                ages_mean, ages_std = self.folds[fold]['age mean std']

                adas_dataset_1_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 0].loc[self.folds[fold][key]].index.get_level_values(
                        level='id')),
                    list(self.df['adas_scores'].iloc[:, 0].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 0].loc[self.folds[fold][key]].index.get_level_values(
                        level='age')), ages_mean=ages_mean, ages_std=ages_std)

                adas_dataset_2_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 1].loc[self.folds[fold][key]].index.get_level_values(
                        level='id')),
                    list(self.df['adas_scores'].iloc[:, 1].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 1].loc[self.folds[fold][key]].index.get_level_values(
                        level='age')), ages_mean=ages_mean, ages_std=ages_std)

                adas_dataset_3_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 2].loc[self.folds[fold][key]].index.get_level_values(
                        level='id')),
                    list(self.df['adas_scores'].iloc[:, 2].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 2].loc[self.folds[fold][key]].index.get_level_values(
                        level='age')), ages_mean=ages_mean, ages_std=ages_std)

                adas_dataset_4_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].iloc[:, 3].loc[self.folds[fold][key]].index.get_level_values(
                        level='id')),
                    list(self.df['adas_scores'].iloc[:, 3].loc[self.folds[fold][key]].values.reshape(-1, 1)),
                    list(self.df['adas_scores'].iloc[:, 3].loc[self.folds[fold][key]].index.get_level_values(
                        level='age')), ages_mean=ages_mean, ages_std=ages_std)

            else:
                raise ValueError("Splitting key not known")

            dataset_adas = MultimodalDataset([adas_dataset_1_, adas_dataset_2_, adas_dataset_3_, adas_dataset_4_],
                                             ['memory', 'language', 'praxis', 'concentration'],
                                             [DatasetTypes.SCALAR, DatasetTypes.SCALAR, DatasetTypes.SCALAR,
                                              DatasetTypes.SCALAR])



        elif data_type == 'unimodal':

            if key =='train':

                adas_dataset_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                    list(self.df['adas_scores'].loc[self.folds[fold][key]].values),
                    list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='age')))

                self.folds[fold]['age mean std'] = (adas_dataset_.ages_mean, adas_dataset_.ages_std)

            elif key =='test':

                ages_mean, ages_std = self.folds[fold]['age mean std']

                adas_dataset_ = LongitudinalScalarDataset(
                    list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='id')),
                    list(self.df['adas_scores'].loc[self.folds[fold][key]].values),
                    list(self.df['adas_scores'].loc[self.folds[fold][key]].index.get_level_values(level='age')),
                ages_mean=ages_mean, ages_std=ages_std)

            else:
                raise ValueError("Splitting key not known")



            dataset_adas = MultimodalDataset([adas_dataset_], ['adas_scores'], [DatasetTypes.SCALAR])

        else:
            raise NameError("Data Type neither unimodal or multimodal_2-2 or multimodal 4-1")

        # Only ADAS Dataset

        return dataset_adas

    def compute_fold(self, fold, data_type='multimodal_2-2', pruning_frequency=0):

        # Set the seed
        torch.manual_seed(self.seed)
        np.random.seed(self.seed)

        # get the fold datasets
        train_dataset_adas = self.generate_datasets_from_fold(fold, key='train',
                                                              data_type=data_type, pruning_frequency=pruning_frequency)
        test_dataset_multimodal, test_dataset_pet, test_dataset_adas = None, None, None

        if self.test_validation:
            test_dataset_adas = self.generate_datasets_from_fold(fold, key='test',
                                                                 data_type=data_type, pruning_frequency= pruning_frequency)

        # Get the parameters
        latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs = self.nn_parameters

        # Get Data info
        if data_type == 'unimodal':
            adas_info = (DatasetTypes.SCALAR, 16, 16, self.data_dim['adas_scores'],
                         ['memory', 'language', 'praxis', 'concentration'], ['r', 'g', 'y', 'b'])
            data_info_adas = {'adas_scores': adas_info}

        elif data_type == 'multimodal_2-2':

            data_info_adas = {
                'memory+language': (DatasetTypes.SCALAR, 12, 12, 2, ['memory', 'language'], ['r', 'g']),
                'praxis+concentration': (DatasetTypes.SCALAR, 12, 12, 2, ['praxis', 'concentration'], ['y', 'b']),
            }

        elif data_type == 'multimodal_4-1':

            data_info_adas = {
                'memory': (DatasetTypes.SCALAR, 10, 10, 1, ['memory'], ['r']),
                'language': (DatasetTypes.SCALAR, 10, 10, 1, ['language'], ['g']),
                'praxis': (DatasetTypes.SCALAR, 10, 10, 1, ['praxis'], ['y']),
                'concentration': (DatasetTypes.SCALAR, 10, 10, 1, ['concentration'], ['b'])
            }

        else:
            raise NameError("Data Type neither unimodal nor multimodal2-2 not multimodal 4-1")

        # Directories
        output_dir_adas = os.path.join(self.paths['output_dir'], 'fold_{0}'.format(fold), 'adas_{0}/'.format(data_type))

        if not os.path.exists(output_dir_adas):
            os.makedirs(output_dir_adas)

        # Save the fold datasets
        train_dataset_adas.save(os.path.join(output_dir_adas, 'train_dataset.p'))

        random_slope_model_adas = RandomSlopeModel(
            data_info=data_info_adas,
            latent_space_dim=latent_space_dim,
            pre_encoder_dim=pre_encoder_dim,
            # pre_decoder_dim=pre_decoder_dim,
            random_slope=random_slope,
            variational=False,
            use_cuda=False,
            atlas_path=None,
            depth="shallow"
        )

        estimate_random_slope_model_variational(random_slope_model_adas, train_dataset_adas, n_epochs=n_epochs,
                                                learning_rate=self.learning_rate, output_dir=output_dir_adas,
                                                batch_size=self.batch_size, save_every_n_iters=100,
                                                call_back=lambda x: call_back(x, train_dataset_adas, test_dataset_adas,output_dir_adas),
                                                lr_decay=0.98,
                                                l=0.05, estimate_noise=True,
                                                randomize_nb_obs=True, test_dataset=test_dataset_adas)


"""
        estimate_random_slope_model_variational(random_slope_model_adas, train_dataset_adas, n_epochs=n_epochs,
                                                learning_rate=self.learning_rate, output_dir = output_dir_adas,
                                                batch_size=self.batch_size, save_every_n_iters=100,
                                                call_back=lambda x: call_back(x, train_dataset_adas, output_dir_adas),
                                                lr_decay=0.98,
                                                l=1., estimate_noise=True,
                                                randomize_nb_obs=True, test_dataset=test_dataset_adas)"""


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)


def call_back(model, train_dataset, test_dataset, output_dir):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)


#%%
