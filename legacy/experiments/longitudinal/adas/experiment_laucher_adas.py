import os
import sys
sys.path.append("../../")
from legacy.experiments.longitudinal.adas import Benchmark_adas
#from Benchmark_adas import Benchmark_adas

from shutil import copyfile


#%% Launch

name = 'Adas_Exp'

#output_dir = '../../../output/benchmark_adas_{0}_6(backtobasics)/'.format(name)
#output_dir = '../../../output/benchmark_adas_{0}_7(remove_mean_estimation_alpha_tau)/'.format(name)
#output_dir = '../../../output/benchmark_adas_{0}_9(remove_mean_estimation_alpha_tau+estimatenoiseFalse)/'.format(name)



output_dir = '../../../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v8(testmeanstd)_final'.format(name)
adas_path = '../../../data/data_adas/df.csv'
paths = {'adas_scores': adas_path, 'output_dir': output_dir}
pre_encoder_dim = 10
pre_decoder_dim = 10
latent_space_dim = 2
random_slope = False
n_epochs = 5000
n_jobs = 1
n_runs = 1


validation = (10, 2)
nn_parameters = latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs

# Create experiment
experiment = Benchmark_adas(paths, nn_parameters, seed=0,
                            validation=validation, name=name, patients='intersection', test_validation=True)

# Save the networks
if not os.path.exists( os.path.join(output_dir, "utils")):
    os.makedirs(os.path.join(output_dir, "utils"))
copyfile("../../networks.py", os.path.join(output_dir, "utils", "networks.py"))
copyfile("../../abstract_model.py", os.path.join(output_dir, "utils", "abstract_model.py"))
copyfile("experiment_laucher_adas.py", os.path.join(output_dir, "utils", "experiment_laucher_adas.py"))

# Launch experiment
def compute_experiment_1_fold(experiment, fold, data_type='multimodal_2-2'):
    return experiment.compute_fold(fold, data_type)


#Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'multimodal_4-1') for i in range(n_runs))
#Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'multimodal_2-2') for i in range(n_runs))
#Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'unimodal') for i in range(n_runs))


compute_experiment_1_fold(experiment, 0, data_type='multimodal_2-2')


# No Parallel Computing
#for fold in range(5):
#    compute_experiment_1_fold(experiment, fold, data_type='multimodal')


