import os
from torch.utils.data import DataLoader
import numpy as np
from models.longitudinal_models.abstract_model import load_model

def get_times_latent_space(dataset, model):


    times = []
    alphas = []
    taus = []
    dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

    for i, data in enumerate(dataloader):
        mean = model.encode(data, randomize_nb_obs=False)
        alpha = np.array([mean[0].data.cpu().detach().numpy()])
        tau = np.array([mean[-1].data.cpu().detach().numpy()])

        alphas.append(alpha)
        taus.append(tau)

        latent_trajectories = model.get_latent_trajectories(data, mean)

        for val in latent_trajectories.values():
            times.append(val[:, 0].cpu().detach().numpy())


    times = np.concatenate(times)


    return times, np.concatenate(alphas), np.concatenate(taus)


def get_average_trajectory_4(fold, results_dir, experiment, keys, type):
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, "adas_{0}".format(type))

    dataset_multimodal = experiment.generate_datasets_from_fold(fold, 'train', type)

    _, _, _, model = load_output(output_dir_multimodal, keys, ['train'])

    times, alpha_uni, tau_uni = get_times_latent_space(dataset_multimodal, model)


    min_x, max_x = np.quantile(times, [0.05, 0.95])
    x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)
    age_min_x, age_max_x = min_x, max_x

    trajectory = []
    for key in keys:
        trajectory.append(mean_traj[key].data.numpy())

    trajectory = np.concatenate([i for i in trajectory], axis=1)

    return times, trajectory, (age_min_x, age_max_x)




def compute_fold_error(fold, type, keys, splits, results_dir, experiment):

    # Load Model
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'adas_{0}'.format(type))
    _, _, _, model = load_output(output_dir_multimodal,  keys ,['train'], 'shallow')

    # Load Dataset
    dataset_multimodal_train = experiment.generate_datasets_from_fold(fold, 'train', type)
    dataset_multimodal_test = experiment.generate_datasets_from_fold(fold, 'test', type)
    test_dataloader = DataLoader(dataset_multimodal_test)
    train_dataloader = DataLoader(dataset_multimodal_train)

    # Compute Residuals
    train_ids, train_residuals, _, _ = model.compute_residuals(train_dataloader)
    test_ids, test_residuals, _, _ = model.compute_residuals(test_dataloader)

    residuals = {'train':train_residuals,
                 'test':test_residuals}


    fold_error = {'train':[],
                  'test':[]}

    for split in splits:
        for key in keys:
            fold_error[split].append(residuals[split][key])
        fold_error[split] = np.mean(np.array(fold_error[split]))

    return fold_error

def load_output(output_dir, keys, splits=['train'], depth='shallow'):
    residuals, latent_positions, latent_trajectories = None, None, None

    # Load model
    model = load_model(os.path.join(output_dir, 'model'), depth=depth)
    print("Load model at : {0}".format(os.path.join(output_dir, 'model')))

    return residuals, latent_positions, latent_trajectories, model
