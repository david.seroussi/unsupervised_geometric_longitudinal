n_runs = 40

import numpy as np
import pickle
import os
from torch.utils.data import DataLoader
import pandas as pd
import torch
from models.longitudinal_models.abstract_model import load_model
import matplotlib.pyplot as plt



def load_text_output(output_dir, output_type, keys, folds=['train']):
    res = {'train': {}, 'test': {}}

    for datasubset in folds:
        for key in keys:
            key_name = key
            if key == '':
                key_name = 'latent'
            res[datasubset][key_name] = np.loadtxt(
                os.path.join(output_dir, '{0}_{1}{2}.txt'.format(datasubset, key, output_type)))
    return res


def load_output(output_dir, keys, splits=['train']):
    # Load .txt
    # residuals = load_text_output(output_dir, '_residuals', keys)
    # latent_positions = load_text_output(output_dir, 'latent_positions', [''])
    # latent_trajectories = load_text_output(output_dir, 'latent_trajectories', [''])

    residuals, latent_positions, latent_trajectories = None, None, None

    # Load model
    model = load_model(os.path.join(output_dir, 'model'), depth=depth)

    return residuals, latent_positions, latent_trajectories, model


def get_times_latent_space(dataset, model):
    times = []
    alphas = []
    taus = []
    dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

    for i, data in enumerate(dataloader):
        mean = model.encode(data, randomize_nb_obs=False)
        alpha = np.array([mean[0].data.cpu().detach().numpy()])
        tau = np.array([mean[-1].data.cpu().detach().numpy()])

        alphas.append(alpha)
        taus.append(tau)

        latent_trajectories = model.get_latent_trajectories(data, mean)

        for val in latent_trajectories.values():
            times.append(val[:, 0].cpu().detach().numpy())

    times = np.concatenate(times)

    return times, np.concatenate(alphas), np.concatenate(taus)


# Paths
project_data_path = '../data/data_pet'
adas_path = '../data/data_adas/df.csv'


# Overall mean/std age
df_adas = pd.read_csv(adas_path, header=None).set_index([0, 1])
mean_age = np.mean(df_adas.index.get_level_values(1))
std_age = np.std(df_adas.index.get_level_values(1))


#%%

import matplotlib as mpl
label_size = 20
mpl.rcParams['xtick.labelsize'] = label_size-5
mpl.rcParams['ytick.labelsize'] = label_size-2

# Plot
pruning_number = 21
pruning_frequencies = [0.1, 0.3, 0.5]
fig, axes = plt.subplots(len(pruning_frequencies), 1, figsize=(10, 12),sharex=True, sharey=True)
plt.ylim(0,1)
axes[0].set_xlim(-1, 1)




name = 'Adas_Exp'
depth = "shallow"


plt.subplots_adjust(wspace = 0.1, hspace = 0.12 ,
                    bottom=0.025, top=0.968,
                    left=0.05, right=0.975)


n_runs = 10
#results_dir = '../noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_pruning_2-2_{0}/'.format(pruning_frequency)

pruning_errors_1 = dict.fromkeys(pruning_frequencies)
pruning_errors_2 = dict.fromkeys(pruning_frequencies)

pruning_errors_1_all = dict.fromkeys(pruning_frequencies)
pruning_errors_2_all = dict.fromkeys(pruning_frequencies)

for i,pruning_frequency in enumerate(pruning_frequencies):

    results_dir = os.path.join("/teams/ARAMIS/HOME/raphael.couronne/working_dir/collab_maxime/output/pruningv{1}/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_pruning_2-2_{0}/".format(pruning_frequency, pruning_number))

    # Load experiment
    print(os.path.join(results_dir, '{0}.p'.format(name)))
    experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))



    torch.manual_seed(0)
    np.random.seed(0)






    # Plot unimodal, multimodal 2-2, multimodal 4-1


    # Multimodal 2x2

    # Plot

    c1 = (17 / 255., 72 / 255., 107. / 255.)
    c2 = (71 / 255., 131 / 255., 86 / 255.)
    c3 = (172 / 255., 43 / 255., 73 / 255.)
    c4 = (218 / 255., 99 / 255., 40 / 255.)
    c5 = (255 / 255., 164 / 255., 37 / 255.)
    colors = [c1, c2, c3, c4, c5]

    fold_errors_1 = dict.fromkeys(list(range(n_runs)))
    fold_errors_2 = dict.fromkeys(list(range(n_runs)))
    for fold in range(n_runs):
        name_dir = 'fold_{0}'.format(fold)
        directory = os.path.join(results_dir, name_dir)
        output_dir_multimodal = os.path.join(directory, 'adas_multimodal_2-2')

        keys = ['memory+language','praxis+concentration']
        dataset_multimodal = experiment.generate_datasets_from_fold(fold, 'train', 'multimodal_2-2')

        _, _, _, model = load_output(output_dir_multimodal,  keys ,['train'])

        times, alpha_uni, tau_uni = get_times_latent_space(dataset_multimodal, model)
        mean_tau = np.mean(tau_uni)
        mean_alpha = np.mean(alpha_uni)

       # axes[i, 1].hist(times, alpha=0.1)

        #min_x, max_x = np.quantile(dataset_multimodal.datasets[0].times,[0.05, 0.95])
        min_x, max_x = np.quantile(times, [0.05, 0.95])
        x_out, mean_traj = model.get_mean_trajectory(min_x=min_x, max_x=max_x)
        #x_out, mean_traj = model.get_mean_trajectory(min_x=-0.5, max_x=1)
        #age_min_x, age_max_x = min_x*std_age+mean_age, max_x*std_age+mean_age

        age_min_x , age_max_x = min_x, max_x

        memorylanguage_trajectory = mean_traj['memory+language'].data.numpy()
        praxisconcentration_trajectory = mean_traj['praxis+concentration'].data.numpy()

        color_index=0
        for dim in range(memorylanguage_trajectory.shape[1]):
            axes[i].plot(np.linspace(age_min_x, age_max_x, memorylanguage_trajectory.shape[0]).reshape(-1,1),
                         memorylanguage_trajectory[:,dim], c=colors[color_index])
            color_index +=1

        for dim in range(praxisconcentration_trajectory.shape[1]):
            axes[i].plot(np.linspace(age_min_x, age_max_x, praxisconcentration_trajectory.shape[0]).reshape(-1,1),
                         praxisconcentration_trajectory[:,dim], c=colors[color_index])
            color_index += 1


        # get the train_test error
        type = "multimodal_2-2"

        modality_res_1_train = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold),"adas_{0}".format(type), 'train_{0}_residuals.txt'.format('memory+language')))
        modality_res_1_test = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format(type),
                                                       'test_{0}_residuals.txt'.format('memory+language')))

        modality_res_2_train = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold),"adas_{0}".format(type),
                                                 'train_{0}_residuals.txt'.format('praxis+concentration')))
        modality_res_2_test = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format(type),
                                                 'test_{0}_residuals.txt'.format('praxis+concentration')))


        fold_error_1 = {"train" : np.mean((modality_res_1_train)),
                      "test": np.mean((modality_res_1_test))}
        fold_errors_1[fold] = fold_error_1

        fold_error_2 = {"train": np.mean((modality_res_2_train)),
                        "test": np.mean((modality_res_2_test))}
        fold_errors_2[fold] = fold_error_2



    pruning_errors_1[pruning_frequency] = pd.DataFrame(fold_errors_1).mean(axis=1)
    pruning_errors_2[pruning_frequency] = pd.DataFrame(fold_errors_2).mean(axis=1)

    pruning_errors_1_all[pruning_frequency] = pd.DataFrame(fold_errors_1)
    pruning_errors_2_all[pruning_frequency] = pd.DataFrame(fold_errors_2)



output_dir = os.path.join("/teams/ARAMIS/HOME/raphael.couronne/working_dir/collab_maxime/output/final/")

if not os.path.exists(os.path.join(output_dir, 'figures')):
    os.mkdir(os.path.join(output_dir, 'figures'))

    'memory', 'language', 'praxis', 'concentration'

# Legend
import matplotlib.patches as mpatches
memory = mpatches.Patch(color=c1, label='memory')
language = mpatches.Patch(color=c2, label='language')

praxis = mpatches.Patch(color=c3, label='praxis')
concentration = mpatches.Patch(color=c4, label='concentration')



for i in range(3):
    l1 = axes[i].legend(handles=[memory, language], loc=2, bbox_to_anchor=(0., 1.02), fontsize=label_size,
                            edgecolor='black')
    l2 = axes[i].legend(handles=[praxis, concentration], loc=2, bbox_to_anchor=(0., 0.75), fontsize=label_size,
                            edgecolor='black')
    axes[i].add_artist(l1)


axes[0].set_title('2-Modalities pruned {0}%'.format(int(pruning_frequencies[0]*100)), fontsize=label_size+6)
axes[1].set_title('2-Modalities pruned {0}%'.format(int(pruning_frequencies[1]*100)), fontsize=label_size+6)
axes[2].set_title('2-Modalities pruned {0}%'.format(int(pruning_frequencies[2]*100)), fontsize=label_size+6)

#plt.legend(l1)

#p1, = plot([1,2,3], label="test1")
#p2, = plot([3,2,1], label="test2")

#l1 = legend([p1], ["Label 1"], loc=1)
#l2 = legend([p2], ["Label 2"], loc=4) # this removes l1 from the axes.
#gca().add_artist(l1)

#plt.legend(handles=[red_patch, blue_patch])

#plt.tight_layout()
plt.savefig(os.path.join(output_dir, 'figures', 'mean_trajectories_final_pruningv{0}.pdf'.format(pruning_number)))
plt.show()

pd.DataFrame(pruning_errors_1).to_csv(os.path.join(output_dir, 'figures', 'errors_final_pruningv_1{0}.csv'.format(pruning_number)))
print(pd.DataFrame(pruning_errors_1))

pd.DataFrame(pruning_errors_2).to_csv(os.path.join(output_dir, 'figures', 'errors_final_pruningv_2{0}.csv'.format(pruning_number)))
print(pd.DataFrame(pruning_errors_2))



print("toto")
plt.show()












#%%

pruning_number = 2

pruning_frequency = 0.5
fold = 2

results_dir = os.path.join(
    "/teams/ARAMIS/HOME/raphael.couronne/working_dir/collab_maxime/output/pruningv{1}/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_pruning_2-2_{0}/".format(
        pruning_frequency, pruning_number))

name_dir = 'fold_{0}'.format(fold)
directory = os.path.join(results_dir, name_dir)
output_dir_multimodal = os.path.join(directory, 'adas_multimodal_2-2')

type = "multimodal_2-2"

modality_res_1_train = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format(type),
                                               'train_{0}_residuals.txt'.format('memory+language')))
modality_res_1_test = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format(type),
                                              'test_{0}_residuals.txt'.format('memory+language')))


modality_res_2_train = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format(type),
                                               'train_{0}_residuals.txt'.format('praxis+concentration')))
modality_res_2_test = np.loadtxt(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format(type),
                                              'test_{0}_residuals.txt'.format('praxis+concentration')))

experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))
dataset_multimodal = experiment.generate_datasets_from_fold(fold, 'train', 'multimodal_2-2', pruning_frequency=pruning_frequency)

#print(dataset_multimodal.ages_mean)
#print(dataset_multimodal.ages_std)



print("Modality 1")
print(np.mean(modality_res_1_train))
print(np.mean(modality_res_1_test))
print(pruning_errors_1_all[pruning_frequency][fold])

print("Modality 2")
print(np.mean(modality_res_2_train))
print(np.mean(modality_res_2_test))
print(pruning_errors_2_all[pruning_frequency][fold])


#from multimodal_dataset import load_multimodal_dataset
#folder_dataset = load_multimodal_dataset(os.path.join(results_dir, "fold_{0}".format(fold), "adas_{0}".format(type), 'train_dataset.p'))
#print(dataset_multimodal.print_dataset_statistics())
#print(folder_dataset.print_dataset_statistics())

#%%

print(pd.DataFrame(pruning_errors_1))
print(pd.DataFrame(pruning_errors_2))

#%%

dataset_multimodal = experiment.generate_datasets_from_fold(fold, 'train', 'multimodal_2-2', pruning_frequency=0.6)
dataset_multimodal[1]