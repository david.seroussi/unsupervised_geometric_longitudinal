import os


import numpy as np
import pickle
import os
import torch
from models.longitudinal_models.abstract_model import load_model
import matplotlib.pyplot as plt

#%%


name = 'benchmark_adas_Adas_multimodal_or_unimodal'


# Paths
project_data_path = '../data/data_pet'
adas_path = '../data/data_adas/df.csv'


name = 'Adas_Exp'
results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources/'.format(name)
#results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_4x1parameters/'.format(name)
#results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v6(testmeanstd)_final/'.format(name)


results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v3(testmeanstd)'
#results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder'


# Load experiment
#name = 'Adas_multimodal_or_unimodal'
print(os.path.join(results_dir, '{0}.p'.format(name)))
experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))


depth = "shallow"


#%%

def load_text_output(output_dir, output_type, keys, folds=['train']):
    res = {'train': {}, 'test': {}}

    for datasubset in folds:
        for key in keys:
            key_name = key
            if key == '':
                key_name = 'latent'
            res[datasubset][key_name] = np.loadtxt(
                os.path.join(output_dir, '{0}_{1}{2}.txt'.format(datasubset, key, output_type)))
    return res


def load_output(output_dir, keys, splits=['train']):
    # Load .txt
    #residuals = load_text_output(output_dir, '_residuals', keys)
    #latent_positions = load_text_output(output_dir, 'latent_positions', [''])
    #latent_trajectories = load_text_output(output_dir, 'latent_trajectories', [''])

    residuals, latent_positions, latent_trajectories = None, None, None

    # Load model
    model = load_model(os.path.join(output_dir, 'model'), depth=depth)

    return residuals, latent_positions, latent_trajectories, model

#%% Parameters

c1 =  (17/255.,72/255.,107./255.)
c2 = (71/255.,131/255.,86/255.)
c3 = (172/255.,43/255.,73/255.)
c4 = (218/255., 99/255., 40/255.)
c5 = (255/255., 164/255., 37/255.)
colors = [c1, c2, c3, c4, c5]


type = 'multimodal_2-2'
name_subdir = 'adas_multimodal_2-2'

type = 'unimodal'
name_subdir = 'adas_unimodal'

#type = 'multimodal_4-1'
#name_subdir = 'adas_multimodal_4-1'

#%%

fold = 0

name_dir = 'fold_{0}'.format(fold)
directory = os.path.join(results_dir, name_dir)
output_dir_multimodal = os.path.join(directory, name_subdir)

keys = ['adas_scores']
dataset = experiment.generate_datasets_from_fold(fold, 'train', type)



_, _, _, model = load_output(output_dir_multimodal,  keys,['train'])
x_out, mean_traj = model.get_mean_trajectory(min_x=-1, max_x=1)


fig, ax = plt.subplots(1,1)

color_index = 0

for modality in dataset.modality_names:
    trajectory = mean_traj[modality].data.numpy()
    for dim in range(trajectory.shape[1]):
        ax.plot(trajectory[:,dim], c=colors[color_index])
        color_index += 1
plt.show()



#%%

fig, ax = plt.subplots(1,1)

grid = np.linspace(-1, 1, 20).reshape(-1,1)

source_dim = 2
source_chosen = 0
sources = np.zeros(shape=(grid.shape[0],source_dim-1))

for source_value in np.linspace(0,0.5,25):
    sources[:,source_chosen] = source_value*np.ones_like(grid).reshape(-1)

    color_index = 0
    for modality in dataset.modality_names:
        latent_traj = {modality : torch.Tensor(np.concatenate([grid, sources], axis=1))}
        decoded = model.decode(latent_traj)

        for dim in range(decoded[modality].size(1)):
            ax.plot(decoded[modality].detach().numpy()[:,dim],
                    c=colors[color_index], alpha=source_value, linestyle='-')
            color_index += 1
plt.show()


#%% Test the times


fig, ax = plt.subplots(1,2)



grid = np.linspace(-1, 1, 20).reshape(-1,1)
sources = np.zeros(shape=(grid.shape[0],source_dim-1))

for tau in np.linspace(0,0.5,15):
    times = grid+tau

    latent_traj = {'adas_scores' : torch.Tensor(np.concatenate([times, sources], axis=1))}
    decoded = model.decode(latent_traj)

    for dim in range(decoded['adas_scores'].size(1)):
        ax[0].plot(decoded['adas_scores'].detach().numpy()[:,dim],
                c=colors[dim], alpha=1-2*tau, linestyle='-')
ax[0].set_title('Tau Effect')


for alpha in np.linspace(1,0.5,15):
    times = grid*alpha

    latent_traj = {'adas_scores' : torch.Tensor(np.concatenate([times, sources], axis=1))}
    decoded = model.decode(latent_traj)

    for dim in range(decoded['adas_scores'].size(1)):
        ax[1].plot(decoded['adas_scores'].detach().numpy()[:,dim],
                c=colors[dim], alpha=alpha, linestyle='-')
ax[1].set_title('AlpHa Effect')

plt.show()

