import numpy as np
import pickle
import os
import torch
torch.manual_seed(0)
np.random.seed(0)
from torch.utils.data import DataLoader
from models.longitudinal_models.abstract_model import load_model
import pandas as pd
# Paths




#%%

def compute_fold_error(fold, type, keys, splits, results_dir, experiment):

    # Load Model
    name_dir = 'fold_{0}'.format(fold)
    directory = os.path.join(results_dir, name_dir)
    output_dir_multimodal = os.path.join(directory, 'adas_{0}'.format(type))
    _, _, _, model = load_output(output_dir_multimodal,  keys ,['train'], 'shallow')

    # Load Dataset
    dataset_multimodal_train = experiment.generate_datasets_from_fold(fold, 'train', type)
    dataset_multimodal_test = experiment.generate_datasets_from_fold(fold, 'test', type)
    test_dataloader = DataLoader(dataset_multimodal_test)
    train_dataloader = DataLoader(dataset_multimodal_train)

    # Compute Residuals
    train_ids, train_residuals, _, _ = model.compute_residuals(train_dataloader)
    test_ids, test_residuals, _, _ = model.compute_residuals(test_dataloader)

    residuals = {'train':train_residuals,
                 'test':test_residuals}


    fold_error_train = dict.fromkeys(keys)
    fold_error_test = dict.fromkeys(keys)

    for key in keys:
        fold_error_train[key] = np.mean(np.array(residuals['train'][key]))
        fold_error_test[key] = np.mean(np.array(residuals['test'][key]))


    return fold_error_train, fold_error_test

def load_output(output_dir, keys, splits=['train'], depth='shallow'):
    residuals, latent_positions, latent_trajectories = None, None, None

    # Load model
    model = load_model(os.path.join(output_dir, 'model'), depth=depth)
    print("Load model at : {0}".format(os.path.join(output_dir, 'model')))

    return residuals, latent_positions, latent_trajectories, model


#%%

name = 'Adas_Exp'
results_dir = '../output/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_v6(testmeanstd)_final/'.format(name)

# Load experiment
experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))
depth = "shallow"
splits=['train','test']

folds = list(range(10))



#%%




model_info = ('unimodal', ['adas_scores'])

folds_error_train = dict.fromkeys(folds)
folds_error_test = dict.fromkeys(folds)


for fold in folds:
        type, keys = model_info
        folds_error_train[fold], folds_error_test[fold] = compute_fold_error(fold, type, keys, splits, results_dir, experiment)


print(model_info)
print(pd.DataFrame(folds_error_train).mean(axis=1))
print(pd.DataFrame(folds_error_test).mean(axis=1))

#%%


model_info = ('multimodal_2-2', ['memory+language','praxis+concentration'])

folds_error_train = dict.fromkeys(folds)
folds_error_test = dict.fromkeys(folds)


for fold in folds:
        type, keys = model_info
        folds_error_train[fold], folds_error_test[fold] = compute_fold_error(fold, type, keys, splits, results_dir, experiment)


print(model_info)
print(pd.DataFrame(folds_error_train).mean(axis=1))
print(pd.DataFrame(folds_error_test).mean(axis=1))

#%%

model_info = ('multimodal_4-1', ['memory','language','praxis','concentration'])

folds_error_train = dict.fromkeys(folds)
folds_error_test = dict.fromkeys(folds)


for fold in folds:
        type, keys = model_info
        folds_error_train[fold], folds_error_test[fold] = compute_fold_error(fold, type, keys, splits, results_dir, experiment)


print(model_info)
print(pd.DataFrame(folds_error_train).mean(axis=1))
print(pd.DataFrame(folds_error_test).mean(axis=1))



#%% Pruning


#%% 0.5

pruning_number = 21
pruning_frequency = 0.5

results_dir = os.path.join(
    "/teams/ARAMIS/HOME/raphael.couronne/working_dir/collab_maxime/output/pruningv{1}/noalphatau_estimateTrue_shallow_dim3_nopredecoder_2sources_pruning_2-2_{0}/".format(
        pruning_frequency, pruning_number))

# Load experiment
experiment = pickle.load(open(os.path.join(results_dir, '{0}.p'.format(name)), 'rb'))
depth = "shallow"
splits=['train','test']

folds = list(range(10))


model_info = ('multimodal_2-2', ['memory+language','praxis+concentration'])

folds_error_train = dict.fromkeys(folds)
folds_error_test = dict.fromkeys(folds)


for fold in folds:
        type, keys = model_info
        folds_error_train[fold], folds_error_test[fold] = compute_fold_error(fold, type, keys, splits, results_dir, experiment)


print(model_info)
print(pd.DataFrame(folds_error_train).mean(axis=1))
print(pd.DataFrame(folds_error_test).mean(axis=1))
