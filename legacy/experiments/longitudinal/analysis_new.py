"""

Author : Raphael Couronne

What can we do with a model ???

-Study Tau / Xi / Sources

-Visualize Tau / Xi / Sources effects. As well as their gradient ???


Ideas :
- Predict also the error ?
-Or get a bootstrap approximation of the model via sampling in latent space in variational ?

-Only autoencoder and deltaT and predict what's next


"""

#%%

import os
import numpy as np
from torch.utils.data import DataLoader
import torch

torch.manual_seed(0)
np.random.seed(0)
use_cuda = False


#%% Load



folder_longitudinal =  "/network/lustre/dtlake01/aramis/users/raphael.couronne/projects/MICCAI_2020/output/output_longitudinal/"
#experient_folder = "output_real_pd_2D64_no-init/exp_2020-3-1___0_archived/output_multimodal_synthetic_pd_9"
#experient_folder = "PPMI-2D64_SpaceTime_variational/exp_2020-3-12___6/output_multimodal_synthetic_pd_9"
#experient_folder = "PPMI-2D64_RandomSlopeModel_variational0/exp_2020-3-13___2/output_multimodal_synthetic_pd_9"
#experient_folder = "PPMI-2D64_SpaceTimeModel_variational0/exp_2020-3-13___3/output_multimodal_synthetic_pd_9"
#experient_folder = "PPMI-2D64_cAETimeModel_variational0/exp_2020-3-13___4/output_multimodal_synthetic_pd_9"
#experient_folder = "PPMI-2D64_cAETimeModel_variational1/exp_2020-3-13___1/output_multimodal_synthetic_pd_9"
#experient_folder = "PPMI-2D64_cAESpaceModel_variational0/exp_2020-3-13___1/output_multimodal_synthetic_pd_9"
experient_folder = "PPMI-2D64_StanleyModel_variational0/exp_2020-3-13___15/output_multimodal_synthetic_pd_9"
experient_folder = "DSprite_RandomSlopeModel_variational0/exp_2020-3-16___3_archived/output_multimodal_synthetic_pd_9"
experient_folder = "DSprite_SpaceTimeModel_variational0/exp_2020-3-16___3_archived/output_multimodal_synthetic_pd_9"
#experient_folder = "DSprite_RandomSlopeModel_variational0/exp_2020-3-16___3_archived/output_multimodal_synthetic_pd_9"
experient_folder = ("PPMI-2D64_cAETimeModel_variational0/exp_2020-3-13___2/output_multimodal_synthetic_pd_9")
experient_folder = "PPMI-2D64_SpaceTimeModel_variational0/exp_2020-3-14___0/output_multimodal_synthetic_pd_9"
experient_folder = "PPMI-2D64_RandomSlopeModel_variational0/exp_2020-3-14___1/output_multimodal_synthetic_pd_9"
model_name = experient_folder.split("_variational")[0].split("_")[1]

folder_path = os.path.join(folder_longitudinal, experient_folder)


shape = (64,64)

analysis_path = os.path.join(folder_path, "analysis")

if not os.path.exists(analysis_path):
    os.makedirs(analysis_path)

# Load model
model_path = os.path.join(folder_path, "model")


if model_name == "RandomSlopeModel":
    from longitudinal_models.random_slope_model import load_model
    model = load_model(model_path)
elif model_name == "cAETimeModel":
    from longitudinal_models.cAE_time_model import load_model
    model = load_model(model_path)
elif model_name == "cAESpaceModel":
    from longitudinal_models.cAE_space_model import load_model
    model = load_model(model_path)
elif model_name == "SpaceTimeModel":
    from longitudinal_models.space_time_model import load_model
    model = load_model(model_path)
elif model_name == "StanleyModel":
    from longitudinal_models.stanley_model import load_model
    model = load_model(model_path)
else:
    raise ValueError("Model not recognized")





# Load Data
train_data_path = os.path.join(folder_path, "train_dataset.p")
test_data_path = os.path.join(folder_path, "test_dataset.p")
from datasets.multimodal_dataset import load_multimodal_dataset
train_dataset = load_multimodal_dataset(train_data_path, None)
test_dataset = load_multimodal_dataset(test_data_path, None)

#train_dataset.set_teacher_forcing(True)
#test_dataset.set_teacher_forcing(True)

from datasets.utils import collate_fn_concat
dataloader = DataLoader(train_dataset, batch_size=1, shuffle=False, collate_fn=collate_fn_concat)

#%% Visualize over
batch =  collate_fn_concat([train_dataset[5]])
print("Do twice mean")
mean, _ = model.encode(batch)
print(mean.sum())
mean, _ = model.encode(batch)
print(mean.sum())
latent_trajectories = model.get_latent_trajectories(batch, mean)
output,_ = model.decode(latent_trajectories)


#%%

output_baseline = output['dat'][0,0,:,:]
slice = output_baseline[:,:].detach().numpy()

import matplotlib.pyplot as plt
plt.switch_backend('agg')

import matplotlib
print( matplotlib.get_backend())

fig, ax = plt.subplots(2,1)

input_baseline = batch['dat']['values'][1, 0, :, :].detach().numpy()
ax[0].imshow(input_baseline)
ax[1].imshow(slice)
plt.savefig(os.path.join(analysis_path, "imshow.pdf"))
plt.show()
#plt.close()

#%% Get std of the dimensions

latent_z = []
idx_z = []
times_baseline = []

for i, batch in enumerate(dataloader):
    latent_z.append((model.encode(batch)[0]).reshape(1,-1))
    idx_z.append(batch['idx'])
    times_baseline.append(float(batch['dat']['times'][0][0]))

    if i>2000:
        break

latent_z = torch.cat(latent_z)

std_1 = latent_z.std(axis=0)

if model_name in ["cAETimeModel", "cAESpaceModel"]:
    std_1 = torch.cat([std_1, torch.Tensor([1.0])])


#%% Do it without grads, with grad approximation

delta = 0.5

num_features = latent_trajectories['dat'].shape[1]

fig, ax = plt.subplots(num_features, 1, figsize=(6,30))

# First ones are the sources, last one is the tau. Xsi is not shown
for dim in range(num_features):

    first_visit = latent_trajectories['dat'][0].clone()
    second_visit = latent_trajectories['dat'][0].clone()
    before_visit = latent_trajectories['dat'][0].clone()
    second_visit[dim] = first_visit[dim] + delta*std_1[dim]
    before_visit[dim] = first_visit[dim] - delta * std_1[dim]

    trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

    output,_ = model.decode(trajs)

    output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

    slice = output_diff[:,:].detach().numpy()

    img = ax[dim].imshow(slice, cmap='jet')
    ax[dim].set_title(dim)
    cbar = fig.colorbar(img, ax=ax[dim], extend='both')
plt.savefig(os.path.join(analysis_path, "finite_difference_all.pdf".format(dim)))
plt.show()
plt.close()



#%% Show tsne of patients source

import numpy as np
from sklearn.manifold import TSNE

if model_name in ["RandomSlopeModel", "SpaceTimeModel", "StanleyModel"]:
    X_to_embedd = latent_z.detach().numpy()[:,:-2]
elif model_name in ["cAETimeModel", "cAESpaceModel"]:
    X_to_embedd = latent_z.detach().numpy()

X_embedded = TSNE(n_components=2, init="pca", random_state=0).fit_transform(X_to_embedd)

#%%


fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(X_embedded[:, 0],
           X_embedded[:, 1], alpha=0.5)

for j, patient in enumerate(idx_z):
    ax.text( X_embedded[j,0], X_embedded[j,1], patient[0], fontsize=6)


plt.savefig(os.path.join(analysis_path, "tsne.pdf"))
plt.show()



#%% Plot the t-sne with age


fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(X_embedded[:, 0],
           X_embedded[:, 1],
           c = times_baseline,
           alpha=0.5)

for j, patient in enumerate(idx_z):
    ax.text( X_embedded[j,0], X_embedded[j,1], patient[0], fontsize=6)


plt.savefig(os.path.join(analysis_path, "tsne_age.pdf"))
plt.show()





#%%
fig, ax = plt.subplots(figsize=(10,10))


size = 3

min_x, max_x = min(X_embedded[:,0]), max(X_embedded[:,0])
min_y, max_y = min(X_embedded[:,1]), max(X_embedded[:,1])

for j, patient in enumerate(idx_z[:30]):

    x, y = X_embedded[j, 0], X_embedded[j, 1]

    img = train_dataset[j]['dat']['values'][0, 0]
    plot = ax.imshow(img, extent=[x-size/2, x+size/2,
                           y-size/2, y+size/2])

ax.set_xlim(min_x, max_x)
ax.set_ylim(min_y, max_y)

plt.savefig(os.path.join(analysis_path, "tsne_img.pdf"))
plt.show()

#%% Pred the time PCA - and performance of time prediction

from sklearn.decomposition import PCA
pca = PCA(n_components=2)

if model_name in ["RandomSlopeModel", "SpaceTimeModel", "StanleyModel"]:
    X_to_embedd = latent_z.detach().numpy()[:,:-2]
elif model_name in ["cAETimeModel", "cAESpaceModel"]:
    X_to_embedd = latent_z.detach().numpy()


X_embedded_pca = pca.fit_transform(X_to_embedd)

print(pca.explained_variance_ratio_)
print(pca.components_)


import numpy as np
from sklearn.linear_model import LinearRegression
X = X_embedded_pca
y = times_baseline
reg = LinearRegression().fit(X, y)




print("R2 of linear regr PCA / time : {}".format((reg.score(X,y))))


#%% Linear Regr

fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(reg.predict(X),
           y,
           alpha=0.5)
plt.savefig(os.path.join(analysis_path, "pca_age_linearregr.pdf"))
plt.show()



#%% Plot PCA

scale_arrow = 8

fig, ax = plt.subplots(figsize=(10,10))
ax.scatter(X_embedded_pca[:, 0],
           X_embedded_pca[:, 1],
           c = times_baseline,
           alpha=0.5)

for j, patient in enumerate(idx_z):
    ax.text( X_embedded_pca[j,0], X_embedded_pca[j,1], patient[0], fontsize=6)


ax.arrow(0,0, reg.coef_[0]/scale_arrow, reg.coef_[1]/scale_arrow, width = 0.06, color="red")


plt.savefig(os.path.join(analysis_path, "pca_age.pdf"))
plt.show()

#%% PCA Age with varying age for subjects

import matplotlib.cm as cm
n_patients = 40
n_sampling = 11

color_palette = cm.get_cmap('jet', n_sampling)
colors = color_palette(range(n_sampling))

fig, ax = plt.subplots(figsize=(10,10))
source_dim = 7

for i, batch in enumerate(dataloader):
    batch_times = batch['dat']['times'].clone()
    patient_info_source = []

    mean, _ = model.encode(batch)

    if model_name in ["RandomSlopeModel", "SpaceTimeModel", "StanleyModel"]:
        mean_to_embedd = mean.detach().numpy()[:, :-2].reshape(-1,1)
    elif model_name in ["cAETimeModel", "cAESpaceModel"]:
        mean_to_embedd = mean.detach().numpy().reshape(-1,1)

    x_real, y_real = pca.components_.dot(mean_to_embedd)
    ax.text(x_real, y_real, batch['idx'])



    for j, delta_time in enumerate(np.linspace(-0.5, 0.5, n_sampling)):
        batch['dat']['times'] = (batch_times+delta_time).clone()
        mean, _ = model.encode(batch)

        if model_name in ["RandomSlopeModel", "SpaceTimeModel", "StanleyModel"]:
            mean_to_embedd = mean.detach().cpu().numpy()[:, :-2].reshape(-1, 1)
        elif model_name in ["cAETimeModel", "cAESpaceModel"]:
            mean_to_embedd = mean.detach().cpu().numpy().reshape(-1, 1)

        patient_info_source.append(mean_to_embedd)

        sources_pca_delta = pca.components_.dot(mean_to_embedd)

        ax.plot(sources_pca_delta[0, :], sources_pca_delta[1, :], linestyle="--", alpha=0.5, c=colors[j])
        ax.scatter(sources_pca_delta[0, :], sources_pca_delta[1, :], alpha=0.5, c=colors[j])


    sources = np.concatenate(patient_info_source, axis=1)
    sources_pca = pca.components_.dot(sources)

    #ax.plot(sources_pca[0, :], sources_pca[1, :], linestyle="--", alpha=0.5, c=colors[j])
    #ax.scatter(sources_pca[0, :], sources_pca[1, :], alpha=0.5, c=colors[j])



    if i>n_patients:
        break


ax.arrow(0,0, reg.coef_[0]/scale_arrow, reg.coef_[1]/scale_arrow, width = 0.06, color="red")

plt.savefig(os.path.join(analysis_path, "pca_age_varying.pdf"))
plt.show()


#%%
res_1 = np.corrcoef(X_embedded_pca[:,0], times_baseline)
res_2 = np.corrcoef(X_embedded_pca[:,1], times_baseline)
res = [res_1, res_2]


min_x, max_x = np.quantile(X_embedded_pca[:,0], 0.1), np.quantile(X_embedded_pca[:,0], 0.9)
min_y, max_y = np.quantile(X_embedded_pca[:,1], 0.1), np.quantile(X_embedded_pca[:,1], 0.9)

min_x_plot, max_x_plot = -1,1
min_y_plot, max_y_plot = -1,1

sampling_images = 6
size_x = (max_x_plot-min_x_plot)/sampling_images
size_y = (max_y_plot-min_y_plot)/sampling_images

fig, ax = plt.subplots(figsize=(12,12))

for x_plot, x_pca in zip(np.linspace(min_x_plot+size_x/2, max_x_plot-size_x/2, sampling_images),
                         np.linspace(min_x, max_x, sampling_images)):
    for y_plot, y_pca in zip(np.linspace(min_y_plot + size_y / 2, max_y_plot - size_y / 2, sampling_images),
                             np.linspace(min_y, max_y, sampling_images)):

        sources = np.array([x_pca,y_pca]).dot(pca.components_)
        latent_position = np.concatenate([[0], sources])

        output,_ = model.decode({"dat" : torch.FloatTensor(latent_position).unsqueeze(0)})

        img = output['dat'][0,0].detach().numpy()
        plot_img = ax.imshow(img, extent=[x_plot - size_x / 2, x_plot + size_x / 2,
                                      y_plot - size_y / 2, y_plot + size_y / 2])


ax.set_xlabel("1")
ax.set_ylabel("2")

ax.set_xlim(min_x_plot, max_x_plot)
ax.set_ylim(min_y_plot, max_y_plot)

plt.title({"Corr 1 with time : {0:.2f}, 2 with time : {1:.2f}".format(res_1[0,1], res_2[0,1])})
plt.savefig(os.path.join(analysis_path, "pca_img_interpolate.pdf"))
plt.show()


#%%
fig, ax = plt.subplots(figsize=(10,10))


size = 0.12

min_x, max_x = min(X_embedded_pca[:,0]), max(X_embedded_pca[:,0])
min_y, max_y = min(X_embedded_pca[:,1]), max(X_embedded_pca[:,1])

for j, patient in enumerate(idx_z[:30]):

    x, y = X_embedded_pca[j, 0], X_embedded_pca[j, 1]

    img = train_dataset[j]['dat']['values'][0, 0]
    plot = ax.imshow(img, extent=[x-size/2, x+size/2,
                           y-size/2, y+size/2])

ax.set_xlim(min_x, max_x)
ax.set_ylim(min_y, max_y)

plt.savefig(os.path.join(analysis_path, "pca_img.pdf"))
plt.show()


#%% First 2 effects of PCA to plot :


delta = 0.5
fig, ax = plt.subplots(1, 2, figsize=(10,6))

dim = 0
for dim in range(2):

    first_visit = latent_trajectories['dat'][0].clone()
    second_visit = latent_trajectories['dat'][0].clone()
    before_visit = latent_trajectories['dat'][0].clone()
    second_visit[:-1] = first_visit[:-1] + delta*torch.FloatTensor(pca.components_[dim])
    before_visit[:-1] = first_visit[:-1] - delta*torch.FloatTensor(pca.components_[dim])

    trajs = {'dat' : torch.cat([before_visit, second_visit]).reshape(-1,num_features)}

    output,_ = model.decode(trajs)

    output_diff = (output['dat'][1]-output['dat'][0]).reshape(shape)

    slice = output_diff[:,:].detach().numpy()

    img = ax[dim].imshow(slice, cmap='jet')
    ax[dim].set_title("PCA dim {0} -- Corr time : {1:.2f}".format(dim+1, res[dim][0,1] ))
    cbar = fig.colorbar(img, ax=ax[dim], extend='both')
plt.savefig(os.path.join(analysis_path, "finite_difference_pca.pdf".format(dim)))
plt.show()
plt.close()






#%% Do Correlations

data_folder = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data"

import pandas as pd
df_covariables = pd.read_csv(os.path.join(data_folder, "Parkinson", "Processed","PPMI", "df_covariables.csv"))
df_covariables = df_covariables.set_index('Unnamed: 0')
df_covariables = df_covariables.rename(index={'Unnamed: 0': 'id'})
df_metadata = pd.read_csv(os.path.join(data_folder, "Parkinson", "Processed","PPMI", "df_ppmi_metadata.csv"))


df_z = pd.DataFrame(latent_z.detach().numpy(), index = np.array(idx_z).reshape(-1))
df_covariables.rename(index={None: 'id'})

df_analysis = df_z.join(df_covariables)

if model_name not in  ["cAETimeModel", "cAESpaceModel"]:
    df_analysis_corr = df_analysis.corr().iloc[:(num_features+1), (num_features+1):]
else:
    df_analysis_corr = df_analysis.corr().iloc[:(num_features - 1), (num_features - 1):]

import seaborn as sns
fig, ax = plt.subplots(figsize=(14,14))
ax = sns.heatmap(np.abs(df_analysis_corr), annot=True, fmt=".2f")
plt.savefig(os.path.join(analysis_path, "corr_abs.pdf"))
plt.show()

fig, ax = plt.subplots(figsize=(14,14))
ax = sns.heatmap(df_analysis_corr, annot=True, fmt=".2f")
plt.savefig(os.path.join(analysis_path, "corr.pdf"))
plt.show()

fig, ax = plt.subplots(figsize=(20,20))
ax = sns.heatmap(df_analysis.corr(), annot=True, fmt=".2f")
plt.savefig(os.path.join(analysis_path, "corr_all.pdf"))
plt.show()


#%% Now test if sources are robust

for num_visit_removed in range(3):

    patients_sources_std = []
    patients_sources_mean = []
    patients_sources_full = []


    latent_this_subject_list = []

    for i, batch in enumerate(dataloader):

        # Sub batch
        num_visits = len(batch['dat']['times'])

        if num_visits>3:

            visits = list(range(num_visits))


            latent_this_subject = []

            for ii in range(20):
                #visits_kept = visits.copy()
                #visits_kept.remove(visit_removed)

                visits_kept = np.sort(np.random.choice(visits, num_visits - num_visit_removed, replace=False))

                print(i, visits, visits_kept, num_visit_removed)

                batch_kept = {
                    "dat":
                        {
                            "idx" : batch["idx"].copy(),
                            "values" : batch["dat"]["values"].clone(),
                            #"ages" : batch["dat"]["ages"].clone(),
                            "times": batch["dat"]["times"].clone(),
                            "times_list": [batch["dat"]["times_list"][0].clone()],
                            "lengths": batch["dat"]["lengths"].copy()
                        },
                    "idx":batch["idx"].copy()
                }

                for key in batch['dat'].keys():
                    #print(key)
                    if key == "lengths":
                        batch_kept['dat'][key] = [len(visits_kept)]
                    elif key == "times_list":
                        batch_kept['dat'][key] = [batch["dat"][key][0][visits_kept].clone()]
                    elif key != "idx":
                        batch_kept['dat'][key] = batch_kept['dat'][key][visits_kept]
                    elif key == "positions":
                        pass



                mean, _ = model.encode(batch_kept)
                latent_trajectories = model.get_latent_trajectories(batch_kept, mean)
                #print(latent_trajectories)

                latent_this_subject.append(latent_trajectories['dat'][0,:-1].reshape(-1,1))

            latent_this_subject_list.append(latent_this_subject)

            mean,_ = model.encode(batch)
            latent_trajectories = model.get_latent_trajectories(batch, mean)
            subjects_full = latent_trajectories['dat'][0,:-1].reshape(-1,1)

            subject_mean = torch.std(torch.cat(latent_this_subject, dim=1), dim=1)
            subject_std = torch.mean(torch.cat(latent_this_subject, dim=1), dim=1)

            patients_sources_std.append(subject_std.reshape(-1,1))
            patients_sources_mean.append(subject_mean.reshape(-1,1))
            patients_sources_full.append(subjects_full.reshape(-1,1))

        if i>40:
            break


    #%%

    patients_sources_mean = torch.cat(patients_sources_mean, dim=1)
    patients_sources_std = torch.cat(patients_sources_std, dim=1)
    patients_sources_full = torch.cat(patients_sources_full, dim=1)

    #%%

    print("std between patients full", patients_sources_full.std(dim=1))
    print("std between patients mean", patients_sources_mean.std(dim=1))
    print("mean of patients std", patients_sources_std.mean(dim=1))


    #%% Plot some patients

    fig, ax = plt.subplots(1,1)

    n_patients = 20

    import matplotlib.cm as cm

    color_palette = cm.get_cmap('jet', n_patients)
    colors = color_palette(range(n_patients))

    for i in range(n_patients):
        X_embedded_patient_array = []
        for randomized_source in latent_this_subject_list[i]:
            X_embedded_patient_array.append(pca.components_.dot(randomized_source.detach().numpy()))

        for j, embedded in enumerate(X_embedded_patient_array):
            ax.text(embedded[0, 0], embedded[1, 0],i, fontsize=6, color=colors[i])

        X_embedded_patient_array = np.array(X_embedded_patient_array)

        ax.scatter(X_embedded_patient_array[:,0,0],
                   X_embedded_patient_array[:,1,0],
                   c = colors[i], alpha=0)



    plt.savefig(os.path.join(analysis_path, "intra-inter_representation_minus{}visits.pdf".format(num_visit_removed)))
    plt.show()
    plt.close()


#%% Plot the robustness of patient on 10-20 patients at random

print("coucou")
import pandas as pd

patient = 0
source_dim = 7

batch = next(iter(dataloader))
batch_times = batch['dat']['times'].clone()

if model_name not in  ["cAETimeModel", "cAESpaceModel"]:
    patient_info = dict.fromkeys(['tau', 'xi']+["source_{}".format(i) for i in range(source_dim)])
else:
    patient_info = dict.fromkeys(["source_{}".format(i) for i in range(source_dim)])


for key in patient_info.keys():
    patient_info[key] = []

for delta_time in np.linspace(-0.5, 0.5, 10):
    batch['dat']['times'] = (batch_times+delta_time).clone()
    mean,_ = model.encode(batch)

    for i in range(source_dim):
        patient_info["source_{}".format(i)].append(mean[0,i].detach().cpu().numpy())

    if model_name not in  ["cAETimeModel", "cAESpaceModel"]:
        patient_info["tau"].append(mean[0,-2].detach().cpu().numpy())
        patient_info["xi"].append(mean[0,-1].detach().cpu().numpy())

df_patient = pd.DataFrame(patient_info)
df_patient["idx"] = batch['idx'][0]
df_patient["delta_time"] = np.linspace(-0.5, 0.5, 10)


fig, ax = plt.subplots(1,1)

if model_name not in  ["cAETimeModel", "cAESpaceModel"]:
    ax.plot(np.linspace(-0.5, 0.5, 10), df_patient["tau"], c="black")
    ax.plot(np.linspace(-0.5, 0.5, 10), df_patient["xi"], c="red")

for i in range(source_dim):
    ax.plot(np.linspace(-0.5, 0.5, 10), df_patient["source_{}".format(i)], alpha=0.2)

plt.savefig(os.path.join(analysis_path, "disentanglement.pdf"))


#%%

