from joblib import Parallel, delayed
from legacy.experiments.longitudinal import Benchmark_multimodal_cross




#%% Launch

name = 'Cross_experiment_v5'

output_dir = '../output/benchmark_{0}/'.format(name)
project_data_path = '../data/cross_synthetic_dataset'

paths = {'data': project_data_path, 'output_dir': output_dir}
pre_encoder_dim = 16
pre_decoder_dim = 16
latent_space_dim = 6
random_slope = False
n_epochs = 5000
n_jobs = 15
n_runs = 15
validation = (20, 10)

nn_parameters = latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs

experiment = Benchmark_multimodal_cross(paths, nn_parameters, seed=0,
                       validation=validation, name=name, patients='intersection',  test_validation=True)

def compute_experiment_1_fold(experiment, fold, data_type='multimodal'):
    return experiment.compute_fold(fold, data_type)



Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i,'multimodal') for i in range(n_runs))


#compute_experiment_1_fold(experiment, 0, data_type='multimodal')


# No Parallel Computing
#for fold in range(5):
#    compute_experiment_1_fold(experiment, fold, data_type='multimodal')


