import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
from legacy.estimate_adversarial import estimate_random_slope_model_adversarial
import pickle as pickle
import numpy as np
from utils.utils import plot_sampler_pca
from legacy.samplers import NormalSampler

output_dir = '../output_synthetic_score_clustering'
# data_dir = '../synthetic_cross_datasets/noise_0.02/train_mixture_0'
data_dir = '../synthetic_cross_datasets/noise_0.02/train_0'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

values = np.loadtxt(os.path.join(data_dir, 'scalar_values.txt'))
_, data_dim = values.shape
times = np.loadtxt(os.path.join(data_dir, 'times.txt'))
ids = np.loadtxt(os.path.join(data_dir, 'ids.txt'))

labels = {}
for id in set(ids):
    labels[id] = 0
# labels = pickle.load(open(os.path.join(data_dir, 'labels.p'), 'rb'))
# n_pos = sum([labels[key] for key in labels.keys()])
# print('Working with two classes with {} and {} elements'.format(n_pos, len(labels)-n_pos))

dataset = LongitudinalScalarDataset(
    ids,
    values,
    times
)

latent_space_dim = 2
random_slope = False

random_slope_model = RandomSlopeModel(
    data_dim=data_dim,
    latent_space_dim=latent_space_dim,
    is_image=False,
    encoder_hidden_dim=32,
    decoder_hidden_dim=32,
    labels=['score_1', 'score_2'],
    initial_slope_norm=1.,
    random_slope=random_slope
)


pickle.dump((dataset, labels), open(os.path.join(output_dir, 'dataset_info.p'), 'wb'))

# sampler = BiSampler(dimension=random_slope_model.encoder_dim, bi_dimension=1, weights=[n_pos/len(labels), 1-n_pos/len(labels)])   # mix on the intercepts to try.

sampler = NormalSampler(dimension=random_slope_model.encoder_dim)

plot_sampler_pca(sampler, os.path.join(output_dir, 'prior_distribution.pdf'))

estimate_random_slope_model_adversarial(random_slope_model, dataset, n_epochs=10000,
                             learning_rate=5e-5, output_dir=output_dir,
                             batch_size=16, save_every_n_iters=50, sampler=sampler, update_sampler=False)


