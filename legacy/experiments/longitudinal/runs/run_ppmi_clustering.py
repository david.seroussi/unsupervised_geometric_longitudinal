import os
from datasets.longitudinal_scalar_dataset import LongitudinalScalarDataset
from models.longitudinal_models.abstract_model import RandomSlopeModel
# from estimate_random_slope_model import estimate_random_slope_model
from legacy.estimate_adversarial import estimate_random_slope_model_adversarial
import pickle as pickle
import pandas as pd
from utils.utils import plot_sampler_pca
from legacy.samplers import BiSampler


data_dir = '../data_ppmi'
output_dir = '../output_ppmi_clustering'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

data = pd.read_csv(os.path.join(data_dir, 'df_allscores_allcohorts.csv'))

# Selecting PD subjects:
input = data.loc[(data['ENROLL_CAT'].isin(['HC', 'PD']))]
print(input.head())
input = input[['Subject Identifier', 'MDS_Rigidity', 'MDS_Bradykinesia', 'MDS_VoiceFace', 'MDS_Gait', 'MDS_sum', 'ENROLL_CAT', 'Age at Visit']]
input = input.dropna()

input = input.values
ids = input[:, 0]
values = input[:, 1:6]
enroll_cats = input[:, 6]
times = input[:, -1]

labels = {}
for i, rid in enumerate(ids):
    if enroll_cats[i] == 'HC':
        labels[rid] = 0
    else:
        labels[rid] = 1
n_pd = sum([labels[rid] for rid in labels.keys()])


print('Working with two classes with {} and {} elements'.format(n_pd, len(labels)-n_pd))

dataset = LongitudinalScalarDataset(
    ids,
    values,
    times
)

latent_space_dim = 2
random_slope = False

random_slope_model = RandomSlopeModel(
    data_dim=5,
    latent_space_dim=latent_space_dim,
    is_image=False,
    encoder_hidden_dim=32,
    decoder_hidden_dim=32,
    labels=['MDS_Rigidity', 'MDS_Bradykinesia', 'MDS_VoiceFace', 'MDS_Gait', 'MDS_sum'],
    initial_slope_norm=3.5,
    random_slope=random_slope
)


pickle.dump((dataset, labels), open(os.path.join(output_dir, 'dataset_info.p'), 'wb'))

sampler = BiSampler(dimension=random_slope_model.encoder_dim, bi_dimension=0, weights=[n_pd/len(labels), 1-n_pd/len(labels)])   # mix on the intercepts to try.

plot_sampler_pca(sampler, os.path.join(output_dir, 'prior_distribution.pdf'))

estimate_random_slope_model_adversarial(random_slope_model, dataset, n_epochs=10000,
                             learning_rate=5e-5, output_dir=output_dir,
                             batch_size=16, save_every_n_iters=50, sampler=sampler, update_sampler=True)


