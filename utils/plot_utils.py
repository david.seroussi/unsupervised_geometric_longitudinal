
from PIL import Image
import numpy as np
import torch
import matplotlib.pyplot as plt
from torchvision.utils import make_grid

def fig_reconstructions(batch, reconst, key):

    n_patients_to_plot = 3

    grid_patients = []
    width_plot = []

    for i in range(n_patients_to_plot):
        pos_patient = range(batch[key]["positions"][i], batch[key]["positions"][i+1])
        to_save_patient = torch.cat([batch[key]["values"][pos_patient], reconst[key][pos_patient]]).detach().cpu()
        grid_patient = make_grid(to_save_patient, padding=10, normalize=False, range=(0, 1), nrow=len(pos_patient), scale_each=False)

        grid_patients.append(grid_patient)
        width_plot.append(grid_patient.shape[2])

    max_width_plot = max(width_plot)

    for i in range(n_patients_to_plot):
        if width_plot[i]<max_width_plot:
            grid_patients[i] = torch.cat([grid_patients[i],
                                         torch.zeros(size=(grid_patients[i].shape[0],
                                                           grid_patients[i].shape[1],
                                                           max_width_plot-grid_patients[i].shape[2]))],axis=2)

    grid = torch.cat(grid_patients, axis=1)

    npimg = grid.numpy()
    fig, ax = plt.subplots(1,1)
    ax.imshow(np.transpose(npimg, (1, 2, 0)), interpolation='nearest')
    return fig



