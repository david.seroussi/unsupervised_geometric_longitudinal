from datasets.multimodal_dataset import DatasetTypes
from torch.utils.data import DataLoader
import numpy as np
import os
from utils.utils import build_train_val_test_datasets
from datasets.utils import collate_fn_concat

from models.cross_sectional_litmodel import CrossSectionalLitmodel
from models.networks import Autoencoder2D64


from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint


#%% Parameters

dir_path = os.path.dirname(os.path.realpath(__file__))

if dir_path.split("/")[1] == "network":
    "Using lustre for computation"
    root_machine = "/network/lustre/dtlake01/aramis/users/raphael.couronne"
else:
    "Using local machine for computation"
    root_machine = '/Users/raphael.couronne/Programming/ARAMIS'


# Optim parameters
min_epochs, max_epochs = 1, 10
batch_size = 16
learning_rate = 1e-3
use_cuda = True
variational = False

# Data parameters
num_visits = 10000
train_ratio = 3/4

# Model parameters
latent_space_dim = 6


root_dir = os.path.join(root_machine,
                        'Results/Unsupervised/Longitudinal_Autoencoder/dSpriteCrossSectional/{}visits_{}variational_{}latent'.format(
    num_visits, variational, latent_space_dim))
if not os.path.exists(root_dir):
    os.makedirs(root_dir)


# %% Import data and get infos data

# Dataset
feature_name = "sprite"
from inputs.access_datasets import access_dSprite2Longitudinal
ids, times, paths, image_dimension, image_shape, df = access_dSprite2Longitudinal()
data_info = {feature_name: (DatasetTypes.IMAGE, 16, 16, image_shape, None, None)}

# Subselect visits
ids = np.array(ids)[:num_visits]
times = np.array(times)[:num_visits]
images_path = np.array(paths)[:num_visits]


# %% Create datasets

def transform(data):
    # Add a random delta age 50% of the time
    do_time_delta = np.random.binomial(1, p=0.5, size=1)
    if do_time_delta:
        data['times'] = data['times'] + np.random.normal(scale=0.05)

train_dataset, val_dataset, test_dataset = build_train_val_test_datasets(
    ids, times, images_path, image_shape, image_dimension,
    feature_name,
    transform=lambda x:x, use_cuda=False, train_ratio=0.75)

train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat)
val_dataloader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn_concat)
test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, collate_fn=collate_fn_concat)

#%%

# Model
#model = Autoencoder2D64(latent_space_dim=latent_space_dim)
model = Autoencoder2D64(variational=variational, latent_space_dim=latent_space_dim)
litmodel = CrossSectionalLitmodel(model, learning_rate=learning_rate)

# Checkpoint callback
checkpoint_callback = ModelCheckpoint(
    save_top_k=True,
    verbose=True,
    monitor='loss_val',
    mode='min',
    prefix=''
)

# Training loop
trainer = Trainer(gpus=int(use_cuda), num_nodes=1, min_epochs=min_epochs, max_epochs=max_epochs,
                  default_root_dir=root_dir,
                  checkpoint_callback=checkpoint_callback)

trainer.fit(litmodel, train_dataloader, val_dataloader)
