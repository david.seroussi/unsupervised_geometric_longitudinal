import torch

def collate_fn_concat(batch):
    '''
    Padds batch of variable length

    note: it converts things ToTensor manually here since the ToTensor transform
    assume it takes in images rather than arbitrary tensors.
    '''

    idx = [x["idx"] for x in batch]
    patient_label = [x["patient_label"] for x in batch]
    n_batch_patients = len(idx)
    # n_batch_visits = len(idx)

    # TODO beter this data representation : idx / features and in features dat / scores / ...
    keys = [key for key in batch[0].keys() if key not in ["idx", "patient_label"]]

    batch_out = dict.fromkeys(["idx"] + keys)
    batch_out["idx"] = idx
    batch_out["patient_label"] = patient_label
    batch_len = len(idx)

    for key in keys:
        batch_out[key] = dict.fromkeys(["values", "times", "lengths"])

        # Aggregate list of values/times per patients
        data_batch_list = [batch[i][key]['values'].squeeze(0) for i in range(n_batch_patients)]
        times_batch_list = [batch[i][key]['times'].squeeze(0) for i in range(n_batch_patients)]

        # Concatenate
        data_batch = torch.cat(data_batch_list, dim=0)
        times_batch = torch.cat(times_batch_list, dim=0)

        batch_out[key]['values'] = data_batch
        batch_out[key]['times'] = times_batch
        batch_out[key]['times_list'] = times_batch_list
        batch_out[key]['lengths'] = [len(times) for times in times_batch_list]
        batch_out[key]['positions'] = [sum(batch_out[key]['lengths'][:i]) for i in range(batch_len + 1)]

        #batch_out[key]['idxs'] = [range(batch_out[key]["positions"][i], batch_out[key]["positions"][i + 1]) for i in
        #        range(len(batch_out["idx"]))]


    return batch_out