import torch
from torch.nn import functional as F
from pytorch_lightning.core.lightning import LightningModule
from argparse import ArgumentParser
from datasets.cross_sectional_image_dataset_dataset import CrossSectionalImageDataset


def compute_variance_ratio(batch, key, mu):
    idxs = [range(batch[key]["positions"][i], batch[key]["positions"][i + 1]) for i in
            range(len(batch["idx"]) - 1)]

    patient_vars = []
    patient_mus = []

    for idx in idxs:
        patient_var = mu[idx].var(axis=0)
        patient_mu = mu[idx].mean(axis=0)

        patient_vars.append(patient_var)
        patient_mus.append(patient_mu)

    inter_variance = torch.stack(patient_mus, axis=1).var(axis=0).mean()
    intra_variance = torch.stack(patient_vars, axis=1).mean(axis=0).mean()

    return intra_variance / inter_variance

class CrossSectionalLitmodel(LightningModule):

    def __init__(self, model, learning_rate, **kwargs):
        super().__init__()
        self.model = model
        self.learning_rate = learning_rate

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--learning_rate', type=float, default=1e-3)
        return parser

    def configure_optimizers(self):
        return torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)

    def forward(self, x):
        if self.model.variational:
            mu, logvar = self.model.encode(x)
            x = self.model.reparameterize(mu, logvar)
            x = self.model.decode(x)
            res = [x, mu, logvar]
        else:
            x = self.model(x)
            res = [x, x, None] # TODO mu is approximated as current x
        return res

    def training_step(self, batch, batch_idx):
        key = list(batch.keys())[1]
        x = batch[key]["values"]
        x_hat, mu, logvar = self(x)
        reconstruction_loss = F.mse_loss(x_hat, x)
        regularity_loss = 0
        if self.model.variational:
            kld_loss = torch.mean(-0.5 * torch.sum(1 + logvar - mu ** 2 - logvar.exp(), dim=1), dim=0)
            regularity_loss += kld_loss
        loss = reconstruction_loss + regularity_loss
        tensorboard_logs = {'train_reconst': reconstruction_loss,
                            'train_regularity': regularity_loss,
                            'train_loss': loss,
                            'train_variance_ratio': compute_variance_ratio(batch, key, mu)} # TODO and if no mu ???
        # Image logs
        if batch_idx == 1:
            from utils.plot_utils import fig_reconstructions
            fig = fig_reconstructions(batch, {key : x_hat}, key)
            self.logger.experiment.add_figure('Train reconstruction plt', fig, self.current_epoch)
        return {'loss': loss, 'log': tensorboard_logs}


    def validation_step(self, batch, batch_idx):
        #x = batch["image"]
        key = list(batch.keys())[1]
        x = batch[key]["values"]

        x_hat, mu, logvar = self(x)
        reconstruction_loss = F.mse_loss(x_hat, x)
        regularity_loss = 0
        if self.model.variational:
            kld_loss = torch.mean(-0.5 * torch.sum(1 + logvar - mu ** 2 - logvar.exp(), dim=1), dim=0)
            regularity_loss += kld_loss
        loss = reconstruction_loss + regularity_loss

        # Image logs
        if batch_idx == 1:
            from utils.plot_utils import fig_reconstructions
            fig = fig_reconstructions(batch, {key : x_hat}, key)
            self.logger.experiment.add_figure('val reconstruction plt', fig, self.current_epoch)
        return {'val_reconst': reconstruction_loss,
                            'val_regularity': regularity_loss,
                            'val_loss': loss,
                            'val_variance_ratio': compute_variance_ratio(batch, key, mu)}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        avg_reconst= torch.stack([x['val_loss'] for x in outputs]).mean()
        avg_reg = torch.stack([x['val_loss'] for x in outputs]).mean()
        avg_variance_ratio = torch.stack([x['val_variance_ratio'] for x in outputs]).mean()
        tensorboard_logs = {
            'val_loss': avg_loss,
            'val_regularity':avg_reg,
            'val_reconst': avg_reconst,
            'val_variance_ratio' : avg_variance_ratio,
        }
        return {'val_loss': avg_loss, 'log': tensorboard_logs}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['test_loss'] for x in outputs]).mean()
        tensorboard_logs = {'test_loss': avg_loss}
        return {'avg_test_loss': avg_loss, 'log': tensorboard_logs}