import os
import numpy as np
import torch.nn as nn
from models.networks import RCNN64, Deconv64, \
    CNN64
import pickle
from datasets.multimodal_dataset import DatasetTypes
#import nibabel as nib
from models.longitudinal_models.abstract_model import AbstractModel

import platform

if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')


class StanleyModel(AbstractModel):

    def __init__(self, **kwargs):
        """
        Constructor
        :param data_info: info about the used modalities:
        :param latent_space_dim: Dimension of the space of the trajectories
        :param pre_encoder_dim: Dimension output by each individual encoder
        :param pre_decoder_dim: Dimension output before decoding, common for all modalities
        :param use_cuda: Whether to use cuda for computations (if available)
        :param random_slope: Whether the slopes are also random
        :param variational: Whether to use a variational cost for estimation
        :param atlas_path: Atlas used to save pet images.
        """
        self.model_name = "StanleyModel"

        # TODO initialize
        self.encoders_time = {}
        self.encoders_space = {}
        self.decoders_time = {}
        self.decoders_space = {}
        self.space_shift = {}

        super(StanleyModel, self).__init__(**kwargs)

        self.initialize_encoder_and_decoder()
        self.update_types()

    def update_types(self):
        """
        Handles cuda and non-cuda types for all the tensors and neural networks attributes.
        """
        for key in self.encoders_time.keys():
            self.encoders_time[key].float()
            self.encoders_space[key].float()
            self.decoders_time[key].float()
            self.decoders_space[key].float()
            self.space_shift[key].float()
            #self.decoders[key].float()
            if self.use_cuda:
                self.encoders_time[key].cuda()
                self.encoders_space[key].cuda()
                self.decoders_time[key].cuda()
                self.decoders_space[key].cuda()
                self.space_shift[key].cuda()
                #self.decoders[key].cuda()

        for key in self.prior_parameters:
            self.prior_parameters[key] = self.prior_parameters[key].type(self.type).requires_grad_(True)

        self.mean_slope = self.mean_slope.type(self.type)

        self.fc_mu_time.float()
        self.fc_mu_space.float()
        if self.variational:
            self.fc_logvariances_time.float()
            self.fc_logvariances_space.float()
        if self.use_cuda:
            self.fc_mu_time.cuda()
            self.fc_mu_space.cuda()
            if self.variational:
                self.fc_logvariances_time.cuda()
                self.fc_logvariances_space.cuda()
        print(self.prior_parameters)

    def initialize_encoder_and_decoder(self):
        """
        Reads the info for the data at hand and initialize the needed encoders/decoders
        """
        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():
            if dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):
                    self.encoders_time[dataset_name] = RCNN64(rnn_hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    self.encoders_space[dataset_name] = CNN64(hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    self.decoders_time[dataset_name] = Deconv64(in_dim=1)
                    self.decoders_space[dataset_name] = Deconv64(in_dim=self.latent_space_dim-1)
                    self.decoders[dataset_name] = Deconv64(in_dim=self.latent_space_dim)
                    self.space_shift[dataset_name] = UNet(latent_space_dim=12, source_dim=self.latent_space_dim-1)
                else:
                    raise ValueError('Unexpected observation shape')

            else:
                raise ValueError('Unexpected dataset type')

        """

        if not self.monomodal:
            assert not self.variational, 'Variational not compatible with new merge op'
            self.merge_network = MergeNetwork(in_dim=len(self.data_info) * self.pre_encoder_dim, hidden_dim=8,
                                              out_dim=self.encoder_dim, depth=self.depth)
            #self.pre_decoder = PreDecoder(in_dim=self.latent_space_dim, hidden_dim=self.pre_decoder_dim,
            #                              out_dim=self.pre_decoder_dim, depth=self.depth)"""

        self.fc_mu_time = nn.Linear(self.pre_encoder_dim, 2)
        self.fc_mu_space = nn.Linear(self.pre_encoder_dim, self.latent_space_dim-1)
        if self.variational:
            self.fc_logvariances_time = nn.Linear(self.pre_encoder_dim,2)
            self.fc_logvariances_space = nn.Linear(self.pre_encoder_dim,self.latent_space_dim-1)

    def parameters(self):
        """
        All the parameters to be optimized.
        """
        out = []
        # TODO : remove self.decoders.keys() --> save key somewhere
        for key in self.decoders.keys():
            out = out + list(self.decoders_time[key].parameters())
            out = out + list(self.decoders_space[key].parameters())
            out = out + list(self.encoders_time[key].parameters())
            out = out + list(self.encoders_space[key].parameters())
            out2 = list(self.space_shift[key].parameters())
            #out = out + list(self.decoders[key].parameters())
        if not self.monomodal:
            raise ValueError("Not monomodal")
        out = out + list(self.fc_mu_time.parameters())
        out = out + list(self.fc_mu_space.parameters())
        if self.variational:
            out = out + list(self.fc_logvariances_time.parameters())
            out = out + list(self.fc_logvariances_space.parameters())

        return out, out2

    def encode(self, data, randomize_nb_obs=False):
        """
        :param data: dictionnary. Each entry is a dictionary which contains times and observations
        :return: a latent variable z.
        """
        #encoded = torch.from_numpy(np.zeros((len(self.data_info), self.pre_encoder_dim))).type(self.type)
        batch_len = len(data['idx'])
        encoded_mu = []
        #encoded_mu = torch.from_numpy(np.zeros((len(self.data_info),batch_len, self.pre_encoder_dim))).type(self.type)
        if self.variational:
            encoded_logvariance = torch.from_numpy(np.zeros((len(self.data_info), batch_len, self.pre_encoder_dim))).type(self.type)
        i = 0
        for key in self.data_info.keys():
            if key in data.keys():
                times = data[key]['times'].squeeze(0)
                values = data[key]['values'].squeeze(0)
                lengths = data[key]['lengths']
                positions = [sum(lengths[:i]) for i in range(batch_len + 1)]

                time = self.encoders_time[key].get_timereparam_output(times, values, lengths, positions).squeeze(0)
                space = self.encoders_space[key].get_space_output(values, lengths, positions).squeeze(0)

                #time_mu = time
                #space_mu = space

                time_mu = self.fc_mu_time(torch.tanh(time))
                space_mu = self.fc_mu_space(torch.tanh(space))

                if self.variational:
                    time_logvariance = 1e-5 + self.fc_logvariances_time(torch.tanh(time))
                    space_logvariance = 1e-5 + self.fc_logvariances_space(torch.tanh(space))

                # TODO handle thislater
                if len(space_mu.shape)==1:
                    space_mu = space_mu.reshape(1, -1)
                    if self.variational:
                        space_logvariance = space_logvariance.reshape(1, -1)


                encoded_mu.append(torch.cat([space_mu, time_mu], dim=1))

                if self.variational:
                    encoded_logvariance = torch.cat([space_logvariance, time_logvariance], dim=1)

                #encoded[i] = self.encoders[key].get_sequence_output(times, values, lengths, positions)
            i += 1

        encoded_mu = torch.cat(encoded_mu, dim=-1)

        # Merging the representations:
        #return #self._merge_representation(encoded)
        if self.variational:
            return encoded_mu, encoded_logvariance
        else:
            return encoded_mu, None



    def decode(self, latent_trajectories):

        out = {}
        out_mean = {}

        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:

                source_dim = self.source_dim

                decoded_time = self.decoders_time[key](latent_trajectories[key][:,-1].reshape(-1,1))
                sources = latent_trajectories[key][:, :source_dim].reshape(-1, source_dim)
                #decoded_space = self.decoders_space[key](sources)
                shifted = self.space_shift[key](decoded_time, sources)

                #decoded = self.decoders[key](latent_trajectories[key])
                #decoded_space = self.decoders_space[key](sources)


                #out[key] = shifted#decoded_time + decoded_space
                out[key] = shifted
                out_mean[key] = decoded_time

        return out, out_mean




    def get_mean_trajectory(self, min_x, max_x):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """

        x_out = {}
        mean_traj = {}

        for key, decoder in self.decoders_time.items():
            dataset_type, _, _, _, _, _ = self.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            x_np = np.linspace(min_x, max_x, nb_points)
            x_torch = torch.from_numpy(x_np).type(self.type)
            latent_traj_np = x_np.reshape(-1,1)
            latent_traj = torch.from_numpy(latent_traj_np).type(self.type)
            x_out[key] = latent_traj
            if self.monomodal:
                mean_traj[key] = decoder(latent_traj)
            else:
                # mean_traj[key] = decoder(self.pre_decoder(latent_traj))
                mean_traj[key] = decoder(latent_traj)
        return x_out, mean_traj


    def save(self, output_dir):
        super(StanleyModel, self).save(output_dir)

        model_save_dir = os.path.join(output_dir, 'model')

        for key in self.decoders_time.keys():
            torch.save(self.encoders_time[key].state_dict(), os.path.join(model_save_dir, key + '_encoder_time.p'))
            torch.save(self.encoders_space[key].state_dict(), os.path.join(model_save_dir, key + '_encoder_space.p'))
            torch.save(self.decoders_time[key].state_dict(), os.path.join(model_save_dir, key + '_decoder_time.p'))
            torch.save(self.space_shift[key].state_dict(), os.path.join(model_save_dir, key + '_space_shift.p'))



# TODO load model --> put in model info the model type, to use the good load model.

def load_model(folder, depth="normal"):
    (data_info, latent_space_dim, pre_encoder_dim,# pre_decoder_dim,
     use_cuda, random_slope, variational,
     prior_parameters_dict) = pickle.load(open(os.path.join(folder, 'model_info.p'), 'rb'))

    model = StanleyModel(data_info=data_info, latent_space_dim=latent_space_dim,
                             pre_encoder_dim=pre_encoder_dim,# pre_decoder_dim=pre_decoder_dim,
                             use_cuda=False, random_slope=random_slope,
                             variational=variational, depth=depth)

    model.set_prior_parameters(prior_parameters_dict)

    if not model.monomodal:
        model.merge_network.load_state_dict(torch.load(os.path.join(folder, 'merge_network.p'),
                                                  map_location=lambda storage, loc: storage))
        #model.pre_decoder.load_state_dict(torch.load(os.path.join(folder, 'pre_decoder.p'),
        #                                          map_location=lambda storage, loc: storage))

        if model.variational:
            model.fc_log_variances.load_state_dict(torch.load(os.path.join(folder, 'merge_layer_variances.p'),
                                                              map_location=lambda storage, loc: storage))

    for key in model.decoders.keys():
        model.encoders_time[key].load_state_dict(torch.load(os.path.join(folder, key + '_encoder_time.p'),
                                                       map_location=lambda storage, loc: storage))
        model.encoders_space[key].load_state_dict(torch.load(os.path.join(folder, key + '_encoder_space.p'),
                                                       map_location=lambda storage, loc: storage))
        model.decoders_time[key].load_state_dict(torch.load(os.path.join(folder, key + '_decoder_time.p'),
                                                       map_location=lambda storage, loc: storage))
        model.space_shift[key].load_state_dict(torch.load(os.path.join(folder, key + '_space_shift.p'),
                                                       map_location=lambda storage, loc: storage))


    return model



class Autoencoder2D64(nn.Module):
    def __init__(self, latent_space_dim=16, source_dim=7):
        super(Autoencoder2D64, self).__init__()

        self.encoder = nn.ModuleList([
            # 1
            nn.Conv2d(1, 4, kernel_size=3, stride=1, padding=1),  # 8 x 32 x 32
            nn.BatchNorm2d(4),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 2
            nn.Conv2d(4, 8,  kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.BatchNorm2d(8),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 3
            nn.Conv2d(8, 16, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
            nn.BatchNorm2d(16),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
            # 4
            nn.Conv2d(16, 32, kernel_size=3, stride=2, padding=1),  # 8 x 16 x 16
        ])

        ngf = 2

        self.decoder = nn.ModuleList([
            # 1
            nn.ConvTranspose2d(32, 16 * ngf, 3, stride=3),
            nn.BatchNorm2d(16 * ngf),
            nn.LeakyReLU(),

            # 2
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 3, stride=3, padding=1),
            nn.BatchNorm2d(8 * ngf),
            nn.LeakyReLU(),

            # 3
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 3, stride=2, padding=1),
            nn.BatchNorm2d(4 * ngf),
            nn.LeakyReLU(),

            # 3
            nn.ConvTranspose2d(4 * ngf, 2 * ngf, 3, stride=2, padding=0),
            nn.BatchNorm2d(2 * ngf),
            nn.LeakyReLU(),

            # 4
            nn.ConvTranspose2d(2 * ngf, 1, 2, stride=1),
            nn.Sigmoid()
        ])

        self.latent_space_dim = latent_space_dim
        self.source_dim = source_dim


        self.linear_encode = nn.Linear(128, self.latent_space_dim-source_dim)
        self.linear_decode = nn.Linear(self.latent_space_dim, 128)


    def encode(self, x):
        for layer in self.encoder:
            x = layer(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))
        return x

    def forward(self, x, source):
        for layer in self.encoder:
            x = layer(x)
        x = self.linear_encode(x.reshape(x.shape[0], -1))

        x = torch.cat([x, source], dim=1)

        x = self.linear_decode(x)
        x = x.reshape(x.shape[0], 32, 2, 2)
        for layer in self.decoder:
            x = layer(x)
        return x




from collections import OrderedDict
import torch
import torch.nn as nn

class UNet(nn.Module):

    def __init__(self, in_channels=1, out_channels=1, init_features=2,
                 latent_space_dim=12, source_dim=7):
        super(UNet, self).__init__()

        features = init_features
        self.encoder1 = UNet._block(in_channels, features, name="enc1")
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.encoder2 = UNet._block(features, features * 2, name="enc2")
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.encoder3 = UNet._block(features * 2, features * 4, name="enc3")
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.encoder4 = UNet._block(features * 4, features * 8, name="enc4")
        self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.bottleneck = UNet._block(features * 8, features * 16, name="bottleneck")

        self.upconv4 = nn.ConvTranspose2d(
            features * 16, features * 8, kernel_size=2, stride=2
        )
        self.decoder4 = UNet._block((features * 8) * 2, features * 8, name="dec4")
        self.upconv3 = nn.ConvTranspose2d(
            features * 8, features * 4, kernel_size=2, stride=2
        )
        self.decoder3 = UNet._block((features * 4) * 2, features * 4, name="dec3")
        self.upconv2 = nn.ConvTranspose2d(
            features * 4, features * 2, kernel_size=2, stride=2
        )
        self.decoder2 = UNet._block((features * 2) * 2, features * 2, name="dec2")
        self.upconv1 = nn.ConvTranspose2d(
            features * 2, features, kernel_size=2, stride=2
        )
        self.decoder1 = UNet._block(features * 2, features, name="dec1")

        self.conv = nn.Conv2d(
            in_channels=features, out_channels=out_channels, kernel_size=1
        )


        self.fc_reduce_1 = nn.Linear(32*4*4, 32)
        assert latent_space_dim>source_dim
        self.fc_reduce_2 = nn.Linear(32, latent_space_dim-source_dim)

        self.fc_augment_1 = nn.Linear(latent_space_dim, 32)
        self.fc_augment_2 = nn.Linear(32, 32*4*4)

    def forward(self, x, source):
        enc1 = self.encoder1(x)
        enc2 = self.encoder2(self.pool1(enc1))
        enc3 = self.encoder3(self.pool2(enc2))
        enc4 = self.encoder4(self.pool3(enc3))

        bottleneck = self.bottleneck(self.pool4(enc4))

        #print(bottleneck.shape)

        bottleneck = bottleneck.reshape(bottleneck.shape[0], -1)

        fc_reduce_1 = self.fc_reduce_1(torch.tanh(bottleneck.reshape(bottleneck.shape[0], -1)))
        fc_reduce_2 = self.fc_reduce_2(torch.tanh(fc_reduce_1))

        fc_reduce_2 = torch.cat([fc_reduce_2, source], dim=1)

        #print(fc_reduce_2.shape)

        fc_augment_1 = self.fc_augment_1(torch.tanh(fc_reduce_2))
        fc_augment_2 = self.fc_augment_2(torch.tanh(fc_augment_1))

        bottleneck = fc_augment_2.reshape(bottleneck.shape[0], 32, 4, 4)

        dec4 = self.upconv4(bottleneck)
        dec4 = torch.cat((dec4, enc4), dim=1)
        dec4 = self.decoder4(dec4)
        dec3 = self.upconv3(dec4)
        dec3 = torch.cat((dec3, enc3), dim=1)
        dec3 = self.decoder3(dec3)
        dec2 = self.upconv2(dec3)
        dec2 = torch.cat((dec2, enc2), dim=1)
        dec2 = self.decoder2(dec2)
        dec1 = self.upconv1(dec2)
        dec1 = torch.cat((dec1, enc1), dim=1)
        dec1 = self.decoder1(dec1)
        return torch.sigmoid(self.conv(dec1))

    @staticmethod
    def _block(in_channels, features, name):
        return nn.Sequential(
            OrderedDict(
                [
                    (
                        name + "conv1",
                        nn.Conv2d(
                            in_channels=in_channels,
                            out_channels=features,
                            kernel_size=3,
                            padding=1,
                            bias=False,
                        ),
                    ),
                    (name + "norm1", nn.BatchNorm2d(num_features=features)),
                    (name + "relu1", nn.LeakyReLU(inplace=True))]))