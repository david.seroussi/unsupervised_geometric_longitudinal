import torch
from torch.nn import functional as F
from pytorch_lightning.core.lightning import LightningModule
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
from torch.optim import Adam
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np
from sklearn.feature_selection import mutual_info_regression
from utils.mmd_loss import MMD_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score

class LongitudinalLitModel(LightningModule):

    def __init__(self, longitudinal_model, mmd=0, learning_rate=1e-3):
        super().__init__()
        self.model = longitudinal_model
        self.initial_learning_rate = 1e-3
        self.mmd = mmd

    def configure_optimizers(self):
        optimizer = Adam(self.model.parameters(), lr=self.initial_learning_rate, weight_decay=1e-3)
        gen_sched = {'scheduler': ExponentialLR(optimizer, 0.99),
                     'interval': 'epoch'}  # called after each training step
        return [optimizer], [gen_sched]

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):

        key = list(batch.keys())[1]

        labels = batch["patient_label"]

        num_visits = len(batch[key]["values"])
        num_patients = len(batch[key]["lengths"])
        means, _ = self.model.encode(batch)
        latent_trajectories = self.model.get_latent_trajectories(batch, means)

        reconstructed_patients, _ = self.model.decode(latent_trajectories)
        loss_reconstruction = torch.sum((reconstructed_patients[key] - batch[key]["values"]) ** 2) / num_visits
        loss_regularity = self.model.compute_regularity(means, None) / num_patients

        loss = loss_reconstruction + loss_regularity

        # What to I need ?
        idxs = [range(batch[key]["positions"][i], batch[key]["positions"][i + 1]) for i in
                range(len(batch["idx"]))]
        x = means.cpu().detach()
        z_space_patients = x[:,:-2]
        z_time_patients = x[:,-2:]
        mean_t_reparam_patients = torch.stack([latent_trajectories[key][idx,-1].mean().cpu().detach() for idx in idxs])
        dict_z = {
            "z_space":z_space_patients,
            "z_time":z_time_patients,
            "mean_t_reparam_patients":mean_t_reparam_patients
        }

        # MMD Loss
        if self.mmd:
            z_space_patients_grad = means[:,:-2]
            mmd_loss = MMD_loss()
            low_t_reparam = mean_t_reparam_patients.argsort()[:int(num_patients / 2)]
            high_t_reparam = mean_t_reparam_patients.argsort()[int(num_patients / 2):]
            loss_mmd = mmd_loss(z_space_patients_grad[low_t_reparam], z_space_patients_grad[high_t_reparam])
            loss += self.mmd*loss_mmd
            dict_mmd = {"loss_mmd" : loss_mmd}

        """
        z_space_dims = z_space_patients.shape[1]
        xi_patients = x[:,-1]
        tau_patients = x[:,-1]

        # Mutual information
        mi_xi = mutual_info_regression(z_space_patients, xi_patients)
        mi_tau = mutual_info_regression(z_space_patients, tau_patients)
        mi_t_reparam = mutual_info_regression(z_space_patients, mean_t_reparam_patients)
        dict_mi = {"dis_mi_xi_train" : mi_xi.mean(),
                   "dis_mi_tau_train": mi_tau.mean(),
                   "dis_mi_t_reparam_train": mi_t_reparam.mean()}

        # Compute Correlation #TODO PCA, but epoch train rather because here too low ...
        latent_corr = []
        for latent_dim in range(z_space_dims):
            latent_corr.append(np.abs(np.corrcoef(x[:,latent_dim], mean_t_reparam_patients)[0,1]))
        dict_corr = {"corr_train": np.mean(latent_corr)}

        # Compute MMD

        mmd_loss = MMD_loss()
        low_t_reparam = mean_t_reparam_patients.argsort()[:int(num_patients/2)]
        high_t_reparam = mean_t_reparam_patients.argsort()[int(num_patients/2):]
        mmd = mmd_loss(z_space_patients[low_t_reparam], z_space_patients[high_t_reparam])
        dict_mmd = {"dis_mmd_train" : mmd}

        tensorboard_logs.update(dict_mi)
        tensorboard_logs.update(dict_mmd)
        tensorboard_logs.update(dict_corr)
        """

        # Scalar logs
        tensorboard_logs = {'loss_train': loss,
                            'reconstruction_train': loss_reconstruction,
                            'regularity_train': loss_regularity}
        if self.mmd:
            tensorboard_logs.update(dict_mmd)

        # Image logs
        if batch_idx == 1:
            from utils.plot_utils import fig_reconstructions
            fig = fig_reconstructions(batch, reconstructed_patients, key)
            self.logger.experiment.add_figure('train reconsutruction plt', fig, self.current_epoch)
        return {'loss': loss, "z": dict_z, "labels":labels,
                'log': tensorboard_logs}

    def training_epoch_end(self, outputs):

        x_out, mean_traj = self.model.get_mean_trajectory(min_x=-1, max_x=1)
        for key in mean_traj.keys():
            grid_patient = make_grid(mean_traj[key], padding=10, normalize=True, range=(0, 1), nrow=1, scale_each=True)
            fig, ax = plt.subplots(1,1)
            npimg = grid_patient.detach().cpu().numpy()
            ax.imshow(np.transpose(npimg, (2, 1, 0)), interpolation='nearest')
            self.logger.experiment.add_figure('Average trajectory {}'.format(key), fig, self.current_epoch)

        # Get representations
        z_space_patients = torch.cat([outputs[i]["z"]["z_space"] for i in range(len(outputs))])
        z_time_patients = torch.cat([outputs[i]["z"]["z_time"] for i in range(len(outputs))])
        mean_t_reparam_patients = torch.cat([outputs[i]["z"]["mean_t_reparam_patients"] for i in range(len(outputs))])
        num_patients = len(z_space_patients)
        z_space_dims = z_space_patients.shape[1]
        labels = np.concatenate([outputs[i]["labels"]for i in range(len(outputs))])

        # Compute metrics of interest

        # MMD
        mmd_loss = MMD_loss()
        low_t_reparam = mean_t_reparam_patients.argsort()[:int(num_patients/2)]
        high_t_reparam = mean_t_reparam_patients.argsort()[int(num_patients/2):]
        mmd = mmd_loss(z_space_patients[low_t_reparam], z_space_patients[high_t_reparam])
        dict_mmd = {"dis_avgmmd_train" : mmd}

        # MI
        mi_t_reparam = mutual_info_regression(z_space_patients, mean_t_reparam_patients)
        dict_mi = {"dis_avgmi_t_reparam" : mi_t_reparam.mean()}

        # Correlation
        latent_corr = []
        for latent_dim in range(z_space_dims):
            latent_corr.append(np.abs(np.corrcoef(z_space_patients[:,latent_dim], mean_t_reparam_patients)[0,1]))
        corr = np.mean(latent_corr)
        dict_corr = {"dis_avgcorr" : corr}

        # Loss Train
        avg_loss = torch.stack([x['loss_train'] for x in outputs]).mean()
        tensorboard_logs = {'loss_train_avg': avg_loss}

        # Classification loss
        clf = RandomForestClassifier(max_depth=2, random_state=0)
        classif_space = cross_val_score(clf, z_space_patients.detach().numpy(), labels, cv=3).mean()
        classif_time = cross_val_score(clf, mean_t_reparam_patients.numpy().reshape(-1,1), labels, cv=3).mean()

        dict_classif = {"dis_classif_t_reparam" : classif_time,
                        "dis_classif_space": classif_space}

        tensorboard_logs.update(dict_mi)
        tensorboard_logs.update(dict_mmd)
        tensorboard_logs.update(dict_corr)
        tensorboard_logs.update(dict_classif)

        return {'log': tensorboard_logs}


    """
    def validation_step(self, batch, batch_idx):

        key = list(batch.keys())[1]

        num_visits = len(batch[key]["values"])
        num_patients = len(batch[key]["lengths"])
        means, _ = self.model.encode(batch)
        latent_trajectories = self.model.get_latent_trajectories(batch, means)
        reconstructed_patients, reconstructed_mean = self.model.decode(latent_trajectories)
        loss_reconstruction = torch.sum((reconstructed_patients[key] - batch[key]["values"]) ** 2) / num_visits
        loss_regularity = self.model.compute_regularity(means, None) / num_patients
        loss = loss_reconstruction + loss_regularity

        # Scalar logs
        tensorboard_logs = {'loss_val': loss,
                            'reconstruction_val': loss_reconstruction,
                            'regularity_val': loss_regularity}

        # Image logs
        if batch_idx == 1:
            from utils.plot_utils import fig_reconstructions
            fig = fig_reconstructions(batch, reconstructed_patients, key)
            self.logger.experiment.add_figure('val reconstruction plt', fig, self.current_epoch)

        return {'loss_val': loss, 'log': tensorboard_logs}

    def validation_epoch_end(self, outputs):

        # Scalar logs
        avg_loss = torch.stack([x['loss_val'] for x in outputs]).mean()
        tensorboard_logs = {'loss_val': avg_loss}

        # Image logs
        x_out, mean_traj = self.model.get_mean_trajectory(min_x=-1, max_x=1)
        for key in mean_traj.keys():
            grid_patient = make_grid(mean_traj[key], padding=10, normalize=True, range=(0, 1), nrow=1, scale_each=True)
            fig, ax = plt.subplots(1,1)
            npimg = grid_patient.detach().cpu().numpy()
            ax.imshow(np.transpose(npimg, (2, 1, 0)), interpolation='nearest')
            self.logger.experiment.add_figure('Average trajectory {}'.format(key), fig, self.current_epoch)
        return {'loss_val': avg_loss, 'log': tensorboard_logs}
        """

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss_test'] for x in outputs]).mean()
        tensorboard_logs = {'loss_test': avg_loss}
        return {'avg_loss_test': avg_loss, 'log': tensorboard_logs}

