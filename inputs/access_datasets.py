
import numpy as np
import os
import json
import pandas as pd

dir_path = os.path.dirname(os.path.realpath(__file__))

if dir_path.split("/")[1] == "network":
    "Using lustre to access data"
    root_machine = "/network/lustre/dtlake01/aramis/users/raphael.couronne"
else:
    "Using local machine to access data"
    root_machine = '/Users/raphael.couronne/Programming/ARAMIS'

#/Users/raphael.couronne/Programming/ARAMIS/Data/SyntheticNeurodegenerative/dSpriteLongitudinal

def access_dSpriteLongitudinal(n_clusters=0):

    dim=2

    data_path = os.path.join(root_machine, "Data/SyntheticNeurodegenerative/dSpriteLongitudinal_{}clusters".format(n_clusters))

    df = pd.read_csv(os.path.join(data_path, "df.csv"))

    ids = df['id'].tolist()
    times = df['scale'].tolist()
    paths = df['path'].tolist()

    return ids, times, paths, dim, (64,64), df




def access_realDatScan_2D64():

    dim=2


    data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/PPMI-DatScan"
    df = pd.read_csv(os.path.join(data_path, "visits.csv"))

    # Remove subject where there was a problem in spect dataframe
    subjects_spect_problems = [3387, 3542, 3706, 3710, 3791, 3953, 4107, 4136, 40543, 40755, 40757, 40760, 40778, 41767, 42164, 42449, 50586]
    df.drop( df[ np.isin(df['patient_id'], subjects_spect_problems)].index, inplace=True)

    # Remove nans
    df_nans = df[pd.isna(df).sum(axis=1)>0]
    df = df.drop(df_nans.index)
    print("Removing visit {} of patient {} because of nans".format(df_nans.index.values, df_nans['patient_id'].values))

    # Remove bad shapes
    df_badshape_0 = df[df["shape_0"] != 91]
    df_badshape_1 = df[df["shape_1"] != 109]
    df_badshape_2 = df[df["shape_2"] != 91]
    index_bad_shapes = np.unique(df_badshape_0.index.values.tolist() +
                                 df_badshape_1.index.values.tolist() +
                                 df_badshape_2.index.values.tolist())
    print("Removing visit {} of patient {} because of shape".format(index_bad_shapes, df.loc[index_bad_shapes,'patient_id']))
    df.drop(index_bad_shapes, inplace=True)

    # Keep only Parkinsonians # TODO later
    ids = df['patient_id'].tolist()
    times = df['age'].tolist()
    paths = df['path_sliced41_reshaped'].tolist()

    return ids, times, paths, dim, (64,64), df



def access_Simona_3D64(size=None):


    # TODO, load dataset practical method

    dim = 3

    miccai_data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/"
    data_simona_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/subsampled_64"

    l_subsampled = os.listdir(data_simona_path)


    groups = []
    ids = []
    visits = []
    names_images = []

    for i, name in enumerate(l_subsampled):
        group = int(name.split("atr-")[1].split(".")[0])
        id = name.split("sub-")[1].split("_")[0]
        visit = int(name.split("perc-")[1].split("_")[0])

        groups.append(group)
        ids.append(id)
        visits.append(visit)
        names_images.append(os.path.join(data_simona_path, name))

        if size is not None:
            if i>size:
                break

    df = pd.DataFrame({
        "id": id,
        "subject_cohort": groups
    })

    return ids, visits, names_images, dim, (64,64,64), df


def access_syntheticDatscanCluster_2D128():
    dim = 2

    data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/MICCAI_2020"
    data_dat_path = os.path.join(data_path, "Synthetic", "PD_DAT_2D_cluster")
    data_images_dat_path = os.path.join(data_dat_path, "Images")

    # Img
    names_images = []
    counter = 0
    for patient_id in range(100):
        for visit in range(6):
            names_images.append(os.path.join(data_images_dat_path, "Img{}_Patient{}_Visit{}.npy".format(counter, patient_id, visit)))
            counter += 1

    # Info
    with open(os.path.join(data_dat_path, 'patient_info.json'), "r") as json_data:
        patient_info = json.load(json_data)

    # As maxime syntax
    ids = np.array(patient_info['ids'])
    times = np.array(patient_info['ages']).reshape(-1)



    data_path = names_images[0].split("Images")[0]
    path_info_patient = os.path.join(data_path, "patient_info.json")

    with open(path_info_patient, 'r') as f:
        patient_info = json.load(f)

    df = pd.DataFrame({
        "id": patient_info["uids"],
        "subject_cohort": patient_info["cluster_assignment"]
    })

    return ids, times, names_images, dim, (128,128), df



#%% Dataset ADNI + NIFD

def access_Simona_3DMRI(size=None):

    dim = 3
    simona_data_path = "/network/lustre/dtlake01/aramis/projects/collaborations/miccai_2020_sb_rc/left_right_longitudinal"
    l_subsampled = os.listdir(simona_data_path)
    groups = []
    ids = []
    visits = []
    names_images = []

    for i, name in enumerate(l_subsampled):
        group = int(name.split("atr-")[1].split(".")[0])
        id = name.split("sub-")[1].split("_")[0]
        visit = int(name.split("perc-")[1].split("_")[0])

        groups.append(group)
        ids.append(id)
        visits.append(visit)
        names_images.append(os.path.join(simona_data_path, name))

        if size is not None:
            if i>size:
                break

    df = pd.DataFrame({
        "id": id,
        "subject_cohort": groups,
        "images_path":names_images,
        "group":groups
    })
    return ids, visits, names_images, dim, (121, 145, 121), df


#%%

def access_PPMI_3DMRI(size=None):

    # Data infos
    info_data_path = "/network/lustre/dtlake01/aramis/users/raphael.couronne/Data/Parkinson/Processed/PPMI/df_ppmi_metadata.csv"
    df_info = pd.read_csv(info_data_path)
    df_info = df_info[np.isin(df_info["ENROLL_CAT"], ['PD', 'GENPD'])]
    df_info.loc[df_info["ENROLL_CAT"] == "PD", "DESCRP_CAT"] = "iPD"
    df_info["ID"] = df_info["ID"].astype(str)
    df_info.set_index('ID', inplace=True)

    data_path = "/network/lustre/dtlake01/aramis/datasets/ppmi/caps/subjects"


    dim = 3

    ids = []
    visits = []
    names_images = []
    groups = []

    for root, dirs, files in os.walk(data_path):

        # If found relevant filefiles
        if len(files) > 0 and 'nii.gz' in files[0]:
            for file in files:

                # If the type we want
                if "segm-graymatter_space-Ixi549Space_modulated-on_probability" in file:

                    id = file.split("sub-PPMI")[1].split("_")[0]
                    visit = int(file.split("ses-M")[1].split("_")[0])

                    if id in df_info.index.unique():

                        # Append to lists
                        ids.append(id)
                        visits.append(visit)
                        names_images.append(os.path.join(root, file))
                        groups.append(df_info.loc[id, "DESCRP_CAT"])

                    else:
                        print("Patient {} was not found in dataframe infos".format(id))

        # Stop if too many
        if size is not None:
            if len(ids) > size:
                break

    df = pd.DataFrame({
        "id": ids,
        "visits": visits,
        "images_path": names_images,
        "groups": groups
    })

    return ids, visits, names_images, dim, (121, 145, 121), df


"""
import pandas as pd

dim = 3

aramis_dataset_path = "/network/lustre/dtlake01/aramis/datasets/"
adni_data_path = os.path.join(aramis_dataset_path, "adni")
nifd_data_path = os.path.join(aramis_dataset_path, "nifd")

df_nifd = pd.read_csv(os.path.join(nifd_data_path, "bids", "participants.tsv"), sep="\t")


import nibabel as nib
img = nib.load(os.path.join(nifd_data_path, "caps", "subjects",df_nifd["participant_id"][0], "ses-M00", "anat","sub-NIFD1S0001_ses-M00_T1w.nii.gz"))
#img = nib.load(os.path.join(nifd_data_path, "bids", df_nifd["participant_id"][0], "ses-M00", "anat","sub-NIFD1S0001_ses-M00_FLAIR.nii.gz"))


import matplotlib.pyplot as plt
plt.imshow(img.get_data()[:,:,60])
plt.savefig("../mri.pdf")
plt.show()

#%%

# Create dataframe nifd
df_nifd_torch = pd.DataFrame()
for participant_id in df_nifd['participant_id']:
    patient_nifd_dir = os.path.join(nifd_data_path, "bids", participant_id)
    df_patient_nifd = pd.read_csv(os.path.join(patient_nifd_dir, '{}_sessions.tsv'.format(participant_id)), sep="\t")
    df_patient_nifd['participant_id'] = participant_id

    df_nifd_torch = pd.concat(([df_nifd_torch, df_patient_nifd]))


#%% Load in ADNI

df_adni = pd.read_csv(os.path.join(adni_data_path, "bids", "BIDS","participants.tsv"), sep="\t")
img = nib.load(os.path.join(adni_data_path, "bids", "BIDS",df_adni["participant_id"][0], "ses-M00", "anat","{}_ses-M00_T1w.nii.gz".format(df_adni["participant_id"][0])))
"""
#%%

